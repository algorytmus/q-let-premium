"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="./../../messaging/TypeIdentity.ts" />
var TypeIdentity_1 = require("./../../messaging/TypeIdentity");
/// <reference path="./../../data/WritePageQuizletDataMessage.ts" />
var WritePageQuizletDataMessage_1 = require("./../../data/WritePageQuizletDataMessage");
var WritePageQuizletDataProvider = /** @class */ (function () {
    function WritePageQuizletDataProvider(messageBus, window, dom) {
        this.messageBus = messageBus;
        this.window = window;
        this.dom = dom;
    }
    WritePageQuizletDataProvider.prototype.attach = function () {
        var instance = this;
        return this.messageBus
            .observeRequests(TypeIdentity_1.TypeIdentity.createIdentity(WritePageQuizletDataMessage_1.WritePageQuizletDataMessage.prototype))
            .select(function (observer) { return instance.dom.observeInitialized()
            .where(function (_) { return instance.window.Quizlet != undefined; })
            .where(function (_) { return instance.window.Quizlet.learnGameData != undefined; })
            .do(function (_) { return observer.onNext(new WritePageQuizletDataMessage_1.WritePageQuizletDataMessage({ learnGameData: instance.window.Quizlet.learnGameData })); }); })
            .switch()
            .subscribe();
    };
    return WritePageQuizletDataProvider;
}());
exports.WritePageQuizletDataProvider = WritePageQuizletDataProvider;
//# sourceMappingURL=WritePageQuizletDataProvider.js.map