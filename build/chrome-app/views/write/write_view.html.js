/// <reference path="./../../injections/InnerHTMLInjector.ts" />
/// <reference path="./../../injections/Url.ts" />
/// <reference path="./../../scripts/jquery/jquery.d.ts" />
/// <reference path="./../../scripts/rx-dom/rx-dom.d.ts" />
/// <reference path="./WritePageQuizletDataProvider.as-script.ts" />
//dependsOn=messaging/Export.js
//dependsOn=injections/Url.js
//dependsOn=injections/ScriptInjector.js
//dependsOn=injections/InnerHTMLInjector.js
//dependsOn=injections/BindingInjector.js
//dependsOn=injections/AttributeInjector.js
//dependsOn=injections/ElementAppender.js
//dependsOn=views/bindings/SelfBinding.js
//dependsOn=scripts/jquery/jquery.js
//dependsOn=scripts/rxjs/rx.all.js
//dependsOn=scripts/rx-dom/rx.dom.js
//dependsOn=scripts/rxjs-jquery/rx.jquery.js
//dependsOn=views/write/write_styles.css
//dependsOn=view_models/write/WriteViewModel.js
//dependsOn=views/bindings/ViewModelResolver.js
//dependsOn=views/write/WritePageQuizletDataProvider.init.js
//dependsOn=views/write/WritePageQuizletDataProvider.as-script.js
var dom = new Dom(10);
new ScriptInjector(dom, writePageQuizletDataProvider_init_dependencies.map(function (f) { return new Url(chrome.runtime.getURL(f)); }), 'body').attach();
new AttributeInjector('body', 'data-bind', 'resolveViewModel: WriteViewModel', dom)
    .attach();
new ElementAppender('div.UITextarea-content', '#user-hint', function () { return $('#user-answer').length > 0 || $('#js-learnModeCopyInput').length > 0; }, '<span id="user-hint"></span>', dom)
    .attach();
new AttributeInjector('body', 'self-binding', '', dom).attach();
new BindingInjector('#user-answer', function (kbc) { return kbc.extend({
    value: kbc.$data.userAnswer,
    valueUpdate: 'input',
    onInitialized: kbc.$data,
    externalEvent: {
        thisValue: kbc.$data.userAnswer,
        element: 'button.UISpecialCharacterButton',
        event: 'click'
    }
}); }, dom).attach();
new BindingInjector('span.TermText', function (kbc) { return kbc.extend({
    viewImmutableText: kbc.$data.term
}); }, dom).attach();
new BindingInjector('#js-learnModeCopyInput', function (kbc) { return kbc.extend({
    value: kbc.$data.userAnswer,
    valueUpdate: 'input',
    externalEvent: {
        thisValue: kbc.$data.userAnswer,
        element: 'button.UISpecialCharacterButton',
        event: 'click'
    }
}); }, dom).attach();
new BindingInjector('#user-hint', function (kbc) { return kbc.extend({ text: kbc.$data.hint }); }, dom).attach();
dom.observeInitialized()
    .filter(function (v) { return $('#user-hint').height() != undefined; })
    .filter(function (v) { return $('#user-answer').height() != undefined; })
    .do(function (v) { return $('#user-hint').width($('#user-answer').width()); })
    .do(function (v) { return $('#user-answer').attr('style', 'min-height:' + ($('#user-hint').height() + 20).toString() + 'px;'); })
    .subscribe();
dom.observeInitialized()
    .filter(function (v) { return $('#user-hint').height() != undefined; })
    .filter(function (v) { return $('#js-learnModeCopyInput').height() != undefined; })
    .do(function (v) { return $('#user-hint').width($('#js-learnModeCopyInput').width()); })
    .do(function (v) { return $('#js-learnModeCopyInput').attr('style', 'min-height:' + ($('#user-hint').height() + 20).toString() + 'px;'); })
    .subscribe();
//# sourceMappingURL=write_view.html.js.map