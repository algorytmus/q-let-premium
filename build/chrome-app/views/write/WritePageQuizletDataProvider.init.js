"use strict";
//dependsOn=views/write/WritePageQuizletDataProvider.js
//dependsOn=messaging/Export.js
Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="./../../messaging/RawMessageBus.ts" />
var RawMessageBus_1 = require("./../../messaging/RawMessageBus");
/// <reference path="./../../messaging/MessageBus.ts" />
var MessageBus_1 = require("./../../messaging/MessageBus");
/// <reference path="./WritePageQuizletDataProvider.ts" />
var WritePageQuizletDataProvider_1 = require("./WritePageQuizletDataProvider");
/// <reference path="./../../messaging/MessageWindow.ts" />
var MessageWindow_1 = require("./../../messaging/MessageWindow");
/// <reference path="./../../injections/Dom.ts" />
var Dom_1 = require("../../injections/Dom");
var writePageQuizletDataProvider = new WritePageQuizletDataProvider_1.WritePageQuizletDataProvider(new MessageBus_1.MessageBus(new RawMessageBus_1.RawMessageBus(new MessageWindow_1.MessageWindow(window))), window, new Dom_1.Dom(100));
writePageQuizletDataProvider.attach();
//# sourceMappingURL=WritePageQuizletDataProvider.init.js.map