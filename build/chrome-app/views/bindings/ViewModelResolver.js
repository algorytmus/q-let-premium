"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="./../../messaging/MessageWindow.ts" />
var MessageWindow_1 = require("./../../messaging/MessageWindow");
/// <reference path="./../../../node_modules/@types/knockout/index.d.ts" />
/// <reference path="./../../../node_modules/@types/knockout/index.d.ts" />
/// <reference path="./../../scripts/reactive-knockout/2-reactive-knockout.d.ts" />
//dependsOn=scripts/knockout/knockout.js
//dependsOn=scripts/reactive-knockout/1-reactive-knockout.js
//dependsOn=view_models/write/WriteViewModel.js
//dependsOn=data/QuizletDataProvider.js
//dependsOn=scripts/knockout/knockout.js
/// <reference path="./../../messaging/RawMessageBus.ts" />
var RawMessageBus_1 = require("./../../messaging/RawMessageBus");
/// <reference path="./../../messaging/MessageBus.ts" />
var MessageBus_1 = require("./../../messaging/MessageBus");
/// <reference path="./../../data/QuizletDataProvider.ts" />
var QuizletDataProvider_1 = require("./../../data/QuizletDataProvider");
/// <reference path="./../../injections/Dom.ts" />
var Dom_1 = require("./../../injections/Dom");
/// <reference path="./../../view_models/write/WriteViewModel.ts"/>
var WriteViewModel_1 = require("./../../view_models/write/WriteViewModel");
var ViewModelResolver = /** @class */ (function () {
    function ViewModelResolver() {
    }
    ViewModelResolver.prototype.init = function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var type = valueAccessor();
        var mw = new MessageWindow_1.MessageWindow(window);
        var dom = new Dom_1.Dom(200);
        var hintProvider = new WriteViewModel_1.HintProvider(new WriteViewModel_1.SpecificHintProviderCollection({
            StarsInsteadOfLetters: new WriteViewModel_1.StarsInsteadOfLettersHintProvider(),
            FirstLetterThenStarsInsteadOfLetters: new WriteViewModel_1.FirstLetterThenStarsInsteadOfLettersHintProvider(),
            FirstLettersOfWordsThenEveryOtherLetter: new WriteViewModel_1.FirstLettersOfWordsThenEveryOtherLetterHintProvider(),
            FirstLettersOfWordsThenStarsInsteadOfLetters: new WriteViewModel_1.FirstLettersOfWordsThenStarsInsteadOfLettersHintProvider()
        }));
        var vm = new WriteViewModel_1.WriteViewModel(new QuizletDataProvider_1.QuizletDataProvider(new MessageBus_1.MessageBus(new RawMessageBus_1.RawMessageBus(mw))), hintProvider);
        if (bindingContext) {
            bindingContext.$data = vm;
        }
    };
    return ViewModelResolver;
}());
ko.bindingHandlers.resolveViewModel = new ViewModelResolver();
var vm = undefined;
ko.bindingHandlers.onInitialized = {
    init: function (element, valueAccessor, allBindings, viewModel) {
        var callback = valueAccessor();
        console.log('calling callback');
        console.log(callback);
        if (callback != undefined) {
            console.log('callback called 1');
            vm = callback;
            callback.nextExcercise({}); //.call(viewModel);
            console.log('callback called 2');
        }
    }
};
ko.bindingHandlers.externalEvent = {
    init: function (element, valueAccessor, allBindings, viewModel) {
        var property = valueAccessor();
        $(property.element).click(function () {
            setTimeout(function () { return property.thisValue($(element)[0].value); }, 10);
            return true;
        });
    }
};
ko.bindingHandlers.viewImmutableText = {
    init: function (element, valueAccessor, allBindings, viewModel) {
        var property = valueAccessor();
        property($("span.TermText")[0].outerText);
        $("span.TermText").on('DOMSubtreeModified', function () {
            property($("span.TermText")[0].outerText);
        });
    }
};
//# sourceMappingURL=ViewModelResolver.js.map