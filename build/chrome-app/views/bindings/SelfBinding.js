"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var rx_all_1 = require("./../scripts/rxjs/rx.all");
/// <reference path="./../../scripts/jquery/jquery.d.ts" />
/// <reference path="./../../scripts/rxjs/ts/rx.all.d.ts" />
/// <reference path="./../../scripts/rxjs-jquery/ts/rx.jquery.d.ts" />
/// <reference path="./../../scripts/rx-dom/rx-dom.d.ts" />
/// <reference path="./../../../node_modules/@types/knockout/index.d.ts" />
//dependsOn=scripts/jquery/jquery.js
//dependsOn=scripts/rxjs/rx.all.js
//dependsOn=scripts/rxjs-jquery/rx.jquery.js
//dependsOn=scripts/rx-dom/rx.dom.js
//dependsOn=injections/Url.js
//dependsOn=scripts/knockout/knockout.js
//dependsOn=messaging/Export.js
var SelfBinding = /** @class */ (function () {
    function SelfBinding() {
    }
    SelfBinding.prototype.attach = function () {
        var observable = this.observeInitialized()
            .merge(this.observeDOMSubtreeModified())
            .debounce(200);
        return observable
            .subscribe(function (v) { return SelfBinding.apply(); });
    };
    SelfBinding.prototype.observeInitialized = function () {
        return rx_all_1.Observable.start(function () { return ''; });
    };
    SelfBinding.prototype.observeDOMSubtreeModified = function () {
        return $(document)
            .onAsObservable('DOMSubtreeModified')
            .debounce(200)
            .map(function (f) { return ''; });
    };
    SelfBinding.selectSelfBindingNodes = function () {
        return $('[' + SelfBinding.SelfBindingAttribut + ']');
    };
    SelfBinding.apply = function () {
        SelfBinding.selectSelfBindingNodes().each(function (index, elem) {
            if (!ko.dataFor(elem)) {
                try {
                    ko.applyBindings();
                }
                catch (e) {
                    console.log(e);
                }
            }
        });
    };
    SelfBinding.SelfBindingAttribut = 'self-binding';
    return SelfBinding;
}());
new SelfBinding().attach();
//# sourceMappingURL=SelfBinding.js.map