"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="./../scripts/jquery/jquery.d.ts" />
/// <reference path="./../scripts/rxjs/ts/rx.all.d.ts" />
/// <reference path="./../scripts/rxjs-jquery/ts/rx.jquery.d.ts" />
/// <reference path="./../scripts/rx-dom/rx-dom.d.ts" />
/// <reference path="./Url.ts" />
//dependsOn=scripts/jquery/jquery.js
//dependsOn=scripts/rxjs/rx.all.js
//dependsOn=scripts/rxjs-jquery/rx.jquery.js
//dependsOn=scripts/rx-dom/rx.dom.js
//dependsOn=injections/Url.js
var ElementAppender = /** @class */ (function () {
    function ElementAppender(parentElementSelector, elementSelector, predicate, valueToAppend, dom) {
        this.parentElementSelector = parentElementSelector;
        this.elementSelector = elementSelector;
        this.predicate = predicate;
        this.valueToAppend = valueToAppend;
        this.dom = dom;
    }
    ElementAppender.prototype.attach = function () {
        var injector = this;
        return this.dom
            .observeInitializedAndChanges()
            .delay(100)
            .subscribe(function (v) { return ElementAppender.inject(injector); });
    };
    ElementAppender.inject = function (_this) {
        console.log("APPENDING TRY");
        if ($(_this.elementSelector).length == 0 && _this.predicate()) {
            console.log("APPENDING OK");
            $(_this.parentElementSelector).prepend(_this.valueToAppend);
        }
    };
    return ElementAppender;
}());
//# sourceMappingURL=ElementAppender.js.map