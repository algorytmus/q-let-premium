/// <reference path="./../scripts/jquery/jquery.d.ts" />
/// <reference path="./../scripts/rxjs/ts/rx.all.d.ts" />
/// <reference path="./../scripts/rxjs-jquery/ts/rx.jquery.d.ts" />
/// <reference path="./../scripts/rx-dom/rx-dom.d.ts" />
/// <reference path="./Url.ts" />
//dependsOn=scripts/jquery/jquery.js
//dependsOn=scripts/rxjs/rx.all.js
//dependsOn=scripts/rxjs-jquery/rx.jquery.js
//dependsOn=scripts/rx-dom/rx.dom.js
//dependsOn=injections/Url.js
var BindingInjector = /** @class */ (function () {
    function BindingInjector(elementSelector, on$dataAvailable, dom) {
        this.elementSelector = elementSelector;
        this.on$dataAvailable = on$dataAvailable;
        this.dom = dom;
    }
    BindingInjector.prototype.attach = function () {
        var injector = this;
        return this.dom
            .observeInitializedAndChanges()
            .subscribe(function (v) { return BindingInjector.update(injector); });
    };
    BindingInjector.update = function (injector) {
        $(injector.elementSelector)
            .each(function (index, elem) {
            BindingInjector.inject(injector, elem);
        });
    };
    BindingInjector.inject = function (injector, node) {
        if (ko.dataFor(node)) {
            ko.applyBindingsToNode(node, injector.on$dataAvailable(ko.contextFor(node)));
        }
    };
    return BindingInjector;
}());
//# sourceMappingURL=BindingInjector.js.map