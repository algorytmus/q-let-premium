var ScriptInjector = /** @class */ (function () {
    function ScriptInjector(dom, files, elementSelector) {
        this.dom = dom;
        this.files = files;
        this.elementSelector = elementSelector;
    }
    ScriptInjector.prototype.attach = function () {
        var onloadAction = "";
        while (this.files.length > 0) {
            onloadAction = ScriptInjector.formatScriptInjection(this.files.pop(), onloadAction);
        }
        this.dom.observeInitialized()
            .do(function (_) { return ScriptInjector.inject(onloadAction); })
            .subscribe();
    };
    ScriptInjector.inject = function (script) {
        $('<script>' + script + '</' + 'script>').appendTo(document.body);
    };
    ScriptInjector.formatScriptInjection = function (file, onloadAction) {
        return "(function(d, script) {\n            script = d.createElement('script');\n            script.type = 'text/javascript';\n            script.async = true;\n            script.onload = function(){\n                " + onloadAction + "\n            };\n            script.src = '" + file.url + "';\n            d.getElementsByTagName('head')[0].appendChild(script);\n        }(document));";
    };
    return ScriptInjector;
}());
//# sourceMappingURL=ScriptInjector.js.map