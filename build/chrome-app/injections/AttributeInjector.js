/// <reference path="./../scripts/jquery/jquery.d.ts" />
/// <reference path="./../scripts/rxjs/ts/rx.all.d.ts" />
/// <reference path="./../scripts/rxjs-jquery/ts/rx.jquery.d.ts" />
/// <reference path="./../scripts/rx-dom/rx-dom.d.ts" />
/// <reference path="./Url.ts" />
//dependsOn=scripts/jquery/jquery.js
//dependsOn=scripts/rxjs/rx.all.js
//dependsOn=scripts/rxjs-jquery/rx.jquery.js
//dependsOn=scripts/rx-dom/rx.dom.js
//dependsOn=injections/Url.js
var AttributeInjector = /** @class */ (function () {
    function AttributeInjector(elementSelector, attributeName, attributeValue, dom) {
        this.elementSelector = elementSelector;
        this.attributeName = attributeName;
        this.attributeValue = attributeValue;
        this.dom = dom;
    }
    AttributeInjector.prototype.attach = function () {
        var injector = this;
        return this.dom
            .observeInitializedAndChanges()
            .subscribe(function (v) { return AttributeInjector.inject(injector); });
    };
    AttributeInjector.inject = function (_this) {
        $(_this.elementSelector).attr(_this.attributeName, _this.attributeValue);
    };
    return AttributeInjector;
}());
//# sourceMappingURL=AttributeInjector.js.map