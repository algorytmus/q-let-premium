"use strict";
/// <reference path="./../scripts/jquery/jquery.d.ts" />
/// <reference path="./../scripts/rxjs/ts/rx.all.d.ts" />
/// <reference path="./../scripts/rxjs-jquery/ts/rx.jquery.d.ts" />
/// <reference path="./../scripts/rx-dom/rx-dom.d.ts" />
/// <reference path="./Url.ts" />
/// <reference path="./../messaging/Export.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
//dependsOn=messaging/Export.js
//dependsOn=scripts/jquery/jquery.js
//dependsOn=scripts/rxjs/rx.all.js
//dependsOn=scripts/rxjs-jquery/rx.jquery.js
//dependsOn=scripts/rx-dom/rx.dom.js
//dependsOn=injections/Url.js
var Dom = /** @class */ (function () {
    function Dom(refreshRate) {
        this.refreshRate = refreshRate;
    }
    Dom.prototype.observeInitialized = function () {
        return Dom.observeLoad()
            .startWith('')
            .throttle(this.refreshRate);
    };
    Dom.prototype.observeChanged = function () {
        return Dom.observeDOMSubtreeModified()
            .throttle(this.refreshRate);
    };
    Dom.prototype.observeInitializedAndChanges = function () {
        return this.observeInitialized()
            .merge(this.observeChanged());
    };
    Dom.observeLoad = function () {
        return $(document)
            .onAsObservable('load');
    };
    Dom.observeDOMSubtreeModified = function () {
        return $(document)
            .onAsObservable('DOMSubtreeModified');
    };
    Dom.prototype.$ = function (selector) {
        return jQuery(selector);
    };
    return Dom;
}());
exports.Dom = Dom;
//# sourceMappingURL=Dom.js.map