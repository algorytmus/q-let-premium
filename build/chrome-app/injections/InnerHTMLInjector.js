"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="./../scripts/jquery/jquery.d.ts" />
/// <reference path="./../scripts/rxjs/ts/rx.all.d.ts" />
/// <reference path="./../scripts/rxjs-jquery/ts/rx.jquery.d.ts" />
/// <reference path="./../scripts/rx-dom/rx-dom.d.ts" />
/// <reference path="./Url.ts" />
//dependsOn=scripts/jquery/jquery.js
//dependsOn=scripts/rxjs/rx.all.js
//dependsOn=scripts/rxjs-jquery/rx.jquery.js
//dependsOn=scripts/rx-dom/rx.dom.js
//dependsOn=injections/Url.js
//dependsOn=injections/Dom.js
var InnerHTMLInjector = /** @class */ (function () {
    function InnerHTMLInjector(parentName, htmlUrl, dom) {
        this.parentName = parentName;
        this.htmlUrl = htmlUrl;
        this.dom = dom;
    }
    InnerHTMLInjector.prototype.attach = function () {
        var injector = this;
        return this.dom.observeInitializedAndChanges()
            .subscribe(function () { return InnerHTMLInjector.load(injector); });
    };
    InnerHTMLInjector.load = function (injector) {
        $.get(injector.htmlUrl.url, function (d) { return InnerHTMLInjector.inject(injector, d); });
    };
    InnerHTMLInjector.inject = function (injector, data) {
        if (data == undefined) {
            throw new URIError('Data to inject not found.');
        }
        if (!InnerHTMLInjector.hasInjectionAppliedFlag(injector.parentName)) {
            $(injector.parentName).html(data);
            InnerHTMLInjector.setInjectionAppliedFlag(injector.parentName);
        }
    };
    InnerHTMLInjector.hasInjectionAppliedFlag = function (parentName) {
        return $(parentName).attr('injection-applied') !== undefined;
    };
    InnerHTMLInjector.setInjectionAppliedFlag = function (parentName) {
        $(parentName).attr('injection-applied', '');
    };
    return InnerHTMLInjector;
}());
//# sourceMappingURL=InnerHTMLInjector.js.map