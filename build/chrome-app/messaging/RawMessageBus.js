"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="./../scripts/jquery/jquery.d.ts" />
/// <reference path="./../scripts/rxjs/ts/rx.all.d.ts" />
/// <reference path="./TypeIdentity.ts" />
var TypeIdentity_1 = require("./TypeIdentity");
var RawMessageBus = /** @class */ (function () {
    function RawMessageBus(window) {
        this.window = window;
    }
    RawMessageBus.prototype.sendData = function (data, identity) {
        this.sendMessage({ identity: identity.serialize(), data: data });
    };
    RawMessageBus.prototype.sendMessage = function (rawMessage) {
        this.window.send(rawMessage);
    };
    RawMessageBus.prototype.observeData = function (identity) {
        return this.window.eventStream
            .where(function (rm) { return rm != undefined; })
            .where(function (rm) { return rm.identity != undefined; })
            .map(RawMessageBus.check)
            .where(function (rm) { return rm != undefined; })
            .where(function (rm) { return rm.identity != undefined; })
            .filter(function (rm) { return rm.identity.isSameAs(identity); })
            .map(function (rm) { return rm.data; });
    };
    RawMessageBus.check = function (rawMessage) {
        if (rawMessage.identity && rawMessage.data) {
            return { identity: TypeIdentity_1.TypeIdentityUntyped.deserialize(rawMessage.identity), data: rawMessage.data };
        }
        return undefined;
    };
    return RawMessageBus;
}());
exports.RawMessageBus = RawMessageBus;
//# sourceMappingURL=RawMessageBus.js.map