"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var rx_all_1 = require("./../scripts/rxjs/rx.all");
var MessageWindow = /** @class */ (function () {
    function MessageWindow(window) {
        this.window = window;
    }
    MessageWindow.prototype.send = function (data) {
        this.window.postMessage(data, '*');
    };
    Object.defineProperty(MessageWindow.prototype, "eventStream", {
        get: function () {
            return rx_all_1.Observable
                .fromEvent(this.window, 'message')
                .map(MessageWindow.toRawMessageUnchecked);
        },
        enumerable: true,
        configurable: true
    });
    MessageWindow.toRawMessageUnchecked = function (rawMessage) {
        return { identity: rawMessage.data.identity, data: rawMessage.data.data };
    };
    return MessageWindow;
}());
exports.MessageWindow = MessageWindow;
//# sourceMappingURL=MessageWindow.js.map