"use strict";
/// <reference path="./Export.ts" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var TypeIdentityUntyped = /** @class */ (function () {
    function TypeIdentityUntyped(identity) {
        this.identity = identity;
    }
    TypeIdentityUntyped.prototype.isSameAs = function (identity) {
        return this.identity === identity.identity;
    };
    TypeIdentityUntyped.prototype.serialize = function () {
        return new TypeIdentityUntypedSerialized(this.identity);
    };
    TypeIdentityUntyped.deserialize = function (data) {
        return new TypeIdentityUntyped(data.identity);
    };
    return TypeIdentityUntyped;
}());
exports.TypeIdentityUntyped = TypeIdentityUntyped;
var TypeIdentityUntypedSerialized = /** @class */ (function () {
    function TypeIdentityUntypedSerialized(identity) {
        this.identity = identity;
    }
    return TypeIdentityUntypedSerialized;
}());
exports.TypeIdentityUntypedSerialized = TypeIdentityUntypedSerialized;
var TypeIdentity = /** @class */ (function (_super) {
    __extends(TypeIdentity, _super);
    function TypeIdentity(identity) {
        return _super.call(this, identity) || this;
    }
    TypeIdentity.createIdentity = function (prototype) {
        /*if (!TypeIdentity.canBeCloned(prototype)) {
            throw TypeError('Cannot create TypeIdentity because the type is not serializable.');
        }*/
        return TypeIdentity.createIdentityFromConstructor(prototype.constructor);
    };
    TypeIdentity.createIdentityFromConstructor = function (constructor) {
        return new TypeIdentity(constructor.name);
    };
    TypeIdentity.createIdentityComplex = function (outerPrototype, innerPrototype) {
        return new TypeIdentity(TypeIdentity.createIdentity(outerPrototype).identity + "<" + innerPrototype.identity + ">");
    };
    TypeIdentity.canBeCloned = function (prototype) {
        if (Object(prototype) !== prototype)
            return true;
        switch ({}.toString.call(prototype).slice(8, -1)) {
            case 'Boolean':
            case 'Number':
            case 'String':
            case 'Date':
            case 'RegExp':
            case 'Blob':
            case 'FileList':
            case 'ImageData':
            case 'ImageBitmap':
            case 'ArrayBuffer':
                return true;
            case 'Array':
            case 'Object':
                return Object.keys(prototype).every(function (prop) { return TypeIdentity.canBeCloned(prototype[prop]); });
            case 'Map':
                return prototype.keys().slice().every(TypeIdentity.canBeCloned)
                    && prototype.prototypeues().slice().every(TypeIdentity.canBeCloned);
            case 'Set':
                return prototype.keys().slice().every(TypeIdentity.canBeCloned);
            default:
                return false;
        }
    };
    return TypeIdentity;
}(TypeIdentityUntyped));
exports.TypeIdentity = TypeIdentity;
//# sourceMappingURL=TypeIdentity.js.map