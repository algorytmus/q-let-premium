"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ClientId = /** @class */ (function () {
    function ClientId() {
        this.id = ClientId.generateGuid();
    }
    ClientId.generateGuid = function () {
        var S4 = function () {
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        };
        return (S4() + S4() + '-' + S4() + '-' + S4() + '-' + S4() + '-' + S4() + S4() + S4());
    };
    return ClientId;
}());
exports.ClientId = ClientId;
//# sourceMappingURL=ClientId.js.map