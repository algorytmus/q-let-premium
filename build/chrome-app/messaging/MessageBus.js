"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var rx_all_1 = require("./../scripts/rxjs/rx.all");
/// <reference path="./../scripts/jquery/jquery.d.ts" />
/// <reference path="./../scripts/rxjs/ts/rx.all.d.ts" />
/// <reference path="./ClientId.ts" />
var ClientId_1 = require("./ClientId");
/// <reference path="./RawSubscribtion.ts" />
var RawSubscribtion_1 = require("./RawSubscribtion");
/// <reference path="./RawResponse.ts" />
var RawResponse_1 = require("./RawResponse");
/// <reference path="./TypeIdentity.ts" />
var TypeIdentity_1 = require("./TypeIdentity");
var MessageBus = /** @class */ (function () {
    function MessageBus(rawMessageBus) {
        this.rawMessageBus = rawMessageBus;
        this.clientId = new ClientId_1.ClientId();
    }
    MessageBus.prototype.observeRequests = function (identity) {
        var _this = this;
        var clientId = this.clientId;
        return this.rawMessageBus
            .observeData(MessageBus.GetRawSubscribtionIdentity(identity))
            .groupBy(function (s) { return s.clientId.id; })
            .map(function (_) {
            var subject = new rx_all_1.Subject();
            subject.do(function (m) {
                _this.rawMessageBus
                    .sendData(new RawResponse_1.RawResponse(m, clientId), MessageBus.GetRawResponseIdentity(identity));
            }).subscribe();
            return subject;
        });
    };
    MessageBus.GetRawSubscribtionIdentity = function (identity) {
        return TypeIdentity_1.TypeIdentity.createIdentityComplex(RawSubscribtion_1.RawSubscribtion.prototype, identity);
    };
    MessageBus.GetRawResponseIdentity = function (identity) {
        return TypeIdentity_1.TypeIdentity.createIdentityComplex(RawResponse_1.RawResponse.prototype, identity);
    };
    MessageBus.prototype.observe = function (identity) {
        var set = false;
        var rawMessageBus = this.rawMessageBus;
        var clientId = this.clientId;
        var dataArrivalEvent = rawMessageBus.observeData(MessageBus.GetRawResponseIdentity(identity))
            .do(function (_) { return set = true; })
            .map(function (rawResponse) { return rawResponse.response; });
        var subscriptionData = new RawSubscribtion_1.RawSubscribtion(clientId);
        var dataRequestPushes = rx_all_1.Observable.interval(50)
            .takeWhile(function () { return !set; })
            .do(function (_) { return rawMessageBus.sendData(subscriptionData, MessageBus.GetRawSubscribtionIdentity(identity)); })
            .startWith(function (_) { return rawMessageBus.sendData(subscriptionData, MessageBus.GetRawSubscribtionIdentity(identity)); });
        return dataArrivalEvent.merge(dataRequestPushes.where(function (_) { return false; }));
    };
    return MessageBus;
}());
exports.MessageBus = MessageBus;
//# sourceMappingURL=MessageBus.js.map