"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var RawResponse = /** @class */ (function () {
    function RawResponse(response, clientId) {
        this.response = response;
        this.clientId = clientId;
    }
    return RawResponse;
}());
exports.RawResponse = RawResponse;
//# sourceMappingURL=RawResponse.js.map