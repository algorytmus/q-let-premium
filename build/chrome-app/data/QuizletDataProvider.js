"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="./../messaging/TypeIdentity.ts" />
var TypeIdentity_1 = require("./../messaging/TypeIdentity");
/// <reference path="./WritePageQuizletDataMessage.ts" />
var WritePageQuizletDataMessage_1 = require("./WritePageQuizletDataMessage");
var QuizletDataProvider = /** @class */ (function () {
    function QuizletDataProvider(messageBus) {
        this.messageBus = messageBus;
        this.observableQuizlet = this.messageBus
            .observe(TypeIdentity_1.TypeIdentity.createIdentity(WritePageQuizletDataMessage_1.WritePageQuizletDataMessage.prototype))
            .map(function (d) { return d.data; });
    }
    Object.defineProperty(QuizletDataProvider.prototype, "Quizlet", {
        get: function () {
            return this.observableQuizlet;
        },
        enumerable: true,
        configurable: true
    });
    return QuizletDataProvider;
}());
exports.QuizletDataProvider = QuizletDataProvider;
//# sourceMappingURL=QuizletDataProvider.js.map