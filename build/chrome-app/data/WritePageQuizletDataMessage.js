"use strict";
/// <reference path="./../scripts/jquery/jquery.d.ts" />
/// <reference path="./../scripts/rxjs/ts/rx.all.d.ts" />
/// <reference path="./../scripts/rxjs-jquery/ts/rx.jquery.d.ts" />
/// <reference path="./../scripts/rx-dom/rx-dom.d.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var WritePageQuizletDataMessage = /** @class */ (function () {
    function WritePageQuizletDataMessage(data) {
        this.data = data;
    }
    return WritePageQuizletDataMessage;
}());
exports.WritePageQuizletDataMessage = WritePageQuizletDataMessage;
//# sourceMappingURL=WritePageQuizletDataMessage.js.map