"use strict";
/// <reference path="./Exercise.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var PromptOrder;
(function (PromptOrder) {
    PromptOrder["WordThenDefinition"] = "1-2";
    PromptOrder["DefinitionThenWord"] = "2-1";
})(PromptOrder = exports.PromptOrder || (exports.PromptOrder = {}));
//# sourceMappingURL=Quizlet.js.map