"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var gulp = require('gulp');
var TsRenderer_1 = require("./render-ts/TsRenderer");
gulp.task('render', function () { return new TsRenderer_1.TsRenderer('./chrome-extension').render(); });
var BuilderSpace_1 = require("./chrome-extension/BuilderSpace");
var Workspace_1 = require("./chrome-extension/Workspace");
var BuilderPath_1 = require("./chrome-extension/BuilderPath");
var workspace = new Workspace_1.Workspace('./');
var builderSpace = new BuilderSpace_1.BuilderSpace(new BuilderPath_1.BuilderPath('./../build/'));
var projects = workspace.projects();
gulp.task('build', function () { return projects[0].build(builderSpace); });
//# sourceMappingURL=gulpfile.js.map