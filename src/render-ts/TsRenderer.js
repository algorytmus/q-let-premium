"use strict";
var fs_1 = require("fs");
var path_1 = require("path");
var TsRendering;
(function (TsRendering) {
    var TsRenderer = /** @class */ (function () {
        function TsRenderer(root) {
            this.root = root;
        }
        TsRenderer.prototype.render = function () {
            //console.log(TsRenderer.listFilesRecursively(this.root));
            TsRenderer.listFilesRecursively(this.root)
                .forEach(TsRenderer.renderModule);
        };
        TsRenderer.listFilesRecursively = function (dir) {
            return fs_1.readdirSync(dir)
                .map(function (entry) { return path_1.join(dir, entry); })
                .map(function (fullPathEntry) { return fs_1.statSync(fullPathEntry).isDirectory()
                ? TsRenderer.listFilesRecursively(fullPathEntry) : [fullPathEntry]; })
                .reduce(function (previousValue, currentValue, _) { return previousValue.concat(currentValue); }, [])
                .filter(function (fullPathEntry) { return fullPathEntry.lastIndexOf('render-ts.ts') > 0; });
        };
        TsRenderer.renderModule = function (moduleFile) {
            var moduleName = path_1.relative(__dirname, moduleFile.replace('render-ts.ts', 'render-ts'));
            var renderedFileName = moduleFile.replace('.render-ts.ts', '.ts');
            if (fs_1.existsSync(renderedFileName)) {
                fs_1.unlinkSync(renderedFileName);
            }
            TsRenderer.renderPromise(require(moduleName).render, renderedFileName);
        };
        TsRenderer.renderPromise = function (promise, fileName) {
            if (fs_1.existsSync(fileName)) {
                fs_1.unlinkSync(fileName);
            }
            fs_1.writeFileSync(fileName, promise());
        };
        return TsRenderer;
    }());
    TsRendering.TsRenderer = TsRenderer;
})(TsRendering || (TsRendering = {}));
exports.TsRenderer = TsRendering.TsRenderer;
module.exports = TsRendering;
//# sourceMappingURL=TsRenderer.js.map