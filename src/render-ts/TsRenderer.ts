import { readdirSync, statSync, writeFileSync, unlinkSync, existsSync } from 'fs';
import { join, relative } from 'path';

module TsRendering {
    export class TsRenderer {
        constructor(public root: string) {}

        public render(): void {
            //console.log(TsRenderer.listFilesRecursively(this.root));
            TsRenderer.listFilesRecursively(this.root)
                .forEach(TsRenderer.renderModule);
        }

        private static listFilesRecursively(dir: string): string[] {
            return readdirSync(dir)
                .map(entry => join(dir, entry))
                .map(fullPathEntry => statSync(fullPathEntry).isDirectory()
                    ? TsRenderer.listFilesRecursively(fullPathEntry) : [fullPathEntry] )
                .reduce( (previousValue, currentValue, _) => previousValue.concat(currentValue), [] )
                .filter(fullPathEntry => fullPathEntry.lastIndexOf('render-ts.ts') > 0);
        }

        private static renderModule(moduleFile: string): void {
            let moduleName = relative(__dirname, moduleFile.replace('render-ts.ts', 'render-ts'));
            let renderedFileName = moduleFile.replace('.render-ts.ts', '.ts');
            if (existsSync(renderedFileName)) {
                unlinkSync(renderedFileName);
            }
            TsRenderer.renderPromise(require(moduleName).render, renderedFileName);
        }

        private static renderPromise(promise: () => string, fileName: string): void {
            if (existsSync(fileName)) {
                unlinkSync(fileName);
            }
            writeFileSync(fileName, promise());
        }
    }
}

export = TsRendering;
exports.TsRenderer = TsRendering.TsRenderer;