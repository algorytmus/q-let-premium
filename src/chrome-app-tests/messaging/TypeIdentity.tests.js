"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="./../../node_modules/mocha-typescript/globals.d.ts" />
var mocha_typescript_1 = require("mocha-typescript");
/// <reference path="./../../node_modules/@types/chai/index.d.ts" />
var chai_1 = require("chai");
/// <reference path="./../../chrome-app/messaging/TypeIdentity.ts" />
var TypeIdentity_1 = require("./../../chrome-app/messaging/TypeIdentity");
var TestIdentityClass1 = /** @class */ (function () {
    function TestIdentityClass1() {
    }
    return TestIdentityClass1;
}());
var TestIdentityClass2 = /** @class */ (function () {
    function TestIdentityClass2() {
    }
    return TestIdentityClass2;
}());
var TestIdentityGenericClass1 = /** @class */ (function () {
    function TestIdentityGenericClass1() {
    }
    return TestIdentityGenericClass1;
}());
var TestIdentityGenericClass2 = /** @class */ (function () {
    function TestIdentityGenericClass2() {
    }
    return TestIdentityGenericClass2;
}());
var TypeIdentityTestSuite = /** @class */ (function () {
    function TypeIdentityTestSuite() {
    }
    TypeIdentityTestSuite.prototype.primitiveIdentityEqual = function () {
        var typeIdentity = TypeIdentity_1.TypeIdentity.createIdentity(TestIdentityClass1.prototype);
        chai_1.assert.isTrue(typeIdentity.isSameAs(typeIdentity), 'Identity same.');
    };
    TypeIdentityTestSuite.prototype.primitiveIdentitiesEqual = function () {
        var typeIdentity1 = TypeIdentity_1.TypeIdentity.createIdentity(TestIdentityClass1.prototype);
        var typeIdentity2 = TypeIdentity_1.TypeIdentity.createIdentity(TestIdentityClass1.prototype);
        chai_1.assert.isTrue(typeIdentity1.isSameAs(typeIdentity2), 'Identities same.');
    };
    TypeIdentityTestSuite.prototype.primitiveIdentityDeserializedEqual = function () {
        var typeIdentity = TypeIdentity_1.TypeIdentity.createIdentity(TestIdentityClass1.prototype);
        var typeIdentityDeserialized = TypeIdentity_1.TypeIdentityUntyped.deserialize(typeIdentity.serialize());
        chai_1.assert.isTrue(typeIdentityDeserialized.isSameAs(typeIdentity), 'Identities deserialized same.');
    };
    TypeIdentityTestSuite.prototype.primitiveIdentitiesDifferent = function () {
        var testIdentityClass1Identity = TypeIdentity_1.TypeIdentity.createIdentity(TestIdentityClass1.prototype);
        var testIdentityClass2Identity = TypeIdentity_1.TypeIdentity.createIdentity(TestIdentityClass2.prototype);
        chai_1.assert.isFalse(testIdentityClass1Identity.isSameAs(testIdentityClass2Identity), "Identity " + testIdentityClass1Identity.identity + " should be different than " + testIdentityClass2Identity.identity + ".");
    };
    TypeIdentityTestSuite.prototype.primitiveIdentitiesDeserializedDifferent = function () {
        var typeIdentity1 = TypeIdentity_1.TypeIdentity.createIdentity(TestIdentityClass1.prototype);
        var typeIdentity1Deserialized = TypeIdentity_1.TypeIdentityUntyped.deserialize(typeIdentity1.serialize());
        var typeIdentity2 = TypeIdentity_1.TypeIdentity.createIdentity(TestIdentityClass2.prototype);
        var typeIdentity2Deserialized = TypeIdentity_1.TypeIdentityUntyped.deserialize(typeIdentity2.serialize());
        var testIdentityClass2Identity = TypeIdentity_1.TypeIdentity.createIdentity(TestIdentityClass2.prototype);
        chai_1.assert.isFalse(typeIdentity1Deserialized.isSameAs(typeIdentity2Deserialized), "Identity " + typeIdentity1.identity + " should be different than " + typeIdentity2.identity + ".");
    };
    TypeIdentityTestSuite.prototype.complexIdentityEqual = function () {
        var typeIdentity = TypeIdentity_1.TypeIdentity
            .createIdentityComplex(TestIdentityGenericClass1.prototype, TypeIdentity_1.TypeIdentity.createIdentity(TestIdentityClass1.prototype));
        chai_1.assert.isTrue(typeIdentity.isSameAs(typeIdentity), 'Identity same.');
    };
    TypeIdentityTestSuite.prototype.complexIdentitiesEqual = function () {
        var typeIdentity1 = TypeIdentity_1.TypeIdentity
            .createIdentityComplex(TestIdentityGenericClass1.prototype, TypeIdentity_1.TypeIdentity.createIdentity(TestIdentityClass1.prototype));
        var typeIdentity2 = TypeIdentity_1.TypeIdentity
            .createIdentityComplex(TestIdentityGenericClass1.prototype, TypeIdentity_1.TypeIdentity.createIdentity(TestIdentityClass1.prototype));
        chai_1.assert.isTrue(typeIdentity1.isSameAs(typeIdentity2), 'Identities same.');
    };
    TypeIdentityTestSuite.prototype.complexIdentityDeserializedEqual = function () {
        var typeIdentity = TypeIdentity_1.TypeIdentity
            .createIdentityComplex(TestIdentityGenericClass1.prototype, TypeIdentity_1.TypeIdentity.createIdentity(TestIdentityClass1.prototype));
        var typeIdentityDeserialized = TypeIdentity_1.TypeIdentityUntyped.deserialize(typeIdentity.serialize());
        chai_1.assert.isTrue(typeIdentityDeserialized.isSameAs(typeIdentity), 'Identities deserialized same.');
    };
    TypeIdentityTestSuite.prototype.complexIdentitiesDifferent = function () {
        var testIdentityClass1Identity = TypeIdentity_1.TypeIdentity
            .createIdentityComplex(TestIdentityGenericClass1.prototype, TypeIdentity_1.TypeIdentity.createIdentity(TestIdentityClass1));
        var testIdentityClass2Identity = TypeIdentity_1.TypeIdentity
            .createIdentityComplex(TestIdentityGenericClass1.prototype, TypeIdentity_1.TypeIdentity.createIdentity(TestIdentityClass2.prototype));
        chai_1.assert.isFalse(testIdentityClass1Identity.isSameAs(testIdentityClass2Identity), "Identity " + testIdentityClass1Identity.identity + " should be different than " + testIdentityClass2Identity.identity + ".");
    };
    TypeIdentityTestSuite.prototype.complexIdentitiesDeserializedDifferent = function () {
        var testIdentityClass1Identity = TypeIdentity_1.TypeIdentity
            .createIdentityComplex(TestIdentityGenericClass1.prototype, TypeIdentity_1.TypeIdentity.createIdentity(TestIdentityClass1));
        var typeIdentity1Deserialized = TypeIdentity_1.TypeIdentityUntyped.deserialize(testIdentityClass1Identity.serialize());
        var testIdentityClass2Identity = TypeIdentity_1.TypeIdentity
            .createIdentityComplex(TestIdentityGenericClass1.prototype, TypeIdentity_1.TypeIdentity.createIdentity(TestIdentityClass2.prototype));
        var typeIdentity2Deserialized = TypeIdentity_1.TypeIdentityUntyped.deserialize(testIdentityClass2Identity.serialize());
        chai_1.assert.isFalse(typeIdentity1Deserialized.isSameAs(typeIdentity2Deserialized), "Identity " + testIdentityClass1Identity.identity + " should be different than " + testIdentityClass2Identity.identity + ".");
    };
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], TypeIdentityTestSuite.prototype, "primitiveIdentityEqual", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], TypeIdentityTestSuite.prototype, "primitiveIdentitiesEqual", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], TypeIdentityTestSuite.prototype, "primitiveIdentityDeserializedEqual", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], TypeIdentityTestSuite.prototype, "primitiveIdentitiesDifferent", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], TypeIdentityTestSuite.prototype, "primitiveIdentitiesDeserializedDifferent", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], TypeIdentityTestSuite.prototype, "complexIdentityEqual", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], TypeIdentityTestSuite.prototype, "complexIdentitiesEqual", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], TypeIdentityTestSuite.prototype, "complexIdentityDeserializedEqual", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], TypeIdentityTestSuite.prototype, "complexIdentitiesDifferent", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], TypeIdentityTestSuite.prototype, "complexIdentitiesDeserializedDifferent", null);
    TypeIdentityTestSuite = __decorate([
        mocha_typescript_1.suite
    ], TypeIdentityTestSuite);
    return TypeIdentityTestSuite;
}());
//# sourceMappingURL=TypeIdentity.tests.js.map