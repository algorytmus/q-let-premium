// tslint:disable-next-line:no-eval
eval('var Rx = this');

import { Observable } from './../../chrome-app/scripts/rxjs/rx.all.js';
import { ClientId } from './../../chrome-app/messaging/ClientId';
import { RawSubscribtion } from './../../chrome-app/messaging/RawSubscribtion';

import { TestWindow } from './TestWindow';
import { IMessageWindow } from './../../chrome-app/messaging/IMessageWindow';
import { MessageWindow } from './../../chrome-app/messaging/MessageWindow';

/// <reference path="./../../node_modules/mocha-typescript/globals.d.ts" />
import {it, suite,  test,  slow,  timeout} from 'mocha-typescript';

/// <reference path="./../../node_modules/@types/chai/index.d.ts" />
import { assert } from 'chai';

/// <reference path="./../../node_modules/moq.ts/index.d.ts" />
import {Mock, It, Times, ExpectedGetPropertyExpression, MockBehavior} from './../../node_modules/moq.ts/index';

/// <reference path="./TestMessage.ts" />
import { TestMessage } from './TestMessage';

/// <reference path="./../../chrome-app/messaging/RawMessageBus.ts" />
import { RawMessageBus } from './../../chrome-app/messaging/RawMessageBus';

/// <reference path="./../../chrome-app/messaging/MessageBus.ts" />
import { MessageBus } from './../../chrome-app/messaging/MessageBus';

/// <reference path="./../../chrome-app/messaging/TypeIdentity.ts" />
import { TypeIdentity, TypeIdentityUntypedSerialized } from './../../chrome-app/messaging/TypeIdentity';

@suite
class MessageBusTestSuite {

    static createMessageBus(window: IWindow): MessageBus {
        const messageWindow: MessageWindow = new MessageWindow(window);
        const rawMessageBus = new RawMessageBus(messageWindow);
        return new MessageBus(rawMessageBus);
    }

    @test async observe() {
        const testWindow: TestWindow = new TestWindow();
        const messageWindow: MessageWindow = new MessageWindow(testWindow);
        const rawMessageBus = new RawMessageBus(messageWindow);
        const messageBus: MessageBus = new MessageBus(rawMessageBus);

        let results = rawMessageBus
            .observeData<RawSubscribtion<TestMessage>>(TypeIdentity.createIdentityComplex(RawSubscribtion.prototype, TestMessage.identity))
            .first()
            .do(sub => assert.containsAllDeepKeys(sub, new RawSubscribtion<TestMessage>(new ClientId())))
            .toPromise();

        messageBus
            .observe(TestMessage.identity)
            .subscribe();

        await results;
    }

    @test async observeRequestsOnTheSameMessageBus() {
        const testWindow: TestWindow = new TestWindow();
        const messageBusServer: MessageBus = MessageBusTestSuite.createMessageBus(testWindow);

        messageBusServer.observeRequests(TestMessage.identity)
            .subscribe(observer => { observer.onNext(new TestMessage('test message')); } );

        let results = messageBusServer.observe(TestMessage.identity)
            .first()
            .do(actual => assert.deepEqual(actual, new TestMessage('test message')))
            .toPromise();

        await results;
    }

    @test async observeRequestsOnTheDifferentMessageBus() {
        const testWindow: TestWindow = new TestWindow();
        const messageBusServer: MessageBus = MessageBusTestSuite.createMessageBus(testWindow);
        const messageBusClient: MessageBus = MessageBusTestSuite.createMessageBus(testWindow);

        messageBusClient.observeRequests(TestMessage.identity)
            .subscribe(observer => { observer.onNext(new TestMessage('test message')); } );

        let results = messageBusServer.observe(TestMessage.identity)
            .first()
            .do(actual => assert.deepEqual(actual, new TestMessage('test message')))
            .toPromise();

        await results;
    }
}