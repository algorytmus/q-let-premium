import * as Rx from './../../node_modules/rxjs/Rx.d';


import { ClientId } from './../../chrome-app/messaging/ClientId';
import { TestWindow } from './TestWindow';
import { IMessageWindow } from './../../chrome-app/messaging/IMessageWindow';
import { MessageWindow } from './../../chrome-app/messaging/MessageWindow';

/// <reference path="./../../node_modules/mocha-typescript/globals.d.ts" />
import { suite, test, slow, timeout } from 'mocha-typescript';

/// <reference path="./../../node_modules/@types/chai/index.d.ts" />
import { assert } from 'chai';

/// <reference path="./../../node_modules/moq.ts/index.d.ts" />
import {Mock, It, Times, ExpectedGetPropertyExpression, MockBehavior} from './../../node_modules/moq.ts/index';

/// <reference path="./TestMessage.ts" />
import { TestMessage } from './TestMessage';

/// <reference path="./../../chrome-app/messaging/RawMessageBus.ts" />
import { RawMessageBus } from './../../chrome-app/messaging/RawMessageBus';

/// <reference path="./../../chrome-app/messaging/MessageBus.ts" />
import { MessageBus } from './../../chrome-app/messaging/MessageBus';

/// <reference path="./../../chrome-app/messaging/TypeIdentity.ts" />
import { TypeIdentity, TypeIdentityUntypedSerialized } from './../../chrome-app/messaging/TypeIdentity';
import { RawResponse } from '../../chrome-app/messaging/RawResponse';

type RawMessageUnchecked = {identity: TypeIdentityUntypedSerialized | undefined, data: any | undefined};
type RawMessageUncheckedPacked = {data: RawMessageUnchecked};

@suite
class RawMessageBusTestSuite {
    @test sendData() {
        const testWindow: TestWindow = new TestWindow();
        const messageWindow: MessageWindow = new MessageWindow(testWindow);
        const rawMessageUnderTest = new RawMessageBus(messageWindow);
        let actual: any;
        testWindow.addEventListener('message', data => {actual = data; });
        let expected: TestMessage = new TestMessage('test message');
        rawMessageUnderTest.sendData(expected, TestMessage.identity);
        assert.equal(JSON.stringify(actual), JSON.stringify({ type: 'message', data: { identity: { identity : 'TestMessage' }, data: {data: 'test message'}}}));
    }

    @test receiveData() {
        const testWindow: TestWindow = new TestWindow();
        const messageWindow: MessageWindow = new MessageWindow(testWindow);
        const rawMessageUnderTest = new RawMessageBus(messageWindow);

        let actual: any;
        rawMessageUnderTest.observeData<TestMessage>(TestMessage.identity)
        .subscribe(d => actual = d);

        rawMessageUnderTest.sendData(new TestMessage('test message'), TestMessage.identity);

        assert.equal(JSON.stringify(actual), JSON.stringify(new TestMessage('test message')));
    }

    @test receiveComplexData() {
        const testWindow: TestWindow = new TestWindow();
        const messageWindow: MessageWindow = new MessageWindow(testWindow);
        const rawMessageUnderTest = new RawMessageBus(messageWindow);

        const complexIdentity: TypeIdentity<RawResponse<TestMessage>> = TypeIdentity.createIdentityComplex(RawResponse.prototype, TypeIdentity.createIdentity(TestMessage.prototype));
        let actual: RawResponse<TestMessage>;

        const clientId: ClientId = new ClientId();

        rawMessageUnderTest.observeData<RawResponse<TestMessage>>(complexIdentity)
            .subscribe(d => actual = d);

        rawMessageUnderTest.sendData(new RawResponse<TestMessage>(new TestMessage('test message'), clientId), complexIdentity);

        assert.equal(JSON.stringify(actual), JSON.stringify(new RawResponse<TestMessage>(new TestMessage('test message'), clientId)));
    }
}