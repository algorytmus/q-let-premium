"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CustomEvent = /** @class */ (function () {
    function CustomEvent() {
    }
    CustomEvent.prototype.initEvent = function (eventTypeArg, canBubbleArg, cancelableArg) {
        throw new Error("Method not implemented.");
    };
    CustomEvent.prototype.preventDefault = function () {
        throw new Error("Method not implemented.");
    };
    CustomEvent.prototype.stopImmediatePropagation = function () {
        throw new Error("Method not implemented.");
    };
    CustomEvent.prototype.stopPropagation = function () {
        throw new Error("Method not implemented.");
    };
    CustomEvent.prototype.deepPath = function () {
        throw new Error("Method not implemented.");
    };
    return CustomEvent;
}());
exports.CustomEvent = CustomEvent;
var TestWindow = /** @class */ (function () {
    function TestWindow() {
        this.listeners = {};
    }
    TestWindow.prototype.postMessage = function (message, targetOrigin, transfer) {
        var event = new CustomEvent();
        event.type = 'message';
        event.data = message;
        this.dispatchEvent(event);
    };
    TestWindow.prototype.addEventListener = function (type, listener, options) {
        if (this.listeners[type] === undefined) {
            this.listeners[type] = [listener];
        }
        else {
            this.listeners[type].push(listener);
        }
    };
    TestWindow.prototype.dispatchEvent = function (evt) {
        var eventListeners = this.listeners[evt.type];
        if (eventListeners != undefined) {
            eventListeners.forEach(function (eventListener) {
                if (eventListener === undefined) {
                    return;
                }
                if (eventListener) {
                    var el = eventListener;
                    el(evt);
                }
                else {
                    var el = eventListener;
                    el.handleEvent(evt);
                }
            });
        }
        return true;
    };
    TestWindow.prototype.removeEventListener = function (type, listener, options) {
        this.listeners[type];
    };
    return TestWindow;
}());
exports.TestWindow = TestWindow;
//# sourceMappingURL=TestWindow.js.map