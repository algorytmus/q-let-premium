/// <reference path="./../../node_modules/mocha-typescript/globals.d.ts" />
import { suite, test, slow, timeout } from 'mocha-typescript';

/// <reference path="./../../node_modules/@types/chai/index.d.ts" />
import { assert } from 'chai';

/// <reference path="./../../chrome-app/messaging/ClientId.ts" />
import { ClientId } from './../../chrome-app/messaging/ClientId';

@suite
class ClientIdTestSuite {
    @test idLength() {
        let clientId = new ClientId();
        assert.equal(clientId.id.length, 36, 'Expected client id to be of certain length.');
    }

    @test differentIds() {
        let clientId1 = new ClientId();
        let clientId2 = new ClientId();
        assert.notEqual(clientId1.id, clientId2.id, 'Expected client ids to be different.');
    }
}