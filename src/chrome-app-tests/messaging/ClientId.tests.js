"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="./../../node_modules/mocha-typescript/globals.d.ts" />
var mocha_typescript_1 = require("mocha-typescript");
/// <reference path="./../../node_modules/@types/chai/index.d.ts" />
var chai_1 = require("chai");
/// <reference path="./../../chrome-app/messaging/ClientId.ts" />
var ClientId_1 = require("./../../chrome-app/messaging/ClientId");
var ClientIdTestSuite = /** @class */ (function () {
    function ClientIdTestSuite() {
    }
    ClientIdTestSuite.prototype.idLength = function () {
        var clientId = new ClientId_1.ClientId();
        chai_1.assert.equal(clientId.id.length, 36, 'Expected client id to be of certain length.');
    };
    ClientIdTestSuite.prototype.differentIds = function () {
        var clientId1 = new ClientId_1.ClientId();
        var clientId2 = new ClientId_1.ClientId();
        chai_1.assert.notEqual(clientId1.id, clientId2.id, 'Expected client ids to be different.');
    };
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], ClientIdTestSuite.prototype, "idLength", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], ClientIdTestSuite.prototype, "differentIds", null);
    ClientIdTestSuite = __decorate([
        mocha_typescript_1.suite
    ], ClientIdTestSuite);
    return ClientIdTestSuite;
}());
//# sourceMappingURL=ClientId.tests.js.map