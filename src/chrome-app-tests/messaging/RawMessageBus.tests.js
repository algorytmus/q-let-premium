"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var ClientId_1 = require("./../../chrome-app/messaging/ClientId");
var TestWindow_1 = require("./TestWindow");
var MessageWindow_1 = require("./../../chrome-app/messaging/MessageWindow");
/// <reference path="./../../node_modules/mocha-typescript/globals.d.ts" />
var mocha_typescript_1 = require("mocha-typescript");
/// <reference path="./../../node_modules/@types/chai/index.d.ts" />
var chai_1 = require("chai");
/// <reference path="./TestMessage.ts" />
var TestMessage_1 = require("./TestMessage");
/// <reference path="./../../chrome-app/messaging/RawMessageBus.ts" />
var RawMessageBus_1 = require("./../../chrome-app/messaging/RawMessageBus");
/// <reference path="./../../chrome-app/messaging/TypeIdentity.ts" />
var TypeIdentity_1 = require("./../../chrome-app/messaging/TypeIdentity");
var RawResponse_1 = require("../../chrome-app/messaging/RawResponse");
var RawMessageBusTestSuite = /** @class */ (function () {
    function RawMessageBusTestSuite() {
    }
    RawMessageBusTestSuite.prototype.sendData = function () {
        var testWindow = new TestWindow_1.TestWindow();
        var messageWindow = new MessageWindow_1.MessageWindow(testWindow);
        var rawMessageUnderTest = new RawMessageBus_1.RawMessageBus(messageWindow);
        var actual;
        testWindow.addEventListener('message', function (data) { actual = data; });
        var expected = new TestMessage_1.TestMessage('test message');
        rawMessageUnderTest.sendData(expected, TestMessage_1.TestMessage.identity);
        chai_1.assert.equal(JSON.stringify(actual), JSON.stringify({ type: 'message', data: { identity: { identity: 'TestMessage' }, data: { data: 'test message' } } }));
    };
    RawMessageBusTestSuite.prototype.receiveData = function () {
        var testWindow = new TestWindow_1.TestWindow();
        var messageWindow = new MessageWindow_1.MessageWindow(testWindow);
        var rawMessageUnderTest = new RawMessageBus_1.RawMessageBus(messageWindow);
        var actual;
        rawMessageUnderTest.observeData(TestMessage_1.TestMessage.identity)
            .subscribe(function (d) { return actual = d; });
        rawMessageUnderTest.sendData(new TestMessage_1.TestMessage('test message'), TestMessage_1.TestMessage.identity);
        chai_1.assert.equal(JSON.stringify(actual), JSON.stringify(new TestMessage_1.TestMessage('test message')));
    };
    RawMessageBusTestSuite.prototype.receiveComplexData = function () {
        var testWindow = new TestWindow_1.TestWindow();
        var messageWindow = new MessageWindow_1.MessageWindow(testWindow);
        var rawMessageUnderTest = new RawMessageBus_1.RawMessageBus(messageWindow);
        var complexIdentity = TypeIdentity_1.TypeIdentity.createIdentityComplex(RawResponse_1.RawResponse.prototype, TypeIdentity_1.TypeIdentity.createIdentity(TestMessage_1.TestMessage.prototype));
        var actual;
        var clientId = new ClientId_1.ClientId();
        rawMessageUnderTest.observeData(complexIdentity)
            .subscribe(function (d) { return actual = d; });
        rawMessageUnderTest.sendData(new RawResponse_1.RawResponse(new TestMessage_1.TestMessage('test message'), clientId), complexIdentity);
        chai_1.assert.equal(JSON.stringify(actual), JSON.stringify(new RawResponse_1.RawResponse(new TestMessage_1.TestMessage('test message'), clientId)));
    };
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], RawMessageBusTestSuite.prototype, "sendData", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], RawMessageBusTestSuite.prototype, "receiveData", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], RawMessageBusTestSuite.prototype, "receiveComplexData", null);
    RawMessageBusTestSuite = __decorate([
        mocha_typescript_1.suite
    ], RawMessageBusTestSuite);
    return RawMessageBusTestSuite;
}());
//# sourceMappingURL=RawMessageBus.tests.js.map