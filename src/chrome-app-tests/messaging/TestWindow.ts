
declare var EventTarget: {
    prototype: EventTarget;
    new(): EventTarget;
};

export class CustomEvent implements Event {
    bubbles: boolean;
    cancelable: boolean;
    cancelBubble: boolean;
    currentTarget: EventTarget;
    defaultPrevented: boolean;
    eventPhase: number;
    isTrusted: boolean;
    returnValue: boolean;
    srcElement: Element;
    target: EventTarget;
    timeStamp: number;
    type: string;
    scoped: boolean;
    data: any;
    initEvent(eventTypeArg: string, canBubbleArg: boolean, cancelableArg: boolean): void {
        throw new Error("Method not implemented.");
    }
    preventDefault(): void {
        throw new Error("Method not implemented.");
    }
    stopImmediatePropagation(): void {
        throw new Error("Method not implemented.");
    }
    stopPropagation(): void {
        throw new Error("Method not implemented.");
    }
    deepPath(): EventTarget[] {
        throw new Error("Method not implemented.");
    }
    AT_TARGET: number;
    BUBBLING_PHASE: number;
    CAPTURING_PHASE: number;
}

export class TestWindow implements IWebAccessibleWindow {
    public Quizlet: Quizlet;

    private listeners: { [id: string]: [EventListenerOrEventListenerObject]; } = {};

    postMessage(message: any, targetOrigin: string, transfer?: any[]): void {
        let event = new CustomEvent();
        event.type = 'message';
        event.data = message;
        this.dispatchEvent(event);
    }

    addEventListener(type: string, listener?: EventListenerOrEventListenerObject, options?: boolean | AddEventListenerOptions): void {
        if (this.listeners[type] === undefined) {
            this.listeners[type] = [listener];
        } else {
            this.listeners[type].push(listener);
        }
    }

    dispatchEvent(evt: Event): boolean {
        let eventListeners: [EventListener | EventListenerObject] = this.listeners[evt.type];
        if(eventListeners != undefined)
        {
            eventListeners.forEach(eventListener => {
                if (eventListener === undefined) {
                    return;
                }
                if (eventListener as EventListener) {
                    let el: EventListener = <EventListener> eventListener;
                    el(evt);
                } else {
                    let el: EventListenerObject = <EventListenerObject> eventListener;
                    el.handleEvent(evt);
                }
            });
        }
        return true;
    }

    removeEventListener(type: string, listener?: EventListenerOrEventListenerObject, options?: boolean | EventListenerOptions): void {
        this.listeners[type];
    }
}