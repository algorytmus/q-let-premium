"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TypeIdentity_1 = require("../../chrome-app/messaging/TypeIdentity");
var TestMessage = /** @class */ (function () {
    function TestMessage(data) {
        this.data = data;
    }
    Object.defineProperty(TestMessage, "identity", {
        get: function () {
            return TypeIdentity_1.TypeIdentity.createIdentity(TestMessage.prototype);
        },
        enumerable: true,
        configurable: true
    });
    return TestMessage;
}());
exports.TestMessage = TestMessage;
//# sourceMappingURL=TestMessage.js.map