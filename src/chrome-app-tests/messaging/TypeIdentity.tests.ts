/// <reference path="./../../node_modules/mocha-typescript/globals.d.ts" />
import { suite, test, slow, timeout } from 'mocha-typescript';

/// <reference path="./../../node_modules/@types/chai/index.d.ts" />
import { assert } from 'chai';

/// <reference path="./../../chrome-app/messaging/TypeIdentity.ts" />
import { TypeIdentity, TypeIdentityUntyped } from './../../chrome-app/messaging/TypeIdentity';

/// <reference path="./TestMessage.ts" />
import { TestMessage } from './TestMessage';

class TestIdentityClass1 {}
class TestIdentityClass2 {}

class TestIdentityGenericClass1<T> {}
class TestIdentityGenericClass2<T> {}

@suite
class TypeIdentityTestSuite {

    @test primitiveIdentityEqual() {
        let typeIdentity = TypeIdentity.createIdentity(TestIdentityClass1.prototype);
        assert.isTrue(typeIdentity.isSameAs(typeIdentity), 'Identity same.');
    }

    @test primitiveIdentitiesEqual() {
        let typeIdentity1 = TypeIdentity.createIdentity(TestIdentityClass1.prototype);
        let typeIdentity2 = TypeIdentity.createIdentity(TestIdentityClass1.prototype);
        assert.isTrue(typeIdentity1.isSameAs(typeIdentity2), 'Identities same.');
    }

    @test primitiveIdentityDeserializedEqual() {
        let typeIdentity = TypeIdentity.createIdentity(TestIdentityClass1.prototype);
        let typeIdentityDeserialized: TypeIdentityUntyped = TypeIdentityUntyped.deserialize(typeIdentity.serialize());
        assert.isTrue(typeIdentityDeserialized.isSameAs(typeIdentity), 'Identities deserialized same.');
    }

    @test primitiveIdentitiesDifferent() {
        let testIdentityClass1Identity = TypeIdentity.createIdentity(TestIdentityClass1.prototype);
        let testIdentityClass2Identity = TypeIdentity.createIdentity(TestIdentityClass2.prototype);
        assert.isFalse(testIdentityClass1Identity.isSameAs(testIdentityClass2Identity), `Identity ${testIdentityClass1Identity.identity} should be different than ${testIdentityClass2Identity.identity}.`);
    }

    @test primitiveIdentitiesDeserializedDifferent() {

        let typeIdentity1 = TypeIdentity.createIdentity(TestIdentityClass1.prototype);
        let typeIdentity1Deserialized: TypeIdentityUntyped = TypeIdentityUntyped.deserialize(typeIdentity1.serialize());

        let typeIdentity2 = TypeIdentity.createIdentity(TestIdentityClass2.prototype);
        let typeIdentity2Deserialized: TypeIdentityUntyped = TypeIdentityUntyped.deserialize(typeIdentity2.serialize());

        let testIdentityClass2Identity = TypeIdentity.createIdentity(TestIdentityClass2.prototype);
        assert.isFalse(typeIdentity1Deserialized.isSameAs(typeIdentity2Deserialized), `Identity ${typeIdentity1.identity} should be different than ${typeIdentity2.identity}.`);
    }

    @test complexIdentityEqual() {
        let typeIdentity = TypeIdentity
            .createIdentityComplex<TestIdentityGenericClass1<TestIdentityClass1>, TestIdentityClass1>
            (TestIdentityGenericClass1.prototype, TypeIdentity.createIdentity(TestIdentityClass1.prototype));
        assert.isTrue(typeIdentity.isSameAs(typeIdentity), 'Identity same.');
    }

    @test complexIdentitiesEqual() {
        let typeIdentity1 = TypeIdentity
            .createIdentityComplex<TestIdentityGenericClass1<TestIdentityClass1>, TestIdentityClass1>
            (TestIdentityGenericClass1.prototype, TypeIdentity.createIdentity(TestIdentityClass1.prototype));
        let typeIdentity2 = TypeIdentity
            .createIdentityComplex<TestIdentityGenericClass1<TestIdentityClass1>, TestIdentityClass1>
            (TestIdentityGenericClass1.prototype, TypeIdentity.createIdentity(TestIdentityClass1.prototype));
        assert.isTrue(typeIdentity1.isSameAs(typeIdentity2), 'Identities same.');
    }

    @test complexIdentityDeserializedEqual() {
        let typeIdentity = TypeIdentity
            .createIdentityComplex<TestIdentityGenericClass1<TestIdentityClass1>, TestIdentityClass1>
            (TestIdentityGenericClass1.prototype, TypeIdentity.createIdentity(TestIdentityClass1.prototype));
        let typeIdentityDeserialized: TypeIdentityUntyped = TypeIdentityUntyped.deserialize(typeIdentity.serialize());
        assert.isTrue(typeIdentityDeserialized.isSameAs(typeIdentity), 'Identities deserialized same.');
    }

    @test complexIdentitiesDifferent() {
        let testIdentityClass1Identity = TypeIdentity
            .createIdentityComplex<TestIdentityGenericClass1<TestIdentityClass1>, TestIdentityClass1>
            (TestIdentityGenericClass1.prototype, TypeIdentity.createIdentity(TestIdentityClass1));
        let testIdentityClass2Identity = TypeIdentity
            .createIdentityComplex<TestIdentityGenericClass1<TestIdentityClass2>, TestIdentityClass2>
            (TestIdentityGenericClass1.prototype, TypeIdentity.createIdentity(TestIdentityClass2.prototype));
        assert.isFalse(testIdentityClass1Identity.isSameAs(testIdentityClass2Identity), `Identity ${testIdentityClass1Identity.identity} should be different than ${testIdentityClass2Identity.identity}.`);
    }

    @test complexIdentitiesDeserializedDifferent() {
        let testIdentityClass1Identity = TypeIdentity
            .createIdentityComplex<TestIdentityGenericClass1<TestIdentityClass1>, TestIdentityClass1>
            (TestIdentityGenericClass1.prototype, TypeIdentity.createIdentity(TestIdentityClass1));
        let typeIdentity1Deserialized: TypeIdentityUntyped = TypeIdentityUntyped.deserialize(testIdentityClass1Identity.serialize());

        let testIdentityClass2Identity = TypeIdentity
            .createIdentityComplex<TestIdentityGenericClass1<TestIdentityClass2>, TestIdentityClass2>
            (TestIdentityGenericClass1.prototype, TypeIdentity.createIdentity(TestIdentityClass2.prototype));
        let typeIdentity2Deserialized: TypeIdentityUntyped = TypeIdentityUntyped.deserialize(testIdentityClass2Identity.serialize());

        assert.isFalse(typeIdentity1Deserialized.isSameAs(typeIdentity2Deserialized), `Identity ${testIdentityClass1Identity.identity} should be different than ${testIdentityClass2Identity.identity}.`);
    }
}