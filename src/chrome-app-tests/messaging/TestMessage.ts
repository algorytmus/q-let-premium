import { TypeIdentity } from '../../chrome-app/messaging/TypeIdentity';

export class TestMessage {
    constructor(public readonly data: string) {}

    public static get identity(): TypeIdentity<TestMessage> {
        return TypeIdentity.createIdentity(TestMessage.prototype);
    }
}