
import { WritePageQuizletDataMessage } from './../../chrome-app/data/WritePageQuizletDataMessage';
import { TypeIdentity } from './../../chrome-app/messaging/TypeIdentity';
import { MessageBus } from './../../chrome-app/messaging/MessageBus';
import { TestWindow } from './../messaging/TestWindow';
/// <reference path="./../../node_modules/mocha-typescript/globals.d.ts" />
import { suite, test, slow, timeout } from 'mocha-typescript';

/// <reference path="./../../node_modules/@types/chai/index.d.ts" />
import { assert } from 'chai';

/// <reference path="./../../chrome-app/data/QuizletDataProvider.ts" />
import { QuizletDataProvider } from './../../chrome-app/data/QuizletDataProvider';
import { MessageWindow } from '../../chrome-app/messaging/MessageWindow';
import { RawMessageBus } from '../../chrome-app/messaging/RawMessageBus';

@suite
class QuizletDataProviderTestSuite {
    @test async dataReceived() {
        const window = new TestWindow();
        const messageWindow: MessageWindow = new MessageWindow(window);
        const rawMessageBus = new RawMessageBus(messageWindow);
        const messageBus = new MessageBus(rawMessageBus);
        const quizletDataProvider: QuizletDataProvider = new QuizletDataProvider(messageBus);
        const quizlet = {learnGameData:
            {
                allTerms: undefined,
                audioOn: undefined,
                autofocus: undefined,
                canEditSome: undefined,
                correctCount: undefined,
                definitionAccents: undefined,
                definitionSideLabel: undefined,
                hardLangs: undefined,
                incorrect: undefined,
                isEmbedding: undefined,
                promptOrder: undefined,
                remaining: undefined,
                round: undefined,
                roundTotal: undefined,
                selectedOnly: undefined,
                selectedOnlyStartOverPath: undefined,
                session: undefined,
                sessionTermIds: undefined,
                sessionTypeConstant: undefined,
                startOverPath: undefined,
                studyableId: undefined,
                studyableType: undefined,
                thirdSideAccents: undefined,
                thirdSideLabel: undefined,
                wordAccents: undefined,
                wordSideLabel: undefined
            }};

        messageBus
            .observeRequests(TypeIdentity.createIdentity(WritePageQuizletDataMessage.prototype))
            .map(observer => observer.onNext(new WritePageQuizletDataMessage(quizlet)) )
            .subscribe();

        let results = quizletDataProvider.Quizlet
            .first()
            .do(actual => assert.deepEqual(actual, quizlet))
            .toPromise();

        await results;
    }
}