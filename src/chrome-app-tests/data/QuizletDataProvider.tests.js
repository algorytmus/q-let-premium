"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var WritePageQuizletDataMessage_1 = require("./../../chrome-app/data/WritePageQuizletDataMessage");
var TypeIdentity_1 = require("./../../chrome-app/messaging/TypeIdentity");
var MessageBus_1 = require("./../../chrome-app/messaging/MessageBus");
var TestWindow_1 = require("./../messaging/TestWindow");
/// <reference path="./../../node_modules/mocha-typescript/globals.d.ts" />
var mocha_typescript_1 = require("mocha-typescript");
/// <reference path="./../../node_modules/@types/chai/index.d.ts" />
var chai_1 = require("chai");
/// <reference path="./../../chrome-app/data/QuizletDataProvider.ts" />
var QuizletDataProvider_1 = require("./../../chrome-app/data/QuizletDataProvider");
var MessageWindow_1 = require("../../chrome-app/messaging/MessageWindow");
var RawMessageBus_1 = require("../../chrome-app/messaging/RawMessageBus");
var QuizletDataProviderTestSuite = /** @class */ (function () {
    function QuizletDataProviderTestSuite() {
    }
    QuizletDataProviderTestSuite.prototype.dataReceived = function () {
        return __awaiter(this, void 0, void 0, function () {
            var window, messageWindow, rawMessageBus, messageBus, quizletDataProvider, quizlet, results;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        window = new TestWindow_1.TestWindow();
                        messageWindow = new MessageWindow_1.MessageWindow(window);
                        rawMessageBus = new RawMessageBus_1.RawMessageBus(messageWindow);
                        messageBus = new MessageBus_1.MessageBus(rawMessageBus);
                        quizletDataProvider = new QuizletDataProvider_1.QuizletDataProvider(messageBus);
                        quizlet = { learnGameData: {
                                allTerms: undefined,
                                audioOn: undefined,
                                autofocus: undefined,
                                canEditSome: undefined,
                                correctCount: undefined,
                                definitionAccents: undefined,
                                definitionSideLabel: undefined,
                                hardLangs: undefined,
                                incorrect: undefined,
                                isEmbedding: undefined,
                                promptOrder: undefined,
                                remaining: undefined,
                                round: undefined,
                                roundTotal: undefined,
                                selectedOnly: undefined,
                                selectedOnlyStartOverPath: undefined,
                                session: undefined,
                                sessionTermIds: undefined,
                                sessionTypeConstant: undefined,
                                startOverPath: undefined,
                                studyableId: undefined,
                                studyableType: undefined,
                                thirdSideAccents: undefined,
                                thirdSideLabel: undefined,
                                wordAccents: undefined,
                                wordSideLabel: undefined
                            } };
                        messageBus
                            .observeRequests(TypeIdentity_1.TypeIdentity.createIdentity(WritePageQuizletDataMessage_1.WritePageQuizletDataMessage.prototype))
                            .map(function (observer) { return observer.onNext(new WritePageQuizletDataMessage_1.WritePageQuizletDataMessage(quizlet)); })
                            .subscribe();
                        results = quizletDataProvider.Quizlet
                            .first()
                            .do(function (actual) { return chai_1.assert.deepEqual(actual, quizlet); })
                            .toPromise();
                        return [4 /*yield*/, results];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], QuizletDataProviderTestSuite.prototype, "dataReceived", null);
    QuizletDataProviderTestSuite = __decorate([
        mocha_typescript_1.suite
    ], QuizletDataProviderTestSuite);
    return QuizletDataProviderTestSuite;
}());
//# sourceMappingURL=QuizletDataProvider.tests.js.map