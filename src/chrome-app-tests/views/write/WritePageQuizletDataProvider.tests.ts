import { WritePageQuizletDataMessage } from './../../../chrome-app/data/WritePageQuizletDataMessage';
import { TypeIdentity } from './../../../chrome-app/messaging/TypeIdentity';
import { MessageBus } from './../../../chrome-app/messaging/MessageBus';
import { TestWindow } from './../../messaging/TestWindow';
/// <reference path="./../../node_modules/mocha-typescript/globals.d.ts" />
import { suite, test, slow, timeout } from 'mocha-typescript';

import { Observer, Subject, Observable } from './../../../chrome-app/scripts/rxjs/rx.all';
import * as Rx from './../../../chrome-app/scripts/rxjs/rx.all';

/// <reference path="./../../../chrome-app/scripts/jquery/jquery.d.ts" />
/// <reference path="./../../../chrome-app/scripts/rxjs/ts/rx.all.d.ts" />
/// <reference path="./../../../chrome-app/scripts/rxjs-jquery/ts/rx.jquery.d.ts" />
/// <reference path="./../../../chrome-app/scripts/rx-dom/rx-dom.d.ts" />

/// <reference path="./../../node_modules/@types/chai/index.d.ts" />
import { assert } from 'chai';

/// <reference path="./../../chrome-app/views/write/WritePageQuizletDataProvider.ts" />
import { WritePageQuizletDataProvider } from './../../../chrome-app/views/write/WritePageQuizletDataProvider';
import { MessageWindow } from '../../../chrome-app/messaging/MessageWindow';
import { RawMessageBus } from '../../../chrome-app/messaging/RawMessageBus';

@suite
class WritePageQuizletDataProviderTestSuite {
    @test async dataPublished() {
        const window = new TestWindow();
        const messageWindow: MessageWindow = new MessageWindow(window);
        const rawMessageBus = new RawMessageBus(messageWindow);
        const messageBus = new MessageBus(rawMessageBus);

        const dom = {
            observeInitialized(): Rx.Observable<any> {
                return Rx.Observable.interval(10);
            },
            observeChanged(): Rx.Observable<any> {
                return Rx.Observable.interval(10);
            },
            $(selector: string): JQuery {
                return $(selector);
            },
            observeInitializedAndChanges(): Observable<any> {
                return Rx.Observable.interval(10);
            }
        };

        const quizlet = {learnGameData:
            {
                allTerms: [{
                    id: 1,
                    photo: '',
                    word: '',
                    definition: '',
                    quiz_id: 1,
                    term_lang: '',
                    def_lang: ''
                }],
                audioOn: true,
                autofocus: true,
                canEditSome: true,
                correctCount: 1,
                definitionAccents: ['',''],
                definitionSideLabel: '',
                hardLangs: ['',''],
                incorrect: [{
                    id: 1,
                    photo: '',
                    word: '',
                    definition: '',
                    quiz_id: 1,
                    term_lang: '',
                    def_lang: ''
                }],
                isEmbedding: true,
                promptOrder: '',
                remaining: [{
                    id: 1,
                    photo: '',
                    word: '',
                    definition: '',
                    quiz_id: 1,
                    term_lang: '',
                    def_lang: ''
                }],
                round: 1,
                roundTotal: 1,
                selectedOnly: true,
                selectedOnlyStartOverPath: '',
                session: 1,
                sessionTermIds: ['',''],
                sessionTypeConstant: 1,
                startOverPath: '',
                studyableId: 1,
                studyableType: 1,
                thirdSideAccents: ['',''],
                thirdSideLabel: '',
                wordAccents: ['',''],
                wordSideLabel: ''
            }};

        window.Quizlet = quizlet;

        const writePageQuizletDataProvider: WritePageQuizletDataProvider = new WritePageQuizletDataProvider(messageBus, window, dom);

        const results = messageBus
            .observe(TypeIdentity.createIdentity(WritePageQuizletDataMessage.prototype))
            .first()
            .do(actual => assert.deepEqual(actual, new WritePageQuizletDataMessage(quizlet)))
            .toPromise();

        writePageQuizletDataProvider.attach();

        await results;
    }
}