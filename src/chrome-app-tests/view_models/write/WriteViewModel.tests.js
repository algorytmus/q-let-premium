"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var rx_all_1 = require("./../../../chrome-app/scripts/rxjs/rx.all");
var ko = require("./../../../node_modules/knockout/build/output/knockout-latest.debug.js");
global.ko = ko;
if (ko != undefined && ko.subscribable != undefined && ko.subscribable.fn != undefined && ko.subscribable.fn.toRxObservable == undefined) {
    // Knockout uses `fn` instead of `prototype`
    ko.subscribable.fn.toRxObservable = function (startWithCurrentValue) {
        var _this = this;
        if (startWithCurrentValue === void 0) { startWithCurrentValue = false; }
        return rx_all_1.Observable.create(function (observer) {
            // create a subscription, calling onNext on change
            var koSubscription = _this.subscribe(observer.onNext, observer);
            // hack into the underlying ko.subscribable so that if it is an existing
            // ko.subscription, its disposal terminates the Rx.Observable
            if (_this.dispose) {
                var dispose_1 = _this.dispose;
                _this.dispose = function () {
                    var args = [];
                    for (var _i = 0; _i < arguments.length; _i++) {
                        args[_i] = arguments[_i];
                    }
                    // call the underlying knockout disposal function
                    dispose_1.apply(_this, args);
                    // call the observer's onCompleted
                    observer.onCompleted();
                };
            }
            // start with the current value if applicable 
            if (startWithCurrentValue && ko.isObservable(_this)) {
                var currentValue = _this();
                currentValue === undefined || observer.onNext(currentValue);
            }
            // dispose of the ko.subscription when the Rx.Observable is disposed
            return koSubscription.dispose.bind(koSubscription);
        });
    };
    // Static helper from Rx.Observable, mirrors `fromPromise`, `fromEvent`, etc.
    rx_all_1.Observable.fromKnockout = function (koSubscribable) {
        return koSubscribable.toRxObservable();
    };
    rx_all_1.Observable.prototype.toKnockoutComputed = function () {
        var _this = this;
        var koObservable = ko.observable();
        var rxDisposable = new rx_all_1.SingleAssignmentDisposable;
        var computed = ko.pureComputed(function () {
            if (!rxDisposable.getDisposable()) {
                // This is to prevent our computed from accidentally
                // subscribing to any ko observables that happen to 
                // get evaluated during our call to this.subscribe().
                ko.computed(function () {
                    var rxSubscription = _this.subscribe(koObservable);
                    rxDisposable.setDisposable(rxSubscription);
                }).dispose();
            }
            return koObservable();
        });
        var dispose = computed.dispose;
        computed.dispose = function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            rxDisposable.dispose();
            dispose.apply(this, args);
        };
        return computed;
    };
    rx_all_1.Observable.prototype.toKnockoutComputed = function () {
        var _this = this;
        var koObservable = ko.observable();
        var rxDisposable = new rx_all_1.SingleAssignmentDisposable;
        var computed = ko.computed(function () {
            if (!rxDisposable.getDisposable()) {
                // This is to prevent our computed from accidentally
                // subscribing to any ko observables that happen to 
                // get evaluated during our call to this.subscribe().
                ko.computed(function () {
                    var rxSubscription = _this.subscribe(koObservable);
                    rxDisposable.setDisposable(rxSubscription);
                }).dispose();
            }
            return koObservable();
        }, this, { write: function (value) {
                koObservable(value);
            } });
        var dispose = computed.dispose;
        computed.dispose = function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            rxDisposable.dispose();
            dispose.apply(this, args);
        };
        return computed;
    };
}
var WritePageQuizletDataMessage_1 = require("./../../../chrome-app/data/WritePageQuizletDataMessage");
var TypeIdentity_1 = require("./../../../chrome-app/messaging/TypeIdentity");
var MessageBus_1 = require("./../../../chrome-app/messaging/MessageBus");
var TestWindow_1 = require("./../../messaging/TestWindow");
/// <reference path="./../../node_modules/mocha-typescript/globals.d.ts" />
var mocha_typescript_1 = require("mocha-typescript");
/// <reference path="./../../../chrome-app/scripts/jquery/jquery.d.ts" />
/// <reference path="./../../../chrome-app/scripts/rxjs/ts/rx.all.d.ts" />
/// <reference path="./../../../chrome-app/scripts/rxjs-jquery/ts/rx.jquery.d.ts" />
/// <reference path="./../../../chrome-app/scripts/rx-dom/rx-dom.d.ts" />
/// <reference path="./../../../chrome-app/scripts/knockout/knockout.js" />
/// <reference path="./../../../chrome-app/scripts/reactive-knockout/1-reactive-knockout.js" />
/// <reference path="./../../node_modules/@types/chai/index.d.ts" />
var chai_1 = require("chai");
var MessageWindow_1 = require("../../../chrome-app/messaging/MessageWindow");
var RawMessageBus_1 = require("../../../chrome-app/messaging/RawMessageBus");
var WriteViewModel_1 = require("../../../chrome-app/view_models/write/WriteViewModel");
var QuizletDataProvider_1 = require("../../../chrome-app/data/QuizletDataProvider");
var Quizlet_1 = require("../../../chrome-app/data/Quizlet");
var WriteViewModelTestSuite = /** @class */ (function () {
    function WriteViewModelTestSuite() {
    }
    WriteViewModelTestSuite.prototype.defaultHint = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, viewModel()
                            .shoudHaveHint('')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WriteViewModelTestSuite.prototype.hintForOnlyUserAnswer = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, viewModel()
                            .withAnswer('any')
                            .shoudHaveHint('')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WriteViewModelTestSuite.prototype.hintForOnlyTerm = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, viewModel()
                            .withTerm('any')
                            .shoudHaveHint('')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WriteViewModelTestSuite.prototype.hintForOnlyExercise = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, viewModel()
                            .withExercise('someWord', 'someDefinition')
                            .shoudHaveHint('')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WriteViewModelTestSuite.prototype.hintForExerciseWithNoNext = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, viewModel()
                            .withExercise('someWord', 'someDefinition')
                            .withOrder(Quizlet_1.PromptOrder.WordThenDefinition)
                            .withTerm('someWord')
                            .withAnswer('')
                            .withHintMode(WriteViewModel_1.HintMode.FirstLettersOfWordsThenEveryOtherLetter)
                            .shoudHaveHint('')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WriteViewModelTestSuite.prototype.hintForExerciseWithTerm = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, viewModel()
                            .withExercise('someWord', 'someDefinition')
                            .withOrder(Quizlet_1.PromptOrder.WordThenDefinition)
                            .withTerm('someWord')
                            .withPressedNext()
                            .withAnswer('')
                            .withHintMode(WriteViewModel_1.HintMode.FirstLettersOfWordsThenEveryOtherLetter)
                            .shoudHaveHint('someDefinition++FirstLettersOfWordsThenEveryOtherLetter')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WriteViewModelTestSuite.prototype.hintForExerciseWithTermThenNoMatchedTerm = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, viewModel()
                            .withExercise('someWord', 'someDefinition')
                            .withOrder(Quizlet_1.PromptOrder.WordThenDefinition)
                            .withTerm('someWord')
                            .withPressedNext()
                            .withAnswer('')
                            .withHintMode(WriteViewModel_1.HintMode.FirstLettersOfWordsThenEveryOtherLetter)
                            .withTerm('')
                            .shoudHaveHint('')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WriteViewModelTestSuite.prototype.hintForExerciseWithTermThenManyMatchingTerms = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, viewModel()
                            .withExercise('someWord', 'someDefinition')
                            .withExercise('someWord1', 'someDefinition2')
                            .withExercise('someWord1', 'someDefinition3')
                            .withOrder(Quizlet_1.PromptOrder.WordThenDefinition)
                            .withTerm('someWord')
                            .withPressedNext()
                            .withAnswer('')
                            .withHintMode(WriteViewModel_1.HintMode.FirstLettersOfWordsThenEveryOtherLetter)
                            .withTerm('someWord1')
                            .shoudHaveHint('')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WriteViewModelTestSuite.prototype.hintForExerciseWithAnswer = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, viewModel()
                            .withExercise('someWord', 'someDefinition')
                            .withOrder(Quizlet_1.PromptOrder.WordThenDefinition)
                            .withTerm('someWord')
                            .withPressedNext()
                            .withAnswer('withAnswer')
                            .withHintMode(WriteViewModel_1.HintMode.FirstLettersOfWordsThenEveryOtherLetter)
                            .shoudHaveHint('someDefinition+withAnswer+FirstLettersOfWordsThenEveryOtherLetter')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WriteViewModelTestSuite.prototype.hintForExerciseWithAnswerChanged = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, viewModel()
                            .withExercise('someWord', 'someDefinition')
                            .withOrder(Quizlet_1.PromptOrder.WordThenDefinition)
                            .withTerm('someWord')
                            .withPressedNext()
                            .withAnswer('withAnswer')
                            .withAnswer('withAnswer2')
                            .withHintMode(WriteViewModel_1.HintMode.FirstLettersOfWordsThenEveryOtherLetter)
                            .shoudHaveHint('someDefinition+withAnswer2+FirstLettersOfWordsThenEveryOtherLetter')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WriteViewModelTestSuite.prototype.hintForExerciseWithHintModeChanged = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, viewModel()
                            .withExercise('someWord', 'someDefinition')
                            .withOrder(Quizlet_1.PromptOrder.WordThenDefinition)
                            .withTerm('someWord')
                            .withPressedNext()
                            .withAnswer('withAnswer')
                            .withHintMode(WriteViewModel_1.HintMode.FirstLettersOfWordsThenEveryOtherLetter)
                            .withHintMode(WriteViewModel_1.HintMode.FirstLetterThenStarsInsteadOfLetters)
                            .shoudHaveHint('someDefinition+withAnswer+FirstLetterThenStarsInsteadOfLetters')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WriteViewModelTestSuite.prototype.hintForExerciseWithTermChanged = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, viewModel()
                            .withExercise('someWord', 'someDefinition')
                            .withExercise('someWord2', 'someDefinition2')
                            .withOrder(Quizlet_1.PromptOrder.WordThenDefinition)
                            .withTerm('someWord')
                            .withTerm('someWord2')
                            .withPressedNext()
                            .withAnswer('withAnswer')
                            .withHintMode(WriteViewModel_1.HintMode.FirstLetterThenStarsInsteadOfLetters)
                            .shoudHaveHint('someDefinition2+withAnswer+FirstLetterThenStarsInsteadOfLetters')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WriteViewModelTestSuite.prototype.hintForExerciseWithTermChangedForEmpty = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, viewModel()
                            .withExercise('someWord', 'someDefinition')
                            .withExercise('someWord2', 'someDefinition2')
                            .withExercise('someWord2', 'someDefinition3')
                            .withOrder(Quizlet_1.PromptOrder.WordThenDefinition)
                            .withTerm('someWord')
                            .withTerm('someWord2')
                            .withPressedNext()
                            .withAnswer('withAnswer')
                            .withHintMode(WriteViewModel_1.HintMode.FirstLetterThenStarsInsteadOfLetters)
                            .shoudHaveHint('')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WriteViewModelTestSuite.prototype.hintForExerciseWithNoHintMode = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, viewModel()
                            .withExercise('someWord', 'someDefinition')
                            .withOrder(Quizlet_1.PromptOrder.WordThenDefinition)
                            .withTerm('someWord')
                            .withPressedNext()
                            .withAnswer('withAnswer')
                            .shoudHaveHint('someDefinition+withAnswer+StarsInsteadOfLetters')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WriteViewModelTestSuite.prototype.hintForExerciseWithAnswerWithSameTerms = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, viewModel()
                            .withExercise('someWord', 'someDefinition')
                            .withExercise('someWord', 'someDefinition2')
                            .withOrder(Quizlet_1.PromptOrder.WordThenDefinition)
                            .withTerm('someWord')
                            .withPressedNext()
                            .withAnswer('withAnswer')
                            .withHintMode(WriteViewModel_1.HintMode.FirstLettersOfWordsThenEveryOtherLetter)
                            .shoudHaveHint('')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WriteViewModelTestSuite.prototype.hintForExerciseWithAnswerWithSameTermsAndDefinitions = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, viewModel()
                            .withExercise('someWord', 'someDefinition')
                            .withExercise('someWord', 'someDefinition')
                            .withOrder(Quizlet_1.PromptOrder.WordThenDefinition)
                            .withTerm('someWord')
                            .withPressedNext()
                            .withAnswer('withAnswer')
                            .withHintMode(WriteViewModel_1.HintMode.StarsInsteadOfLetters)
                            .shoudHaveHint('someDefinition+withAnswer+StarsInsteadOfLetters')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WriteViewModelTestSuite.prototype.hintForExerciseWithAnswerWithEmptyTerm = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, viewModel()
                            .withExercise('someWord', 'someDefinition')
                            .withOrder(Quizlet_1.PromptOrder.WordThenDefinition)
                            .withTerm('')
                            .withPressedNext()
                            .withAnswer('withAnswer')
                            .withHintMode(WriteViewModel_1.HintMode.FirstLettersOfWordsThenEveryOtherLetter)
                            .shoudHaveHint('')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WriteViewModelTestSuite.prototype.hintForExerciseWithAnswerWithEmptyTermAndWord = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, viewModel()
                            .withExercise('', 'someDefinition')
                            .withOrder(Quizlet_1.PromptOrder.WordThenDefinition)
                            .withTerm('')
                            .withPressedNext()
                            .withAnswer('withAnswer')
                            .withHintMode(WriteViewModel_1.HintMode.FirstLettersOfWordsThenEveryOtherLetter)
                            .shoudHaveHint('')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WriteViewModelTestSuite.prototype.hintForExerciseWithAnswerWithDottedTerm = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, viewModel()
                            .withExercise('someWord', 'someDefinition')
                            .withOrder(Quizlet_1.PromptOrder.WordThenDefinition)
                            .withTerm('...')
                            .withPressedNext()
                            .withAnswer('withAnswer')
                            .withHintMode(WriteViewModel_1.HintMode.FirstLettersOfWordsThenEveryOtherLetter)
                            .shoudHaveHint('')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WriteViewModelTestSuite.prototype.hintForExerciseWithAnswerWithDottedTermAndEmptyWord = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, viewModel()
                            .withExercise('', 'someDefinition')
                            .withOrder(Quizlet_1.PromptOrder.WordThenDefinition)
                            .withTerm('...')
                            .withPressedNext()
                            .withAnswer('withAnswer')
                            .withHintMode(WriteViewModel_1.HintMode.FirstLettersOfWordsThenEveryOtherLetter)
                            .shoudHaveHint('')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WriteViewModelTestSuite.prototype.hintForExerciseWithAnswerWithDottedTermAndDottedWord = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, viewModel()
                            .withExercise('...', 'someDefinition')
                            .withOrder(Quizlet_1.PromptOrder.WordThenDefinition)
                            .withTerm('...')
                            .withPressedNext()
                            .withAnswer('withAnswer')
                            .withHintMode(WriteViewModel_1.HintMode.FirstLettersOfWordsThenEveryOtherLetter)
                            .shoudHaveHint('')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WriteViewModelTestSuite.prototype.hintForExerciseWithAnswerWithDefinitionThenWord = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, viewModel()
                            .withExercise('someWord', 'someDefinition')
                            .withOrder(Quizlet_1.PromptOrder.DefinitionThenWord)
                            .withTerm('someDefinition')
                            .withPressedNext()
                            .withAnswer('withAnswer')
                            .withHintMode(WriteViewModel_1.HintMode.FirstLettersOfWordsThenEveryOtherLetter)
                            .shoudHaveHint('someWord+withAnswer+FirstLettersOfWordsThenEveryOtherLetter')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WriteViewModelTestSuite.prototype.hintForExerciseWithAnswerWithDefinitionThenWordWithTermChanged = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, viewModel()
                            .withExercise('someWord', 'someDefinition')
                            .withExercise('someWord2', 'someDefinition2')
                            .withOrder(Quizlet_1.PromptOrder.DefinitionThenWord)
                            .withTerm('someDefinition')
                            .withTerm('someDefinition2')
                            .withPressedNext()
                            .withAnswer('withAnswer')
                            .withHintMode(WriteViewModel_1.HintMode.FirstLettersOfWordsThenEveryOtherLetter)
                            .shoudHaveHint('someWord2+withAnswer+FirstLettersOfWordsThenEveryOtherLetter')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WriteViewModelTestSuite.prototype.hintForExerciseWithAnswerWithDefinitionThenWordWithTermChangedTooManyMatching = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, viewModel()
                            .withExercise('someWord', 'someDefinition')
                            .withExercise('someWord2', 'someDefinition2')
                            .withExercise('someWord3', 'someDefinition2')
                            .withOrder(Quizlet_1.PromptOrder.DefinitionThenWord)
                            .withTerm('someDefinition')
                            .withTerm('someDefinition2')
                            .withPressedNext()
                            .withAnswer('withAnswer')
                            .withHintMode(WriteViewModel_1.HintMode.FirstLettersOfWordsThenEveryOtherLetter)
                            .shoudHaveHint('')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WriteViewModelTestSuite.prototype.hintForExerciseWithAnswerChangedWithDefinitionThenWord = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, viewModel()
                            .withExercise('someWord', 'someDefinition')
                            .withOrder(Quizlet_1.PromptOrder.DefinitionThenWord)
                            .withTerm('someDefinition')
                            .withPressedNext()
                            .withAnswer('withAnswer')
                            .withAnswer('withAnswer2')
                            .withHintMode(WriteViewModel_1.HintMode.FirstLettersOfWordsThenEveryOtherLetter)
                            .shoudHaveHint('someWord+withAnswer2+FirstLettersOfWordsThenEveryOtherLetter')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WriteViewModelTestSuite.prototype.hintForExerciseWithAnswerWithDefinitionThenWordWithSameTerms = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, viewModel()
                            .withExercise('someWord', 'someDefinition')
                            .withExercise('someWord2', 'someDefinition')
                            .withOrder(Quizlet_1.PromptOrder.DefinitionThenWord)
                            .withTerm('someDefinition')
                            .withPressedNext()
                            .withAnswer('withAnswer')
                            .withHintMode(WriteViewModel_1.HintMode.FirstLettersOfWordsThenEveryOtherLetter)
                            .shoudHaveHint('')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WriteViewModelTestSuite.prototype.hintForExerciseWithAnswerWithDefinitionThenWordWithEmptyTerm = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, viewModel()
                            .withExercise('someWord', 'someDefinition')
                            .withOrder(Quizlet_1.PromptOrder.DefinitionThenWord)
                            .withTerm('')
                            .withPressedNext()
                            .withAnswer('withAnswer')
                            .withHintMode(WriteViewModel_1.HintMode.FirstLettersOfWordsThenEveryOtherLetter)
                            .shoudHaveHint('')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WriteViewModelTestSuite.prototype.hintForExerciseWithAnswerWithDefinitionThenWordWithEmptyTermAndWord = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, viewModel()
                            .withExercise('someWord', '')
                            .withOrder(Quizlet_1.PromptOrder.DefinitionThenWord)
                            .withTerm('')
                            .withPressedNext()
                            .withAnswer('withAnswer')
                            .withHintMode(WriteViewModel_1.HintMode.FirstLettersOfWordsThenEveryOtherLetter)
                            .shoudHaveHint('')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WriteViewModelTestSuite.prototype.hintForExerciseWithAnswerWithDefinitionThenWordWithDottedTerm = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, viewModel()
                            .withExercise('someWord', 'someDefinition')
                            .withOrder(Quizlet_1.PromptOrder.DefinitionThenWord)
                            .withTerm('...')
                            .withPressedNext()
                            .withAnswer('withAnswer')
                            .withHintMode(WriteViewModel_1.HintMode.FirstLettersOfWordsThenEveryOtherLetter)
                            .shoudHaveHint('')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WriteViewModelTestSuite.prototype.hintForExerciseWithAnswerWithDefinitionThenWordWithDottedTermAndEmptyDefinition = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, viewModel()
                            .withExercise('someWord', '')
                            .withOrder(Quizlet_1.PromptOrder.DefinitionThenWord)
                            .withTerm('...')
                            .withPressedNext()
                            .withAnswer('withAnswer')
                            .withHintMode(WriteViewModel_1.HintMode.FirstLettersOfWordsThenEveryOtherLetter)
                            .shoudHaveHint('')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WriteViewModelTestSuite.prototype.hintForExerciseWithAnswerWithDefinitionThenWordWithDottedTermAndDottedWord = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, viewModel()
                            .withExercise('...', 'someDefinition')
                            .withOrder(Quizlet_1.PromptOrder.WordThenDefinition)
                            .withTerm('...')
                            .withPressedNext()
                            .withAnswer('withAnswer')
                            .withHintMode(WriteViewModel_1.HintMode.FirstLettersOfWordsThenEveryOtherLetter)
                            .shoudHaveHint('')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], WriteViewModelTestSuite.prototype, "defaultHint", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], WriteViewModelTestSuite.prototype, "hintForOnlyUserAnswer", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], WriteViewModelTestSuite.prototype, "hintForOnlyTerm", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], WriteViewModelTestSuite.prototype, "hintForOnlyExercise", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], WriteViewModelTestSuite.prototype, "hintForExerciseWithNoNext", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], WriteViewModelTestSuite.prototype, "hintForExerciseWithTerm", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], WriteViewModelTestSuite.prototype, "hintForExerciseWithTermThenNoMatchedTerm", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], WriteViewModelTestSuite.prototype, "hintForExerciseWithTermThenManyMatchingTerms", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], WriteViewModelTestSuite.prototype, "hintForExerciseWithAnswer", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], WriteViewModelTestSuite.prototype, "hintForExerciseWithAnswerChanged", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], WriteViewModelTestSuite.prototype, "hintForExerciseWithHintModeChanged", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], WriteViewModelTestSuite.prototype, "hintForExerciseWithTermChanged", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], WriteViewModelTestSuite.prototype, "hintForExerciseWithTermChangedForEmpty", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], WriteViewModelTestSuite.prototype, "hintForExerciseWithNoHintMode", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], WriteViewModelTestSuite.prototype, "hintForExerciseWithAnswerWithSameTerms", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], WriteViewModelTestSuite.prototype, "hintForExerciseWithAnswerWithSameTermsAndDefinitions", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], WriteViewModelTestSuite.prototype, "hintForExerciseWithAnswerWithEmptyTerm", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], WriteViewModelTestSuite.prototype, "hintForExerciseWithAnswerWithEmptyTermAndWord", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], WriteViewModelTestSuite.prototype, "hintForExerciseWithAnswerWithDottedTerm", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], WriteViewModelTestSuite.prototype, "hintForExerciseWithAnswerWithDottedTermAndEmptyWord", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], WriteViewModelTestSuite.prototype, "hintForExerciseWithAnswerWithDottedTermAndDottedWord", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], WriteViewModelTestSuite.prototype, "hintForExerciseWithAnswerWithDefinitionThenWord", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], WriteViewModelTestSuite.prototype, "hintForExerciseWithAnswerWithDefinitionThenWordWithTermChanged", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], WriteViewModelTestSuite.prototype, "hintForExerciseWithAnswerWithDefinitionThenWordWithTermChangedTooManyMatching", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], WriteViewModelTestSuite.prototype, "hintForExerciseWithAnswerChangedWithDefinitionThenWord", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], WriteViewModelTestSuite.prototype, "hintForExerciseWithAnswerWithDefinitionThenWordWithSameTerms", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], WriteViewModelTestSuite.prototype, "hintForExerciseWithAnswerWithDefinitionThenWordWithEmptyTerm", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], WriteViewModelTestSuite.prototype, "hintForExerciseWithAnswerWithDefinitionThenWordWithEmptyTermAndWord", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], WriteViewModelTestSuite.prototype, "hintForExerciseWithAnswerWithDefinitionThenWordWithDottedTerm", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], WriteViewModelTestSuite.prototype, "hintForExerciseWithAnswerWithDefinitionThenWordWithDottedTermAndEmptyDefinition", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], WriteViewModelTestSuite.prototype, "hintForExerciseWithAnswerWithDefinitionThenWordWithDottedTermAndDottedWord", null);
    WriteViewModelTestSuite = __decorate([
        mocha_typescript_1.suite
    ], WriteViewModelTestSuite);
    return WriteViewModelTestSuite;
}());
var HintProviderTestSuite = /** @class */ (function () {
    function HintProviderTestSuite() {
    }
    HintProviderTestSuite.prototype.defaultHint = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, viewModel()
                            .shoudHaveHint('')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], HintProviderTestSuite.prototype, "defaultHint", null);
    HintProviderTestSuite = __decorate([
        mocha_typescript_1.suite
    ], HintProviderTestSuite);
    return HintProviderTestSuite;
}());
/*
describe.only('HintProvider', () => {
    let hintProvider: HintProvider = new HintProvider(new SpecificHintProviderCollection(
        {
            StarsInsteadOfLetters: new StarsInsteadOfLettersHintProvider(),
            FirstLetterThenStarsInsteadOfLetters: new FirstLetterThenStarsInsteadOfLettersHintProvider(),
            FirstLettersOfWordsThenEveryOtherLetter: new FirstLettersOfWordsThenEveryOtherLetterHintProvider(),
            FirstLettersOfWordsThenStarsInsteadOfLetters: new FirstLettersOfWordsThenStarsInsteadOfLettersHintProvider()
        }
    ));

    function StarsInsteadOfLetters(w: string, uM: string, h: string) : {hintMode: HintMode, word: string, userMask: string, hint: string} {
        return {
            hintMode: HintMode.StarsInsteadOfLetters,
            word: w,
            userMask: uM,
            hint: h
        };
    }

    // These are you test cases for iterating through
    const testCases: {hintMode: HintMode, word: string, userMask: string, hint: string}[] =
    [
        StarsInsteadOfLetters('', '', ''),
        StarsInsteadOfLetters('', ' ', ' '),
        StarsInsteadOfLetters('', ' a', '  '),
        StarsInsteadOfLetters('a', '', '*'),
        StarsInsteadOfLetters('a', 'a', ' '),
        StarsInsteadOfLetters('a', ' a', '  '),
        StarsInsteadOfLetters('a', ' ', ' *'),
        StarsInsteadOfLetters('a', ',', ' *'),
        StarsInsteadOfLetters('a', '!', ' *'),
        StarsInsteadOfLetters('a', '@', ' *'),
        StarsInsteadOfLetters('a', '$', ' *'),
        StarsInsteadOfLetters('a', '.', ' *'),
        StarsInsteadOfLetters('a', '?', ' *'),
        StarsInsteadOfLetters('a', '/', ' *'),
        StarsInsteadOfLetters('a', ':', ' *'),
        StarsInsteadOfLetters('a', '"', ' *'),
        StarsInsteadOfLetters('a', '+', ' *'),
        StarsInsteadOfLetters('a', '=', ' *'),
        StarsInsteadOfLetters('a', '*', ' *'),
        StarsInsteadOfLetters('a', '_', ' *'),
        StarsInsteadOfLetters('a', 'ö', ' '),
        StarsInsteadOfLetters('a', 'ü', ' '),
        StarsInsteadOfLetters('a', 'ä', ' '),
        StarsInsteadOfLetters('ß', 'ß', ' '),
        StarsInsteadOfLetters('ö', 'ö', ' '),
        StarsInsteadOfLetters('ü', 'ü', ' '),
        StarsInsteadOfLetters('ä', ' ', ' *'),
        StarsInsteadOfLetters('ß', ' ', ' *'),
        StarsInsteadOfLetters('ö', ' ', ' *'),
        StarsInsteadOfLetters('ü', ' ', ' *'),
        StarsInsteadOfLetters('ä', ' ', ' *'),
        StarsInsteadOfLetters('ß', ' ', ' *'),
        StarsInsteadOfLetters('aa', '', '**'),
        StarsInsteadOfLetters('aa', ' ', ' **'),
        StarsInsteadOfLetters('aa', 'a', ' *'),
        StarsInsteadOfLetters('aa', 'aa', '  '),
        StarsInsteadOfLetters('aaa', '', '***'),
        StarsInsteadOfLetters('aaa, aaa, aaa', '', '***, ***, ***'),
        StarsInsteadOfLetters('aaa, aaa, aaa', ' ', ' ***, ***, ***'),
        StarsInsteadOfLetters('aaa, aaa, aaa', '  ', '  ***, ***, ***'),
        StarsInsteadOfLetters('aaa, aaa, aaa', 'a', ' **, ***, ***'),
        StarsInsteadOfLetters('aaa, aaa, aaa', 'aaa', '   , ***, ***'),
        StarsInsteadOfLetters('aaa, aaa, aaa', 'aaa, ', '     ***, ***'),
        StarsInsteadOfLetters('aaa, aaa, aaa', 'aaa,  ', '      ***, ***'),
        StarsInsteadOfLetters('aaa, aaa, aaa', 'aaa, a', '      **, ***'),
        StarsInsteadOfLetters('aaa, aaa, aaa', ' aaa, a', '       **, ***'),
        StarsInsteadOfLetters('aaa, aaa, aaa', ' aaa, a ', '        **, ***'),
        StarsInsteadOfLetters('aaa, aaa, aaa', ' aaa, aaa ', '           ***'),
        StarsInsteadOfLetters('aaa, aaa, aaa', '  aaa  ,  aaa  ,  a', '                   **')
    ];

    // Iterate though each item in the array and run it through the test case.
    testCases.forEach((testCase): void => {
        it(`${testCase.hintMode} / word: "${testCase.word}" - mask: "${testCase.userMask}" => hint: "${testCase.hint}"`, () => {
            assert.deepEqual(hintProvider.provide(testCase.word, testCase.userMask, testCase.hintMode), testCase.hint);
        });
    });

});
*/
var TestHintProvider = /** @class */ (function () {
    function TestHintProvider() {
    }
    TestHintProvider.prototype.getDisplay = function (mode) {
        return '';
    };
    TestHintProvider.prototype.provide = function (term, userAnswer, mode) {
        return term + '+' + userAnswer + '+' + mode;
    };
    return TestHintProvider;
}());
var WriteViewModelBuilder = /** @class */ (function () {
    function WriteViewModelBuilder() {
        var _this = this;
        this.promptOrder = Quizlet_1.PromptOrder.WordThenDefinition;
        this.actionsRecorded = [];
        this.exercises = [];
        this.actionsRecorded.push(function (vm, observer) { return observer.onNext(new WritePageQuizletDataMessage_1.WritePageQuizletDataMessage(WriteViewModelBuilder.CreateQuizlet(_this.promptOrder, _this.exercises))); });
    }
    WriteViewModelBuilder.prototype.withOrder = function (promptOrder) {
        this.promptOrder = promptOrder;
        return this;
    };
    WriteViewModelBuilder.prototype.withExercise = function (word, definition) {
        var _this = this;
        this.exercises.push({ Word: word, Definition: definition });
        this.actionsRecorded.push(function (vm, observer) { return observer.onNext(new WritePageQuizletDataMessage_1.WritePageQuizletDataMessage(WriteViewModelBuilder.CreateQuizlet(_this.promptOrder, _this.exercises))); });
        return this;
    };
    WriteViewModelBuilder.prototype.withTerm = function (term) {
        this.actionsRecorded.push(function (vm, observer) { return vm.term(term); });
        return this;
    };
    WriteViewModelBuilder.prototype.withAnswer = function (answer) {
        this.actionsRecorded.push(function (vm, observer) { return vm.userAnswer(answer); });
        return this;
    };
    WriteViewModelBuilder.prototype.withHintMode = function (hint) {
        this.actionsRecorded.push(function (vm, observer) { return vm.hintMode(hint); });
        return this;
    };
    WriteViewModelBuilder.prototype.withPressedNext = function () {
        this.actionsRecorded.push(function (vm, observer) { return vm.nextExcercise({}); });
        return this;
    };
    WriteViewModelBuilder.prototype.shoudHaveHint = function (expectedHint) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var window, messageWindow, rawMessageBus, messageBus, quizletDataProvider, hintProvider, viewModel, results;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        window = new TestWindow_1.TestWindow();
                        messageWindow = new MessageWindow_1.MessageWindow(window);
                        rawMessageBus = new RawMessageBus_1.RawMessageBus(messageWindow);
                        messageBus = new MessageBus_1.MessageBus(rawMessageBus);
                        quizletDataProvider = new QuizletDataProvider_1.QuizletDataProvider(messageBus);
                        hintProvider = new TestHintProvider();
                        viewModel = new WriteViewModel_1.WriteViewModel(quizletDataProvider, hintProvider);
                        results = viewModel.hint
                            .toRxObservable()
                            .select(function (v) { return function () { return v; }; })
                            .startWith(viewModel.hint)
                            .takeUntilWithTime(200)
                            .last()
                            .do(function (actual) { return chai_1.assert.deepEqual(actual(), expectedHint); })
                            .toPromise();
                        messageBus
                            .observeRequests(TypeIdentity_1.TypeIdentity.createIdentity(WritePageQuizletDataMessage_1.WritePageQuizletDataMessage.prototype))
                            .delay(10)
                            .select(function (observer) {
                            return rx_all_1.Observable
                                .of(_this.actionsRecorded)
                                .selectMany(function (v) { return v; })
                                .do(function (action) { return action(viewModel, observer); });
                        })
                            .switch()
                            .subscribe();
                        return [4 /*yield*/, results];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WriteViewModelBuilder.CreateQuizlet = function (promptOrder, exercises) {
        return {
            learnGameData: {
                allTerms: exercises.map(function (e) {
                    return {
                        id: 1,
                        photo: '',
                        word: e.Word,
                        definition: e.Definition,
                        quiz_id: 1,
                        term_lang: '',
                        def_lang: ''
                    };
                }),
                audioOn: true,
                autofocus: true,
                canEditSome: true,
                correctCount: 1,
                definitionAccents: ['', ''],
                definitionSideLabel: '',
                hardLangs: ['', ''],
                incorrect: [],
                isEmbedding: true,
                promptOrder: promptOrder,
                remaining: [],
                round: 1,
                roundTotal: 1,
                selectedOnly: true,
                selectedOnlyStartOverPath: '',
                session: 1,
                sessionTermIds: ['', ''],
                sessionTypeConstant: 1,
                startOverPath: '',
                studyableId: 1,
                studyableType: 1,
                thirdSideAccents: ['', ''],
                thirdSideLabel: '',
                wordAccents: ['', ''],
                wordSideLabel: ''
            }
        };
    };
    return WriteViewModelBuilder;
}());
function viewModel() {
    return new WriteViewModelBuilder();
}
//# sourceMappingURL=WriteViewModel.tests.js.map