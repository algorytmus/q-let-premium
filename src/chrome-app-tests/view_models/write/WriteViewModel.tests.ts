import { Observer, Subject, Observable, AnonymousSubject, SingleAssignmentDisposable } from './../../../chrome-app/scripts/rxjs/rx.all';
import * as Rx from './../../../chrome-app/scripts/rxjs/rx.all';

import * as ko from './../../../node_modules/knockout/build/output/knockout-latest.debug.js';

global.ko = ko;

if (ko != undefined && ko.subscribable != undefined && ko.subscribable.fn != undefined && ko.subscribable.fn.toRxObservable == undefined)
{
    // Knockout uses `fn` instead of `prototype`
    ko.subscribable.fn.toRxObservable = function (startWithCurrentValue = false) {
        return Observable.create(observer => {
            // create a subscription, calling onNext on change
            const koSubscription = this.subscribe(observer.onNext, observer);
            
            // hack into the underlying ko.subscribable so that if it is an existing
            // ko.subscription, its disposal terminates the Rx.Observable
            if (this.dispose) {
            const { dispose } = this;
            this.dispose = (...args) => {
                // call the underlying knockout disposal function
                dispose.apply(this, args);
                // call the observer's onCompleted
                observer.onCompleted();
            }
            }

            // start with the current value if applicable 
            if (startWithCurrentValue && ko.isObservable(this)) {
                const currentValue = this();
                currentValue === undefined || observer.onNext(currentValue);
            }
            
            // dispose of the ko.subscription when the Rx.Observable is disposed
            return koSubscription.dispose.bind(koSubscription);
        });
    }

    // Static helper from Rx.Observable, mirrors `fromPromise`, `fromEvent`, etc.
    Observable.fromKnockout = (koSubscribable) => {
        return koSubscribable.toRxObservable();
    }

    
    Observable.prototype.toKnockoutComputed = function () {
        const koObservable = ko.observable();
        const rxDisposable = new SingleAssignmentDisposable;
        const computed = ko.pureComputed(() => {
            if (!rxDisposable.getDisposable()) {
                // This is to prevent our computed from accidentally
                // subscribing to any ko observables that happen to 
                // get evaluated during our call to this.subscribe().
                ko.computed(() => {
                    const rxSubscription = this.subscribe(koObservable);
                    rxDisposable.setDisposable(rxSubscription);
                }).dispose();
            }
            return koObservable();            
        });
        
        const { dispose } = computed;
        computed.dispose = function (...args) {
            rxDisposable.dispose();        
            dispose.apply(this, args);
        };
    
        return computed;
    };

    Observable.prototype.toKnockoutComputed = function () {
        const koObservable = ko.observable();
        const rxDisposable = new SingleAssignmentDisposable;
        const computed = ko.computed(() => {
            if (!rxDisposable.getDisposable()) {
                // This is to prevent our computed from accidentally
                // subscribing to any ko observables that happen to 
                // get evaluated during our call to this.subscribe().
                ko.computed(() => {
                    const rxSubscription = this.subscribe(koObservable);
                    rxDisposable.setDisposable(rxSubscription);
                }).dispose();
            }
            return koObservable();            
        }, this, {write : (value) => {
            koObservable(value);
        } });
        
        const { dispose } = computed;
        computed.dispose = function (...args) {
            rxDisposable.dispose();        
            dispose.apply(this, args);
        };
    
        return computed;
    };

}

import { WritePageQuizletDataMessage } from './../../../chrome-app/data/WritePageQuizletDataMessage';
import { TypeIdentity } from './../../../chrome-app/messaging/TypeIdentity';
import { MessageBus } from './../../../chrome-app/messaging/MessageBus';
import { TestWindow } from './../../messaging/TestWindow';
/// <reference path="./../../node_modules/mocha-typescript/globals.d.ts" />
import { suite, test, slow, timeout } from 'mocha-typescript';

/// <reference path="./../../../chrome-app/scripts/jquery/jquery.d.ts" />
/// <reference path="./../../../chrome-app/scripts/rxjs/ts/rx.all.d.ts" />
/// <reference path="./../../../chrome-app/scripts/rxjs-jquery/ts/rx.jquery.d.ts" />
/// <reference path="./../../../chrome-app/scripts/rx-dom/rx-dom.d.ts" />
/// <reference path="./../../../chrome-app/scripts/knockout/knockout.js" />
/// <reference path="./../../../chrome-app/scripts/reactive-knockout/1-reactive-knockout.js" />

/// <reference path="./../../node_modules/@types/chai/index.d.ts" />
import { assert } from 'chai';

/// <reference path="./../../chrome-app/views/write/WritePageQuizletDataProvider.ts" />
import { WritePageQuizletDataProvider } from './../../../chrome-app/views/write/WritePageQuizletDataProvider';
import { MessageWindow } from '../../../chrome-app/messaging/MessageWindow';
import { RawMessageBus } from '../../../chrome-app/messaging/RawMessageBus';
import { WriteViewModel, IHintProvider, HintMode, HintProvider, SpecificHintProviderCollection, StarsInsteadOfLettersHintProvider, FirstLetterThenStarsInsteadOfLettersHintProvider, FirstLettersOfWordsThenEveryOtherLetterHintProvider, FirstLettersOfWordsThenStarsInsteadOfLettersHintProvider } from '../../../chrome-app/view_models/write/WriteViewModel';
import { QuizletDataProvider } from '../../../chrome-app/data/QuizletDataProvider';
import { Quizlet, PromptOrder } from '../../../chrome-app/data/Quizlet';

@suite
class WriteViewModelTestSuite {
    
    @test async defaultHint() {
        await viewModel()
            .shoudHaveHint('');
    }

    @test async hintForOnlyUserAnswer() {
        await viewModel()
            .withAnswer('any')
            .shoudHaveHint('');
    }

    @test async hintForOnlyTerm() {
        await viewModel()
            .withTerm('any')
            .shoudHaveHint('');
    }
    
    @test async hintForOnlyExercise() {
        await viewModel()
            .withExercise('someWord', 'someDefinition')
            .shoudHaveHint('');
    }
    
    @test async hintForExerciseWithNoNext() {
        await viewModel()
            .withExercise('someWord', 'someDefinition')
            .withOrder(PromptOrder.WordThenDefinition)
            .withTerm('someWord')
            .withAnswer('')
            .withHintMode(HintMode.FirstLettersOfWordsThenEveryOtherLetter)
            .shoudHaveHint('');
    }
    
    @test async hintForExerciseWithTerm() {
        await viewModel()
            .withExercise('someWord', 'someDefinition')
            .withOrder(PromptOrder.WordThenDefinition)
            .withTerm('someWord')
            .withPressedNext()
            .withAnswer('')
            .withHintMode(HintMode.FirstLettersOfWordsThenEveryOtherLetter)
            .shoudHaveHint('someDefinition++FirstLettersOfWordsThenEveryOtherLetter');
    }
    
    @test async hintForExerciseWithTermThenNoMatchedTerm() {
        await viewModel()
            .withExercise('someWord', 'someDefinition')
            .withOrder(PromptOrder.WordThenDefinition)
            .withTerm('someWord')
            .withPressedNext()
            .withAnswer('')
            .withHintMode(HintMode.FirstLettersOfWordsThenEveryOtherLetter)
            .withTerm('')
            .shoudHaveHint('');
    }
    
    @test async hintForExerciseWithTermThenManyMatchingTerms() {
        await viewModel()
            .withExercise('someWord', 'someDefinition')
            .withExercise('someWord1', 'someDefinition2')
            .withExercise('someWord1', 'someDefinition3')
            .withOrder(PromptOrder.WordThenDefinition)
            .withTerm('someWord')
            .withPressedNext()
            .withAnswer('')
            .withHintMode(HintMode.FirstLettersOfWordsThenEveryOtherLetter)
            .withTerm('someWord1')
            .shoudHaveHint('');
    }
    
    @test async hintForExerciseWithAnswer() {
        await viewModel()
            .withExercise('someWord', 'someDefinition')
            .withOrder(PromptOrder.WordThenDefinition)
            .withTerm('someWord')
            .withPressedNext()
            .withAnswer('withAnswer')
            .withHintMode(HintMode.FirstLettersOfWordsThenEveryOtherLetter)
            .shoudHaveHint('someDefinition+withAnswer+FirstLettersOfWordsThenEveryOtherLetter');
    }
    
    @test async hintForExerciseWithAnswerChanged() {
        await viewModel()
            .withExercise('someWord', 'someDefinition')
            .withOrder(PromptOrder.WordThenDefinition)
            .withTerm('someWord')
            .withPressedNext()
            .withAnswer('withAnswer')
            .withAnswer('withAnswer2')
            .withHintMode(HintMode.FirstLettersOfWordsThenEveryOtherLetter)
            .shoudHaveHint('someDefinition+withAnswer2+FirstLettersOfWordsThenEveryOtherLetter');
    }
    
    @test async hintForExerciseWithHintModeChanged() {
        await viewModel()
            .withExercise('someWord', 'someDefinition')
            .withOrder(PromptOrder.WordThenDefinition)
            .withTerm('someWord')
            .withPressedNext()
            .withAnswer('withAnswer')
            .withHintMode(HintMode.FirstLettersOfWordsThenEveryOtherLetter)
            .withHintMode(HintMode.FirstLetterThenStarsInsteadOfLetters)
            .shoudHaveHint('someDefinition+withAnswer+FirstLetterThenStarsInsteadOfLetters');
    }
    
    @test async hintForExerciseWithTermChanged() {
        await viewModel()
            .withExercise('someWord', 'someDefinition')
            .withExercise('someWord2', 'someDefinition2')
            .withOrder(PromptOrder.WordThenDefinition)
            .withTerm('someWord')
            .withTerm('someWord2')
            .withPressedNext()
            .withAnswer('withAnswer')
            .withHintMode(HintMode.FirstLetterThenStarsInsteadOfLetters)
            .shoudHaveHint('someDefinition2+withAnswer+FirstLetterThenStarsInsteadOfLetters');
    }
    
    @test async hintForExerciseWithTermChangedForEmpty() {
        await viewModel()
            .withExercise('someWord', 'someDefinition')
            .withExercise('someWord2', 'someDefinition2')
            .withExercise('someWord2', 'someDefinition3')
            .withOrder(PromptOrder.WordThenDefinition)
            .withTerm('someWord')
            .withTerm('someWord2')
            .withPressedNext()
            .withAnswer('withAnswer')
            .withHintMode(HintMode.FirstLetterThenStarsInsteadOfLetters)
            .shoudHaveHint('');
    }
    
    @test async hintForExerciseWithNoHintMode() {
        await viewModel()
            .withExercise('someWord', 'someDefinition')
            .withOrder(PromptOrder.WordThenDefinition)
            .withTerm('someWord')
            .withPressedNext()
            .withAnswer('withAnswer')
            .shoudHaveHint('someDefinition+withAnswer+StarsInsteadOfLetters');
    }
    
    @test async hintForExerciseWithAnswerWithSameTerms() {
        await viewModel()
            .withExercise('someWord', 'someDefinition')
            .withExercise('someWord', 'someDefinition2')
            .withOrder(PromptOrder.WordThenDefinition)
            .withTerm('someWord')
            .withPressedNext()
            .withAnswer('withAnswer')
            .withHintMode(HintMode.FirstLettersOfWordsThenEveryOtherLetter)
            .shoudHaveHint('');
    }

    @test async hintForExerciseWithAnswerWithSameTermsAndDefinitions() {
        await viewModel()
            .withExercise('someWord', 'someDefinition')
            .withExercise('someWord', 'someDefinition')
            .withOrder(PromptOrder.WordThenDefinition)
            .withTerm('someWord')
            .withPressedNext()
            .withAnswer('withAnswer')
            .withHintMode(HintMode.StarsInsteadOfLetters)
            .shoudHaveHint('someDefinition+withAnswer+StarsInsteadOfLetters');
    }
    
    @test async hintForExerciseWithAnswerWithEmptyTerm() {
        await viewModel()
            .withExercise('someWord', 'someDefinition')
            .withOrder(PromptOrder.WordThenDefinition)
            .withTerm('')
            .withPressedNext()
            .withAnswer('withAnswer')
            .withHintMode(HintMode.FirstLettersOfWordsThenEveryOtherLetter)
            .shoudHaveHint('');
    }
    
    @test async hintForExerciseWithAnswerWithEmptyTermAndWord() {
        await viewModel()
            .withExercise('', 'someDefinition')
            .withOrder(PromptOrder.WordThenDefinition)
            .withTerm('')
            .withPressedNext()
            .withAnswer('withAnswer')
            .withHintMode(HintMode.FirstLettersOfWordsThenEveryOtherLetter)
            .shoudHaveHint('');
    }
    
    @test async hintForExerciseWithAnswerWithDottedTerm() {
        await viewModel()
            .withExercise('someWord', 'someDefinition')
            .withOrder(PromptOrder.WordThenDefinition)
            .withTerm('...')
            .withPressedNext()
            .withAnswer('withAnswer')
            .withHintMode(HintMode.FirstLettersOfWordsThenEveryOtherLetter)
            .shoudHaveHint('');
    }
    
    @test async hintForExerciseWithAnswerWithDottedTermAndEmptyWord() {
        await viewModel()
            .withExercise('', 'someDefinition')
            .withOrder(PromptOrder.WordThenDefinition)
            .withTerm('...')
            .withPressedNext()
            .withAnswer('withAnswer')
            .withHintMode(HintMode.FirstLettersOfWordsThenEveryOtherLetter)
            .shoudHaveHint('');
    }
    
    @test async hintForExerciseWithAnswerWithDottedTermAndDottedWord() {
        await viewModel()
            .withExercise('...', 'someDefinition')
            .withOrder(PromptOrder.WordThenDefinition)
            .withTerm('...')
            .withPressedNext()
            .withAnswer('withAnswer')
            .withHintMode(HintMode.FirstLettersOfWordsThenEveryOtherLetter)
            .shoudHaveHint('');
    }
    
    @test async hintForExerciseWithAnswerWithDefinitionThenWord() {
        await viewModel()
            .withExercise('someWord', 'someDefinition')
            .withOrder(PromptOrder.DefinitionThenWord)
            .withTerm('someDefinition')
            .withPressedNext()
            .withAnswer('withAnswer')
            .withHintMode(HintMode.FirstLettersOfWordsThenEveryOtherLetter)
            .shoudHaveHint('someWord+withAnswer+FirstLettersOfWordsThenEveryOtherLetter');
    }

    @test async hintForExerciseWithAnswerWithDefinitionThenWordWithTermChanged() {
        await viewModel()
            .withExercise('someWord', 'someDefinition')
            .withExercise('someWord2', 'someDefinition2')
            .withOrder(PromptOrder.DefinitionThenWord)
            .withTerm('someDefinition')
            .withTerm('someDefinition2')
            .withPressedNext()
            .withAnswer('withAnswer')
            .withHintMode(HintMode.FirstLettersOfWordsThenEveryOtherLetter)
            .shoudHaveHint('someWord2+withAnswer+FirstLettersOfWordsThenEveryOtherLetter');
    }
    
    @test async hintForExerciseWithAnswerWithDefinitionThenWordWithTermChangedTooManyMatching() {
        await viewModel()
            .withExercise('someWord', 'someDefinition')
            .withExercise('someWord2', 'someDefinition2')
            .withExercise('someWord3', 'someDefinition2')
            .withOrder(PromptOrder.DefinitionThenWord)
            .withTerm('someDefinition')
            .withTerm('someDefinition2')
            .withPressedNext()
            .withAnswer('withAnswer')
            .withHintMode(HintMode.FirstLettersOfWordsThenEveryOtherLetter)
            .shoudHaveHint('');
    }
    
    @test async hintForExerciseWithAnswerChangedWithDefinitionThenWord() {
        await viewModel()
            .withExercise('someWord', 'someDefinition')
            .withOrder(PromptOrder.DefinitionThenWord)
            .withTerm('someDefinition')
            .withPressedNext()
            .withAnswer('withAnswer')
            .withAnswer('withAnswer2')
            .withHintMode(HintMode.FirstLettersOfWordsThenEveryOtherLetter)
            .shoudHaveHint('someWord+withAnswer2+FirstLettersOfWordsThenEveryOtherLetter');
    }
    
    @test async hintForExerciseWithAnswerWithDefinitionThenWordWithSameTerms() {
        await viewModel()
            .withExercise('someWord', 'someDefinition')
            .withExercise('someWord2', 'someDefinition')
            .withOrder(PromptOrder.DefinitionThenWord)
            .withTerm('someDefinition')
            .withPressedNext()
            .withAnswer('withAnswer')
            .withHintMode(HintMode.FirstLettersOfWordsThenEveryOtherLetter)
            .shoudHaveHint('');
    }
    
    @test async hintForExerciseWithAnswerWithDefinitionThenWordWithEmptyTerm() {
        await viewModel()
            .withExercise('someWord', 'someDefinition')
            .withOrder(PromptOrder.DefinitionThenWord)
            .withTerm('')
            .withPressedNext()
            .withAnswer('withAnswer')
            .withHintMode(HintMode.FirstLettersOfWordsThenEveryOtherLetter)
            .shoudHaveHint('');
    }
    
    @test async hintForExerciseWithAnswerWithDefinitionThenWordWithEmptyTermAndWord() {
        await viewModel()
            .withExercise('someWord', '')
            .withOrder(PromptOrder.DefinitionThenWord)
            .withTerm('')
            .withPressedNext()
            .withAnswer('withAnswer')
            .withHintMode(HintMode.FirstLettersOfWordsThenEveryOtherLetter)
            .shoudHaveHint('');
    }
    
    @test async hintForExerciseWithAnswerWithDefinitionThenWordWithDottedTerm() {
        await viewModel()
            .withExercise('someWord', 'someDefinition')
            .withOrder(PromptOrder.DefinitionThenWord)
            .withTerm('...')
            .withPressedNext()
            .withAnswer('withAnswer')
            .withHintMode(HintMode.FirstLettersOfWordsThenEveryOtherLetter)
            .shoudHaveHint('');
    }
    
    @test async hintForExerciseWithAnswerWithDefinitionThenWordWithDottedTermAndEmptyDefinition() {
        await viewModel()
            .withExercise('someWord', '')
            .withOrder(PromptOrder.DefinitionThenWord)
            .withTerm('...')
            .withPressedNext()
            .withAnswer('withAnswer')
            .withHintMode(HintMode.FirstLettersOfWordsThenEveryOtherLetter)
            .shoudHaveHint('');
    }
    
    @test async hintForExerciseWithAnswerWithDefinitionThenWordWithDottedTermAndDottedWord() {
        await viewModel()
            .withExercise('...', 'someDefinition')
            .withOrder(PromptOrder.WordThenDefinition)
            .withTerm('...')
            .withPressedNext()
            .withAnswer('withAnswer')
            .withHintMode(HintMode.FirstLettersOfWordsThenEveryOtherLetter)
            .shoudHaveHint('');
    }
}

@suite
class HintProviderTestSuite {
    
    @test async defaultHint() {
        await viewModel()
            .shoudHaveHint('');
    }
}


/*
describe.only('HintProvider', () => {
	let hintProvider: HintProvider = new HintProvider(new SpecificHintProviderCollection(
        {
            StarsInsteadOfLetters: new StarsInsteadOfLettersHintProvider(),
            FirstLetterThenStarsInsteadOfLetters: new FirstLetterThenStarsInsteadOfLettersHintProvider(),
            FirstLettersOfWordsThenEveryOtherLetter: new FirstLettersOfWordsThenEveryOtherLetterHintProvider(),
            FirstLettersOfWordsThenStarsInsteadOfLetters: new FirstLettersOfWordsThenStarsInsteadOfLettersHintProvider()
        }
    ));

    function StarsInsteadOfLetters(w: string, uM: string, h: string) : {hintMode: HintMode, word: string, userMask: string, hint: string} {
        return {
            hintMode: HintMode.StarsInsteadOfLetters, 
            word: w, 
            userMask: uM, 
            hint: h
        };
    }

	// These are you test cases for iterating through
    const testCases: {hintMode: HintMode, word: string, userMask: string, hint: string}[] = 
    [
        StarsInsteadOfLetters('', '', ''),
        StarsInsteadOfLetters('', ' ', ' '),
        StarsInsteadOfLetters('', ' a', '  '),
        StarsInsteadOfLetters('a', '', '*'),
        StarsInsteadOfLetters('a', 'a', ' '),
        StarsInsteadOfLetters('a', ' a', '  '),
        StarsInsteadOfLetters('a', ' ', ' *'),
        StarsInsteadOfLetters('a', ',', ' *'),
        StarsInsteadOfLetters('a', '!', ' *'),
        StarsInsteadOfLetters('a', '@', ' *'),
        StarsInsteadOfLetters('a', '$', ' *'),
        StarsInsteadOfLetters('a', '.', ' *'),
        StarsInsteadOfLetters('a', '?', ' *'),
        StarsInsteadOfLetters('a', '/', ' *'),
        StarsInsteadOfLetters('a', ':', ' *'),
        StarsInsteadOfLetters('a', '"', ' *'),
        StarsInsteadOfLetters('a', '+', ' *'),
        StarsInsteadOfLetters('a', '=', ' *'),
        StarsInsteadOfLetters('a', '*', ' *'),
        StarsInsteadOfLetters('a', '_', ' *'),
        StarsInsteadOfLetters('a', 'ö', ' '),
        StarsInsteadOfLetters('a', 'ü', ' '),
        StarsInsteadOfLetters('a', 'ä', ' '),
        StarsInsteadOfLetters('ß', 'ß', ' '),
        StarsInsteadOfLetters('ö', 'ö', ' '),
        StarsInsteadOfLetters('ü', 'ü', ' '),
        StarsInsteadOfLetters('ä', ' ', ' *'),
        StarsInsteadOfLetters('ß', ' ', ' *'),
        StarsInsteadOfLetters('ö', ' ', ' *'),
        StarsInsteadOfLetters('ü', ' ', ' *'),
        StarsInsteadOfLetters('ä', ' ', ' *'),
        StarsInsteadOfLetters('ß', ' ', ' *'),
        StarsInsteadOfLetters('aa', '', '**'),
        StarsInsteadOfLetters('aa', ' ', ' **'),
        StarsInsteadOfLetters('aa', 'a', ' *'),
        StarsInsteadOfLetters('aa', 'aa', '  '),
        StarsInsteadOfLetters('aaa', '', '***'),
        StarsInsteadOfLetters('aaa, aaa, aaa', '', '***, ***, ***'),
        StarsInsteadOfLetters('aaa, aaa, aaa', ' ', ' ***, ***, ***'),
        StarsInsteadOfLetters('aaa, aaa, aaa', '  ', '  ***, ***, ***'),
        StarsInsteadOfLetters('aaa, aaa, aaa', 'a', ' **, ***, ***'),
        StarsInsteadOfLetters('aaa, aaa, aaa', 'aaa', '   , ***, ***'),
        StarsInsteadOfLetters('aaa, aaa, aaa', 'aaa, ', '     ***, ***'),
        StarsInsteadOfLetters('aaa, aaa, aaa', 'aaa,  ', '      ***, ***'),
        StarsInsteadOfLetters('aaa, aaa, aaa', 'aaa, a', '      **, ***'),
        StarsInsteadOfLetters('aaa, aaa, aaa', ' aaa, a', '       **, ***'),
        StarsInsteadOfLetters('aaa, aaa, aaa', ' aaa, a ', '        **, ***'),
        StarsInsteadOfLetters('aaa, aaa, aaa', ' aaa, aaa ', '           ***'),
        StarsInsteadOfLetters('aaa, aaa, aaa', '  aaa  ,  aaa  ,  a', '                   **')
    ];

	// Iterate though each item in the array and run it through the test case.
	testCases.forEach((testCase): void => {
		it(`${testCase.hintMode} / word: "${testCase.word}" - mask: "${testCase.userMask}" => hint: "${testCase.hint}"`, () => {
            assert.deepEqual(hintProvider.provide(testCase.word, testCase.userMask, testCase.hintMode), testCase.hint);
		});
	});

});
*/

class TestHintProvider implements IHintProvider
{
    getDisplay(mode: HintMode): string {
        return '';
    }
    provide(term: string, userAnswer: string, mode: HintMode): string {
        return term + '+' + userAnswer + '+' + mode;
    }
}

class WriteViewModelBuilder{
    private promptOrder: PromptOrder = PromptOrder.WordThenDefinition;
    private actionsRecorded : ((vm: WriteViewModel, observer: Observer<Quizlet>) => void) [] = [];
    private exercises: {Word: string, Definition: string}[] = [];

    constructor()
    {
        this.actionsRecorded.push((vm, observer) => observer.onNext(
            new WritePageQuizletDataMessage(WriteViewModelBuilder.CreateQuizlet(this.promptOrder, this.exercises) )));
    }

    withOrder(promptOrder: PromptOrder) : WriteViewModelBuilder {
        this.promptOrder = promptOrder;
        return this;
    }

    withExercise(word: string, definition: string) : WriteViewModelBuilder
    {
        this.exercises.push({Word: word, Definition: definition});
        this.actionsRecorded.push((vm, observer) => observer.onNext(
            new WritePageQuizletDataMessage(WriteViewModelBuilder.CreateQuizlet(this.promptOrder, this.exercises) )));
        return this;
    }

    withTerm(term: string) : WriteViewModelBuilder
    {
        this.actionsRecorded.push((vm, observer) => vm.term(term));
        return this;
    }

    withAnswer(answer: string) : WriteViewModelBuilder{
        this.actionsRecorded.push((vm, observer) => vm.userAnswer(answer));
        return this;
    }
    
    withHintMode(hint: HintMode) : WriteViewModelBuilder{
        this.actionsRecorded.push((vm, observer) => vm.hintMode(hint));
        return this;
    }

    withPressedNext() : WriteViewModelBuilder{
        this.actionsRecorded.push((vm, observer) => vm.nextExcercise({}));
        return this;
    }

    async shoudHaveHint(expectedHint: string) : Promise<void>{
        const window = new TestWindow();
        const messageWindow: MessageWindow = new MessageWindow(window);
        const rawMessageBus = new RawMessageBus(messageWindow);
        const messageBus = new MessageBus(rawMessageBus);
        const quizletDataProvider: IQuizletDataProvider = new QuizletDataProvider(messageBus);
        const hintProvider: TestHintProvider = new TestHintProvider();

        const viewModel = new WriteViewModel(quizletDataProvider, hintProvider);

        const results = viewModel.hint
            .toRxObservable()
            .select(v => () => v)
            .startWith(viewModel.hint)
            .takeUntilWithTime(200)
            .last()
            .do(actual => assert.deepEqual(actual(), expectedHint))
            .toPromise();

        messageBus
            .observeRequests(TypeIdentity.createIdentity(WritePageQuizletDataMessage.prototype))
            .delay(10)
            .select(observer => 
                Observable
                .of(this.actionsRecorded)
                .selectMany(v => v)
                .do(action => action(viewModel, observer) )
            )
            .switch()
            .subscribe();

        await results;
    }

    private static CreateQuizlet(promptOrder: PromptOrder, exercises: {Word: string, Definition: string}[]): Quizlet{
        return {
            learnGameData:
            {
                allTerms:
                    exercises.map(e => { 
                        return {
                            id: 1,
                            photo: '',
                            word: e.Word,
                            definition: e.Definition,
                            quiz_id: 1,
                            term_lang: '',
                            def_lang: ''
                        } 
                }),
                audioOn: true,
                autofocus: true,
                canEditSome: true,
                correctCount: 1,
                definitionAccents: ['',''],
                definitionSideLabel: '',
                hardLangs: ['',''],
                incorrect: [],
                isEmbedding: true,
                promptOrder: promptOrder,
                remaining: [],
                round: 1,
                roundTotal: 1,
                selectedOnly: true,
                selectedOnlyStartOverPath: '',
                session: 1,
                sessionTermIds: ['',''],
                sessionTypeConstant: 1,
                startOverPath: '',
                studyableId: 1,
                studyableType: 1,
                thirdSideAccents: ['',''],
                thirdSideLabel: '',
                wordAccents: ['',''],
                wordSideLabel: ''
            }};
    }
}


function viewModel()
{
    return new WriteViewModelBuilder();
}