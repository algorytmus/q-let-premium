import { Path } from './Path';
import { basename } from 'path';
import { BuilderPath } from './BuilderPath';
import { ChromePath } from './ChromePath';

module ChromeExtension {
    export class ProjectBuilderRootPath {
        private static COMPILED_MANIFEST_NAME: string = 'manifest.json';

        constructor(public root: BuilderPath) {
        }

        public get manifest(): BuilderPath {
            return this.root.subpath(ProjectBuilderRootPath.COMPILED_MANIFEST_NAME);
        }
    }
}

export = ChromeExtension;
exports.ProjectBuilderRootPath = ChromeExtension.ProjectBuilderRootPath;