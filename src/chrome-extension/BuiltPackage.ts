import { Project } from './Project';
import { ProjectBuilderSpace } from './ProjectBuilderSpace';

module ChromeExtension {
    export class BuiltPackage {
        constructor(private projectBuilderSpace: ProjectBuilderSpace, private project: Project) {}
    }
}

export = ChromeExtension;
exports.BuiltPackage = ChromeExtension.BuiltPackage;