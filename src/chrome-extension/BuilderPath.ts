import { Path } from './Path';
module ChromeExtension {
    export class BuilderPath extends Path {
        constructor(public path: string) {
            super(path);
        }

        public subpath(entry: string): BuilderPath {
            return new BuilderPath(this.resolveSubpath(entry));
        }

        public get parent(): BuilderPath {
            return new BuilderPath(super.resolveParent());
        }
    }
}

export = ChromeExtension;
exports.BuilderPath = ChromeExtension.BuilderPath;