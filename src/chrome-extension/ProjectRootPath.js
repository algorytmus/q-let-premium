"use strict";
var path_1 = require("path");
var ChromeExtension;
(function (ChromeExtension) {
    var ProjectRootPath = /** @class */ (function () {
        function ProjectRootPath(root) {
            this.root = root;
        }
        Object.defineProperty(ProjectRootPath.prototype, "name", {
            get: function () {
                return path_1.basename(this.root.path);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ProjectRootPath.prototype, "projectManifest", {
            get: function () {
                return this.root.subpath(ProjectRootPath.PROJECT_MANIFEST);
            },
            enumerable: true,
            configurable: true
        });
        ProjectRootPath.PROJECT_MANIFEST = 'project.manifest';
        return ProjectRootPath;
    }());
    ChromeExtension.ProjectRootPath = ProjectRootPath;
})(ChromeExtension || (ChromeExtension = {}));
exports.ProjectRootPath = ChromeExtension.ProjectRootPath;
module.exports = ChromeExtension;
//# sourceMappingURL=ProjectRootPath.js.map