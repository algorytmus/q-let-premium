"use strict";
var ChromeExtension;
(function (ChromeExtension) {
    var ContentfullManifest = /** @class */ (function () {
        function ContentfullManifest(projectManifest, content_scripts, web_accessible_resources, version) {
            this.projectManifest = projectManifest;
            this.content_scripts = content_scripts;
            this.web_accessible_resources = web_accessible_resources;
            this.version = version;
        }
        ContentfullManifest.prototype.stringifyManifest = function () {
            return JSON.stringify(this.createPackageManifest(), null, '\t');
        };
        Object.defineProperty(ContentfullManifest.prototype, "allFiles", {
            get: function () {
                return this.content_scripts
                    .map(function (contentScript) {
                    return contentScript.css
                        .concat(contentScript.js);
                })
                    .concat(this.web_accessible_resources)
                    .reduce(function (previousValue, currentValue, _) { return previousValue.concat(currentValue); });
            },
            enumerable: true,
            configurable: true
        });
        ContentfullManifest.prototype.createPackageManifest = function () {
            return {
                manifest_version: ContentfullManifest.manifest_version,
                version: this.version,
                name: this.projectManifest.properties.name,
                description: this.projectManifest.properties.description,
                permissions: this.projectManifest.properties.permissions,
                content_scripts: this.content_scripts.map(function (cs) {
                    return {
                        css: cs.css.map(function (css) { return css.chromeValidPath; }),
                        js: cs.js.map(function (js) { return js.chromeValidPath; }),
                        matches: cs.matches
                    };
                }),
                web_accessible_resources: this.web_accessible_resources.map(function (war) { return war.chromeValidPath; }),
                content_security_policy: 'script-src \'self\' \'unsafe-eval\'; object-src \'self\''
            };
        };
        ContentfullManifest.manifest_version = 2;
        return ContentfullManifest;
    }());
    ChromeExtension.ContentfullManifest = ContentfullManifest;
})(ChromeExtension || (ChromeExtension = {}));
exports.ContentfullManifest = ChromeExtension.ContentfullManifest;
module.exports = ChromeExtension;
//# sourceMappingURL=ContentfullManifest.js.map