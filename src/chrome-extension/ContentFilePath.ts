import { join } from 'path';
import { existsSync } from 'fs';
import { Project } from './Project';

module ChromeExtension {
    export class ContentFilePath {
        constructor(public project: Project, public fileRelativePath: string) {
            this.fileRelativePath = this.fileRelativePath.trim();
        }

        public get fullPath(): string {
            return this.project.location.root.subpath(this.fileRelativePath).path;
        }

        public get exists(): boolean {
            return existsSync(this.fullPath);
        }
    }
}

export = ChromeExtension;
exports.ContentFilePath = ChromeExtension.ContentFilePath;