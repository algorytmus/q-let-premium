import { Path } from './Path';
import { readFileSync } from 'fs';
import { ContentFilePath } from './ContentFilePath';
import { ChromePath } from './ChromePath';
import { Project } from './Project';
import { ProjectPath } from './ProjectPath';

import { join, basename, parse, resolve } from 'path';

let Regex = require('regex');

module ChromeExtension {

    type dependencies = string[];

    export class ContentFile {

        constructor(public chromePath: ChromePath, private project: Project) {
            if (!this.projectPath.exists) {
                throw new Error(`Cannot find file ${this.projectPath.path}`);
            }
        }

        public get projectPath(): ProjectPath {
            return this.chromePath.toProjectPath(this.project);
        }

        public get dependencies(): ContentFile[] {
            return ContentFile.validateDependencies(this.chromePath, this.chromeDependencies, this.project)
                .map(dependency => new ContentFile(this.convertToCompiledReference(dependency), this.project));
        }

        private get chromeDependencies(): ChromePath[] {
            return new DependencyAnalyzer(this.projectPath.read())
                .dependencies
                .map(dependencyString => new ChromePath(dependencyString));
        }

        private static validateDependencies(
            dependant: ChromePath,
            dependencies: ChromePath[],
            project: Project): ChromePath[] {
            dependencies = dependencies
                .filter(d => d.chromeValidPath.indexOf('node_modules') === -1)
                .map(dependency => {
                    if (dependency.isRelative) {
                        return ContentFile.convertToProjectRelativePath(dependant, dependency, project);
                    } else {
                        return dependency;
                    }
                });
            return dependencies;
        }

        private static convertToProjectRelativePath(dependant: ChromePath, dependency: ChromePath, project: Project): ChromePath {
            let dependantProjectPath: ProjectPath = dependant.toProjectPath(project);
            let dependencyWithReplacedStashes = new Path(ContentFile.replaceSlashes(dependency.chromeValidPath, '/', '\\'));
            let dependencyNormalizedToSystemPath = new ProjectPath(dependantProjectPath.resolveParent()).resolveRelative(dependencyWithReplacedStashes);
            let chromePathRaw = ContentFile.replaceSlashes(dependencyNormalizedToSystemPath.path.replace(resolve(project.location.root.path), ''), '\\', '/');
            chromePathRaw = chromePathRaw.substring(1);
            let chrPath = new ChromePath(chromePathRaw);
            return chrPath;
        }

        private static replaceSlashes(path: string, from: string, to: string): string {
            for (let i = 0 ; i < 100 ; i++) {
                path = path.replace(from, to);
            }
            return path;
        }

        private convertToCompiledReference(chromePath: ChromePath): ChromePath {
            return this.project
                .projectManifest
                .properties
                .dependencies
                .filter(d => d.properties.tsPath === chromePath.chromeValidPath)
                .map(d => new ChromePath(d.properties.jsPath))
                .concat([ new ChromePath(chromePath.chromeValidPath.replace('.ts', '.js')) ])[0];
        }

        private static validateDependency(dependant: ProjectPath, dependency: ProjectPath): void {
            if (!dependency.exists) {
                throw new Error(`Cannot find file ${dependency.path} referneced in ${dependant.path}`);
            }
        }
    }

    export class DependencyAnalyzer {
        private static DependsOnExpression = '//dependsOn=';
        private static ReferenceExpressionRegex = /\/\/\/ <reference path="(.*?)" \/>/;

        constructor(private data: string) {}

        public get dependencies(): dependencies {
            return DependencyAnalyzer.transformDataToDependencies(this.data);
        }

        private static hasDependency(str: string): boolean {
            return str.indexOf(DependencyAnalyzer.DependsOnExpression) !== -1;
        }

        private static parseDependency(str: string): string {
            return str.replace(DependencyAnalyzer.DependsOnExpression, '').trim();
        }

        private static split(str: string): string[] {
            return str.split('\n');
        }

        private static transformDataToDependencies(data: string): dependencies {
            const lines = DependencyAnalyzer.split(data);
            return DependencyAnalyzer
                    .transformLinesToDependencies(lines)
                    .concat(DependencyAnalyzer.transformLinesToReferences(lines));
        }

        private static transformLinesToDependencies(lines: string[]): string[] {
            return lines
                .filter(DependencyAnalyzer.hasDependency)
                .map(DependencyAnalyzer.parseDependency);
        }

        private static transformLinesToReferences(lines: string[]): string[] {
            return lines
                .map(line => DependencyAnalyzer.ReferenceExpressionRegex.exec(line))
                .filter(result => result !== null)
                .map(result => result !== null ? result[1] : '');
        }
    }
}

export = ChromeExtension;
exports.ContentFile = ChromeExtension.ContentFile;
exports.DependencyAnalyzer = ChromeExtension.DependencyAnalyzer;