"use strict";
var ChromeExtension;
(function (ChromeExtension) {
    var WebAccessibleScript = /** @class */ (function () {
        function WebAccessibleScript(rootFile) {
            this.rootFile = rootFile;
        }
        return WebAccessibleScript;
    }());
    ChromeExtension.WebAccessibleScript = WebAccessibleScript;
})(ChromeExtension || (ChromeExtension = {}));
exports.WebAccessibleScript = ChromeExtension.WebAccessibleScript;
module.exports = ChromeExtension;
//# sourceMappingURL=WebAccessibleScript.js.map