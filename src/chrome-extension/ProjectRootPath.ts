import { Path } from './Path';
import { ProjectPath } from './ProjectPath';
import { basename } from 'path';

module ChromeExtension {
    export class ProjectRootPath {
        private static PROJECT_MANIFEST: string = 'project.manifest';

        constructor(public root: ProjectPath) {}

        public get name(): string {
            return basename(this.root.path);
        }

        public get projectManifest(): ProjectPath {
            return this.root.subpath(ProjectRootPath.PROJECT_MANIFEST);
        }
    }
}

export = ChromeExtension;
exports.ProjectRootPath = ChromeExtension.ProjectRootPath;