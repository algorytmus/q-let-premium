import { ProjectManifest } from './ProjectManifest';
import { ChromePath } from './ChromePath';

module ChromeExtension {
    export class ContentfullManifest {
        private static manifest_version: number = 2;

        constructor(private projectManifest: ProjectManifest,
            public content_scripts: {css: ChromePath[], js: ChromePath[], matches: string[]}[],
            public web_accessible_resources: ChromePath[],
            private version: string
        ) {}

        public stringifyManifest(): string {
            return JSON.stringify(this.createPackageManifest(), null, '\t');
        }

        public get allFiles(): ChromePath[] {
            return this.content_scripts
                .map(contentScript =>
                    contentScript.css
                        .concat(contentScript.js)
                )
                .concat(this.web_accessible_resources)
                .reduce( (previousValue, currentValue, _) => previousValue.concat(currentValue) );
        }

        private createPackageManifest():
            {
                manifest_version: number,
                version: string,
                name: string,
                description: string,
                permissions: string[],
                content_scripts: {css: string[], js: string[], matches: string[]}[],
                web_accessible_resources: string[],
                content_security_policy: string
            } {
            return {
                manifest_version: ContentfullManifest.manifest_version,
                version: this.version,
                name: this.projectManifest.properties.name,
                description: this.projectManifest.properties.description,
                permissions: this.projectManifest.properties.permissions,
                content_scripts: this.content_scripts.map(cs => { return {
                        css: cs.css.map(css => css.chromeValidPath),
                        js: cs.js.map(js => js.chromeValidPath),
                        matches: cs.matches
                    }; }
                ),
                web_accessible_resources: this.web_accessible_resources.map(
                    war => war.chromeValidPath
                ),
                content_security_policy: 'script-src \'self\' \'unsafe-eval\'; object-src \'self\''
            };
        }
    }
}

export = ChromeExtension;
exports.ContentfullManifest = ChromeExtension.ContentfullManifest;