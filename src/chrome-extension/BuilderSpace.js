"use strict";
var ProjectBuilderSpace_1 = require("./ProjectBuilderSpace");
var ChromeExtension;
(function (ChromeExtension) {
    var BuilderSpace = /** @class */ (function () {
        function BuilderSpace(location) {
            this.location = location;
        }
        BuilderSpace.prototype.createProjectBuilderSpace = function (project) {
            return new ProjectBuilderSpace_1.ProjectBuilderSpace(this, project);
        };
        return BuilderSpace;
    }());
    ChromeExtension.BuilderSpace = BuilderSpace;
})(ChromeExtension || (ChromeExtension = {}));
exports.BuilderSpace = ChromeExtension.BuilderSpace;
module.exports = ChromeExtension;
//# sourceMappingURL=BuilderSpace.js.map