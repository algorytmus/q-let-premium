"use strict";
var ChromeExtension;
(function (ChromeExtension) {
    var ChromePath = /** @class */ (function () {
        function ChromePath(chromeValidPath) {
            this.chromeValidPath = chromeValidPath;
        }
        ChromePath.prototype.toProjectPath = function (project) {
            return project.location.root.subpath(this.chromeValidPath.replace('/', '\\'));
        };
        ChromePath.prototype.toRelativeChromePath = function (chromePath) {
            return new ChromePath(this.chromeValidPath.replace('./..', '/..' + chromePath.chromeValidPath));
        };
        Object.defineProperty(ChromePath.prototype, "isRelative", {
            get: function () {
                return this.chromeValidPath.indexOf('./') > -1 || this.chromeValidPath.indexOf('..') > -1;
            },
            enumerable: true,
            configurable: true
        });
        ChromePath.prototype.toBuilderPath = function (projectBuilderSpace) {
            return projectBuilderSpace.projectBuildPath.root.subpath(this.chromeValidPath.replace('/', '\\'));
        };
        ChromePath.prototype.isOfExtension = function (extension) {
            return this.chromeValidPath.lastIndexOf(extension) === this.chromeValidPath.length - extension.length;
        };
        return ChromePath;
    }());
    ChromeExtension.ChromePath = ChromePath;
})(ChromeExtension || (ChromeExtension = {}));
exports.ChromePath = ChromeExtension.ChromePath;
module.exports = ChromeExtension;
//# sourceMappingURL=ChromePath.js.map