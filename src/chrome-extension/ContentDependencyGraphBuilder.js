"use strict";
var ContentDependencyGraph_1 = require("./ContentDependencyGraph");
var ChromeExtension;
(function (ChromeExtension) {
    var ContentDependencyGraphBuilder = /** @class */ (function () {
        function ContentDependencyGraphBuilder(contentScript) {
            this.contentScript = contentScript;
            if (!contentScript.projectPath.exists) {
                throw new Error("File does not exist in this scope: " + contentScript.projectPath.path);
            }
        }
        ContentDependencyGraphBuilder.prototype.build = function () {
            var dependencyGraph = new ContentDependencyGraph_1.ContentDependencyGraph;
            ContentDependencyGraphBuilder.addDependencies(dependencyGraph, this.contentScript);
            return dependencyGraph;
        };
        ContentDependencyGraphBuilder.addDependencies = function (dependencyGraph, dependant) {
            if (dependencyGraph.hasDependant(dependant)) {
                return;
            }
            dependencyGraph.addDependant(dependant);
            dependant.dependencies
                .forEach(function (dependency) { return ContentDependencyGraphBuilder.addDependency(dependencyGraph, dependant, dependency); });
        };
        ContentDependencyGraphBuilder.addDependency = function (dependencyGraph, dependant, dependency) {
            ContentDependencyGraphBuilder.addDependencies(dependencyGraph, dependency);
            dependencyGraph.addDependency(dependant, dependency);
        };
        return ContentDependencyGraphBuilder;
    }());
    ChromeExtension.ContentDependencyGraphBuilder = ContentDependencyGraphBuilder;
})(ChromeExtension || (ChromeExtension = {}));
exports.ContentDependencyGraphBuilder = ChromeExtension.ContentDependencyGraphBuilder;
module.exports = ChromeExtension;
//# sourceMappingURL=ContentDependencyGraphBuilder.js.map