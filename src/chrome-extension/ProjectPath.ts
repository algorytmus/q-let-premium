import { Path } from './Path';

module ChromeExtension {
    export class ProjectPath extends Path {
        constructor(public path: string) {
            super(path);
        }

        public subpath(entry: string): ProjectPath {
            return new ProjectPath(this.resolveSubpath(entry));
        }
    }
}

export = ChromeExtension;
exports.ProjectPath = ChromeExtension.ProjectPath;