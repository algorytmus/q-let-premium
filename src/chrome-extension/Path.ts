import { join, basename, parse, resolve } from 'path';
import { existsSync, writeFileSync, unlinkSync, mkdirSync, copyFile, rmdirSync, copyFileSync, readdirSync, statSync, readFileSync } from 'fs';
import { Project } from './Project';

module ChromeExtension {
    export class Path {

        constructor(public readonly path: string) {}

        protected resolveSubpath(entryName: string): string {
            return join(this.path, entryName);
        }

        public create(): void {
            if (!this.exists) {
                mkdirSync(this.path);
            }
        }

        public get exists(): boolean {
            return existsSync(this.path);
        }

        public resolveRelative(path: Path): Path {
            return new Path(resolve(this.path, path.path));
        }

        public resolveParent(): string {
            return parse(this.path).dir;
        }

        public remove(): void {
            if (this.exists) {
                this.removeDirRecursively(this.path);
            }
        }

        private removeDirRecursively(dir: string): void {
            readdirSync(dir)
                .map(entry => join(dir, entry))
                .forEach(subEntry => statSync(subEntry).isDirectory()
                    ? this.removeDirRecursively(subEntry) : unlinkSync(subEntry) );
            rmdirSync(dir);
        }

        public write(content: string): void {
            writeFileSync(this.path, content);
        }

        public read(): string {
            return readFileSync(this.path).toString();
        }

        public copyTo<TPath extends Path>(destination: TPath): void {
            require('cpx').copySync(this.path, destination.path);
        }
    }
}

export = ChromeExtension;
exports.Path = ChromeExtension.Path;