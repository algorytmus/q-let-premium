import { ContentScript } from './ContentScript';
import { WebAccessibleScript } from './WebAccessibleScript';
import { Dependency } from './Dependency';

module ChromeExtension {
    export class ProjectManifest {
        constructor(public properties:
        {
            name: string,
            description: string;
            permissions: string[];
            contentScripts: ContentScript[];
            webAccessibleScripts: WebAccessibleScript[];
            dependencies: Dependency[];
        }) {}
    }
}

export = ChromeExtension;
exports.ProjectManifest = ChromeExtension.ProjectManifest;