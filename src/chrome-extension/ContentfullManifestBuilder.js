"use strict";
var ContentDependencyGraphBuilder_1 = require("./ContentDependencyGraphBuilder");
var ContentFile_1 = require("./ContentFile");
var ContentfullManifest_1 = require("./ContentfullManifest");
var ChromeExtension;
(function (ChromeExtension) {
    var ContentfullManifestBuilder = /** @class */ (function () {
        function ContentfullManifestBuilder(project, manifest) {
            this.project = project;
            this.manifest = manifest;
        }
        ContentfullManifestBuilder.prototype.build = function () {
            var _this = this;
            var contentScriptsMapped = this.manifest.properties.contentScripts
                .map(function (cs) {
                return {
                    contentScript: cs,
                    contentGraph: ContentfullManifestBuilder.createContentGraph(cs.rootFile, _this.project)
                };
            });
            var webAccessibleScriptsMapped = this.manifest.properties.webAccessibleScripts
                .map(function (was) {
                return {
                    webAccessibleScript: was,
                    webAccessibleGraph: ContentfullManifestBuilder.createContentGraph(was.rootFile, _this.project)
                };
            });
            return new ContentfullManifest_1.ContentfullManifest(this.manifest, contentScriptsMapped
                .map(function (csg) {
                return {
                    css: csg.contentGraph.overallOrder('.css'),
                    js: csg.contentGraph.overallOrder('.js'),
                    matches: csg.contentScript.matchesUrlExpressions
                };
            }), webAccessibleScriptsMapped
                .map(function (was) { return was.webAccessibleGraph.overallOrder(''); })
                .reduce(function (previousValue, currentValue, _) { return previousValue.concat(currentValue); }), '0.0.1');
        };
        ContentfullManifestBuilder.createContentGraph = function (chromePath, project) {
            return new ContentDependencyGraphBuilder_1.ContentDependencyGraphBuilder(new ContentFile_1.ContentFile(chromePath, project)).build();
        };
        return ContentfullManifestBuilder;
    }());
    ChromeExtension.ContentfullManifestBuilder = ContentfullManifestBuilder;
})(ChromeExtension || (ChromeExtension = {}));
exports.ContentfullManifestBuilder = ChromeExtension.ContentfullManifestBuilder;
module.exports = ChromeExtension;
//# sourceMappingURL=ContentfullManifestBuilder.js.map