import { ContentScript } from './ContentScript';
import { ContentDependencyGraphBuilder } from './ContentDependencyGraphBuilder';
import { ContentFilePath } from './ContentFilePath';
import { ContentFile } from './ContentFile';
import { ProjectManifest } from './ProjectManifest';
import { ContentfullManifest } from './ContentfullManifest';
import { relative } from 'path';
import { Project } from './Project';
import { DepGraph } from 'dependency-graph';
import { ChromePath } from './ChromePath';
import { ContentDependencyGraph } from './ContentDependencyGraph';

module ChromeExtension {
    export class ContentfullManifestBuilder {
        constructor(private project: Project, private manifest: ProjectManifest) {}

        public build(): ContentfullManifest {
            let contentScriptsMapped =
            this.manifest.properties.contentScripts
                .map( cs => { return {
                        contentScript: cs,
                        contentGraph: ContentfullManifestBuilder.createContentGraph(cs.rootFile, this.project)
                    }; });

            let webAccessibleScriptsMapped =
                    this.manifest.properties.webAccessibleScripts
                        .map( was => { return {
                            webAccessibleScript: was,
                            webAccessibleGraph: ContentfullManifestBuilder.createContentGraph(was.rootFile, this.project)
                            }; });

            return new ContentfullManifest(
                this.manifest,
                contentScriptsMapped
                    .map(csg => { return {
                                css : csg.contentGraph.overallOrder('.css'),
                                js : csg.contentGraph.overallOrder('.js'),
                                matches : csg.contentScript.matchesUrlExpressions
                            };
                        }
                    ),
                webAccessibleScriptsMapped
                    .map(was => was.webAccessibleGraph.overallOrder(''))
                    .reduce( (previousValue, currentValue, _) => previousValue.concat(currentValue) ),
                '0.0.1'
            );
        }

        private static createContentGraph(chromePath: ChromePath, project: Project): ContentDependencyGraph {
            return new ContentDependencyGraphBuilder(
                new ContentFile(
                    chromePath,
                    project
                  )
              ).build();
        }
    }
}

export = ChromeExtension;
exports.ContentfullManifestBuilder = ChromeExtension.ContentfullManifestBuilder;