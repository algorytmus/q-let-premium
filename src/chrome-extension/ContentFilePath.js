"use strict";
var fs_1 = require("fs");
var ChromeExtension;
(function (ChromeExtension) {
    var ContentFilePath = /** @class */ (function () {
        function ContentFilePath(project, fileRelativePath) {
            this.project = project;
            this.fileRelativePath = fileRelativePath;
            this.fileRelativePath = this.fileRelativePath.trim();
        }
        Object.defineProperty(ContentFilePath.prototype, "fullPath", {
            get: function () {
                return this.project.location.root.subpath(this.fileRelativePath).path;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ContentFilePath.prototype, "exists", {
            get: function () {
                return fs_1.existsSync(this.fullPath);
            },
            enumerable: true,
            configurable: true
        });
        return ContentFilePath;
    }());
    ChromeExtension.ContentFilePath = ContentFilePath;
})(ChromeExtension || (ChromeExtension = {}));
exports.ContentFilePath = ChromeExtension.ContentFilePath;
module.exports = ChromeExtension;
//# sourceMappingURL=ContentFilePath.js.map