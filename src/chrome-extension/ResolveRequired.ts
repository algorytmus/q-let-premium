import { ProjectManifest } from './ProjectManifest';
import { relative } from 'path';

module ChromeExtension {
    export class ResolveRequired {
        public static resolve<T>(noduleName: string): T {
            let resolved: T = require( relative(__dirname, noduleName) ).projectManifest as T;
            if (resolved === undefined) {
                throw new Error(`Cannot resolve module name ${noduleName} as type T`);
            }
            return resolved;
        }
    }
}

export = ChromeExtension;
exports.ResolveRequired = ChromeExtension.ResolveRequired;