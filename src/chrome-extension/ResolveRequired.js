"use strict";
var path_1 = require("path");
var ChromeExtension;
(function (ChromeExtension) {
    var ResolveRequired = /** @class */ (function () {
        function ResolveRequired() {
        }
        ResolveRequired.resolve = function (noduleName) {
            var resolved = require(path_1.relative(__dirname, noduleName)).projectManifest;
            if (resolved === undefined) {
                throw new Error("Cannot resolve module name " + noduleName + " as type T");
            }
            return resolved;
        };
        return ResolveRequired;
    }());
    ChromeExtension.ResolveRequired = ResolveRequired;
})(ChromeExtension || (ChromeExtension = {}));
exports.ResolveRequired = ChromeExtension.ResolveRequired;
module.exports = ChromeExtension;
//# sourceMappingURL=ResolveRequired.js.map