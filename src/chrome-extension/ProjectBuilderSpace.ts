import { Project } from './Project';
import { BuilderSpace } from './BuilderSpace';
import { rmdirSync, mkdirSync, existsSync } from 'fs';
import { join } from 'path';
import { BuilderPath } from './BuilderPath';
import { ProjectBuilderRootPath } from './ProjectBuilderRootPath';

module ChromeExtension {
    export class ProjectBuilderSpace {
        public projectBuildPath: ProjectBuilderRootPath;

        constructor(
            private builderSpace: BuilderSpace,
            private project: Project) {
            this.projectBuildPath = new ProjectBuilderRootPath(builderSpace.location.subpath(project.name));
            this.projectBuildPath.root.create();
        }

        public ensureReadyForFreshBuild(): void {
            this.projectBuildPath.root.remove();
            this.projectBuildPath.root.create();
        }
    }
}

export = ChromeExtension;
exports.ProjectBuilderSpace = ChromeExtension.ProjectBuilderSpace;