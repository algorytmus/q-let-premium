"use strict";
var BuiltPackage_1 = require("./BuiltPackage");
var ContentfullManifestBuilder_1 = require("./ContentfullManifestBuilder");
var ResolveRequired_1 = require("./ResolveRequired");
var ChromeExtension;
(function (ChromeExtension) {
    var Project = /** @class */ (function () {
        function Project(location) {
            this.location = location;
            this.projectManifest = ResolveRequired_1.ResolveRequired.resolve(location.projectManifest.path);
        }
        Object.defineProperty(Project.prototype, "name", {
            get: function () {
                return this.location.name;
            },
            enumerable: true,
            configurable: true
        });
        Project.prototype.build = function (builderSpace) {
            var _this = this;
            var projetcBuilderSpace = builderSpace.createProjectBuilderSpace(this);
            projetcBuilderSpace.ensureReadyForFreshBuild();
            var manifest = this.buildManifest();
            this.saveManifest(manifest, projetcBuilderSpace);
            manifest
                .allFiles
                .forEach(function (f) {
                return f.toProjectPath(_this).copyTo(f.toBuilderPath(projetcBuilderSpace).parent);
            });
            return new BuiltPackage_1.BuiltPackage(projetcBuilderSpace, this);
        };
        Project.prototype.buildManifest = function () {
            return new ContentfullManifestBuilder_1.ContentfullManifestBuilder(this, this.projectManifest).build();
        };
        Project.prototype.saveManifest = function (manifest, projectBuilderSpace) {
            projectBuilderSpace
                .projectBuildPath
                .manifest
                .write(manifest.stringifyManifest());
        };
        return Project;
    }());
    ChromeExtension.Project = Project;
})(ChromeExtension || (ChromeExtension = {}));
exports.Project = ChromeExtension.Project;
module.exports = ChromeExtension;
//# sourceMappingURL=Project.js.map