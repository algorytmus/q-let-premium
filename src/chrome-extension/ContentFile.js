"use strict";
var Path_1 = require("./Path");
var ChromePath_1 = require("./ChromePath");
var ProjectPath_1 = require("./ProjectPath");
var path_1 = require("path");
var Regex = require('regex');
var ChromeExtension;
(function (ChromeExtension) {
    var ContentFile = /** @class */ (function () {
        function ContentFile(chromePath, project) {
            this.chromePath = chromePath;
            this.project = project;
            if (!this.projectPath.exists) {
                throw new Error("Cannot find file " + this.projectPath.path);
            }
        }
        Object.defineProperty(ContentFile.prototype, "projectPath", {
            get: function () {
                return this.chromePath.toProjectPath(this.project);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ContentFile.prototype, "dependencies", {
            get: function () {
                var _this = this;
                return ContentFile.validateDependencies(this.chromePath, this.chromeDependencies, this.project)
                    .map(function (dependency) { return new ContentFile(_this.convertToCompiledReference(dependency), _this.project); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ContentFile.prototype, "chromeDependencies", {
            get: function () {
                return new DependencyAnalyzer(this.projectPath.read())
                    .dependencies
                    .map(function (dependencyString) { return new ChromePath_1.ChromePath(dependencyString); });
            },
            enumerable: true,
            configurable: true
        });
        ContentFile.validateDependencies = function (dependant, dependencies, project) {
            dependencies = dependencies
                .filter(function (d) { return d.chromeValidPath.indexOf('node_modules') === -1; })
                .map(function (dependency) {
                if (dependency.isRelative) {
                    return ContentFile.convertToProjectRelativePath(dependant, dependency, project);
                }
                else {
                    return dependency;
                }
            });
            return dependencies;
        };
        ContentFile.convertToProjectRelativePath = function (dependant, dependency, project) {
            var dependantProjectPath = dependant.toProjectPath(project);
            var dependencyWithReplacedStashes = new Path_1.Path(ContentFile.replaceSlashes(dependency.chromeValidPath, '/', '\\'));
            var dependencyNormalizedToSystemPath = new ProjectPath_1.ProjectPath(dependantProjectPath.resolveParent()).resolveRelative(dependencyWithReplacedStashes);
            var chromePathRaw = ContentFile.replaceSlashes(dependencyNormalizedToSystemPath.path.replace(path_1.resolve(project.location.root.path), ''), '\\', '/');
            chromePathRaw = chromePathRaw.substring(1);
            var chrPath = new ChromePath_1.ChromePath(chromePathRaw);
            return chrPath;
        };
        ContentFile.replaceSlashes = function (path, from, to) {
            for (var i = 0; i < 100; i++) {
                path = path.replace(from, to);
            }
            return path;
        };
        ContentFile.prototype.convertToCompiledReference = function (chromePath) {
            return this.project
                .projectManifest
                .properties
                .dependencies
                .filter(function (d) { return d.properties.tsPath === chromePath.chromeValidPath; })
                .map(function (d) { return new ChromePath_1.ChromePath(d.properties.jsPath); })
                .concat([new ChromePath_1.ChromePath(chromePath.chromeValidPath.replace('.ts', '.js'))])[0];
        };
        ContentFile.validateDependency = function (dependant, dependency) {
            if (!dependency.exists) {
                throw new Error("Cannot find file " + dependency.path + " referneced in " + dependant.path);
            }
        };
        return ContentFile;
    }());
    ChromeExtension.ContentFile = ContentFile;
    var DependencyAnalyzer = /** @class */ (function () {
        function DependencyAnalyzer(data) {
            this.data = data;
        }
        Object.defineProperty(DependencyAnalyzer.prototype, "dependencies", {
            get: function () {
                return DependencyAnalyzer.transformDataToDependencies(this.data);
            },
            enumerable: true,
            configurable: true
        });
        DependencyAnalyzer.hasDependency = function (str) {
            return str.indexOf(DependencyAnalyzer.DependsOnExpression) !== -1;
        };
        DependencyAnalyzer.parseDependency = function (str) {
            return str.replace(DependencyAnalyzer.DependsOnExpression, '').trim();
        };
        DependencyAnalyzer.split = function (str) {
            return str.split('\n');
        };
        DependencyAnalyzer.transformDataToDependencies = function (data) {
            var lines = DependencyAnalyzer.split(data);
            return DependencyAnalyzer
                .transformLinesToDependencies(lines)
                .concat(DependencyAnalyzer.transformLinesToReferences(lines));
        };
        DependencyAnalyzer.transformLinesToDependencies = function (lines) {
            return lines
                .filter(DependencyAnalyzer.hasDependency)
                .map(DependencyAnalyzer.parseDependency);
        };
        DependencyAnalyzer.transformLinesToReferences = function (lines) {
            return lines
                .map(function (line) { return DependencyAnalyzer.ReferenceExpressionRegex.exec(line); })
                .filter(function (result) { return result !== null; })
                .map(function (result) { return result !== null ? result[1] : ''; });
        };
        DependencyAnalyzer.DependsOnExpression = '//dependsOn=';
        DependencyAnalyzer.ReferenceExpressionRegex = /\/\/\/ <reference path="(.*?)" \/>/;
        return DependencyAnalyzer;
    }());
    ChromeExtension.DependencyAnalyzer = DependencyAnalyzer;
})(ChromeExtension || (ChromeExtension = {}));
exports.ContentFile = ChromeExtension.ContentFile;
exports.DependencyAnalyzer = ChromeExtension.DependencyAnalyzer;
module.exports = ChromeExtension;
//# sourceMappingURL=ContentFile.js.map