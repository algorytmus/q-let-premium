import { ContentFile } from './ContentFile';
import { } from './../node_modules/dependency-graph/lib/index';
import { DepGraph } from 'dependency-graph';
import { ChromePath } from './ChromePath';

module ChromeExtension {
    export class ContentDependencyGraph {
        private graph: DepGraph<ContentFile> = new DepGraph<ContentFile>();

        public hasDependant(dependency: ContentFile): boolean {
            return this.graph.hasNode(dependency.chromePath.chromeValidPath);
        }

        public addDependant(dependant: ContentFile): void {
            this.graph.addNode(dependant.chromePath.chromeValidPath, dependant);
        }

        public addDependency(dependant: ContentFile, dependency: ContentFile): void {
            this.graph.addDependency(dependant.chromePath.chromeValidPath, dependency.chromePath.chromeValidPath);
        }

        public overallOrder(contentFileExtension: string): ChromePath[] {
            return this.graph
                .overallOrder()
                .map(d => new ChromePath(d))
                .filter(d => d.isOfExtension(contentFileExtension));
        }
    }
}

export = ChromeExtension;
exports.ContentDependencyGraph = ChromeExtension.ContentDependencyGraph;