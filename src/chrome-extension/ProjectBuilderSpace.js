"use strict";
var ProjectBuilderRootPath_1 = require("./ProjectBuilderRootPath");
var ChromeExtension;
(function (ChromeExtension) {
    var ProjectBuilderSpace = /** @class */ (function () {
        function ProjectBuilderSpace(builderSpace, project) {
            this.builderSpace = builderSpace;
            this.project = project;
            this.projectBuildPath = new ProjectBuilderRootPath_1.ProjectBuilderRootPath(builderSpace.location.subpath(project.name));
            this.projectBuildPath.root.create();
        }
        ProjectBuilderSpace.prototype.ensureReadyForFreshBuild = function () {
            this.projectBuildPath.root.remove();
            this.projectBuildPath.root.create();
        };
        return ProjectBuilderSpace;
    }());
    ChromeExtension.ProjectBuilderSpace = ProjectBuilderSpace;
})(ChromeExtension || (ChromeExtension = {}));
exports.ProjectBuilderSpace = ChromeExtension.ProjectBuilderSpace;
module.exports = ChromeExtension;
//# sourceMappingURL=ProjectBuilderSpace.js.map