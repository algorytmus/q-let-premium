
module ChromeExtension {
    export class Dependency {
        constructor( public readonly properties: {
                name: string,
                jsPath: string,
                tsPath: string
            }
        ) {}
    }
}

export = ChromeExtension;
exports.Dependency = ChromeExtension.Dependency;