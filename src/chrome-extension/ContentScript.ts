import { ChromePath } from './ChromePath';

module ChromeExtension {
    export class ContentScript {
        constructor(public matchesUrlExpressions: string[], public rootFile: ChromePath) {}
    }
}

export = ChromeExtension;
exports.ContentScript = ChromeExtension.ContentScript;