"use strict";
var ChromeExtension;
(function (ChromeExtension) {
    var ProjectManifest = /** @class */ (function () {
        function ProjectManifest(properties) {
            this.properties = properties;
        }
        return ProjectManifest;
    }());
    ChromeExtension.ProjectManifest = ProjectManifest;
})(ChromeExtension || (ChromeExtension = {}));
exports.ProjectManifest = ChromeExtension.ProjectManifest;
module.exports = ChromeExtension;
//# sourceMappingURL=ProjectManifest.js.map