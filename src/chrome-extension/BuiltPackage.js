"use strict";
var ChromeExtension;
(function (ChromeExtension) {
    var BuiltPackage = /** @class */ (function () {
        function BuiltPackage(projectBuilderSpace, project) {
            this.projectBuilderSpace = projectBuilderSpace;
            this.project = project;
        }
        return BuiltPackage;
    }());
    ChromeExtension.BuiltPackage = BuiltPackage;
})(ChromeExtension || (ChromeExtension = {}));
exports.BuiltPackage = ChromeExtension.BuiltPackage;
module.exports = ChromeExtension;
//# sourceMappingURL=BuiltPackage.js.map