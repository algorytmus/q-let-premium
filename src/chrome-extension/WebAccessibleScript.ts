import { ChromePath } from './ChromePath';

module ChromeExtension {
    export class WebAccessibleScript {
        constructor(public rootFile: ChromePath) {}
    }
}

export = ChromeExtension;
exports.WebAccessibleScript = ChromeExtension.WebAccessibleScript;