import { BuilderPath } from './BuilderPath';
import { Project } from './Project';
import { ProjectPath } from './ProjectPath';
import { ProjectBuilderSpace } from './ProjectBuilderSpace';
import { Path } from './Path';

module ChromeExtension {
    export class ChromePath {
        constructor(public chromeValidPath: string) {}

        public toProjectPath(project: Project): ProjectPath {
            return project.location.root.subpath(this.chromeValidPath.replace('/', '\\'));
        }

        public toRelativeChromePath(chromePath: ChromePath): ChromePath {
            return new ChromePath(this.chromeValidPath.replace('./..', '/..' + chromePath.chromeValidPath));
        }

        public get isRelative(): Boolean {
            return this.chromeValidPath.indexOf('./') > -1 || this.chromeValidPath.indexOf('..') > -1;
        }

        public toBuilderPath(projectBuilderSpace: ProjectBuilderSpace): BuilderPath {
            return projectBuilderSpace.projectBuildPath.root.subpath(this.chromeValidPath.replace('/', '\\'));
        }

        public isOfExtension(extension: string): boolean {
            return this.chromeValidPath.lastIndexOf(extension) === this.chromeValidPath.length - extension.length;
        }
    }
}

export = ChromeExtension;
exports.ChromePath = ChromeExtension.ChromePath;