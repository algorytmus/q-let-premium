"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Path_1 = require("./Path");
var ChromeExtension;
(function (ChromeExtension) {
    var BuilderPath = /** @class */ (function (_super) {
        __extends(BuilderPath, _super);
        function BuilderPath(path) {
            var _this = _super.call(this, path) || this;
            _this.path = path;
            return _this;
        }
        BuilderPath.prototype.subpath = function (entry) {
            return new BuilderPath(this.resolveSubpath(entry));
        };
        Object.defineProperty(BuilderPath.prototype, "parent", {
            get: function () {
                return new BuilderPath(_super.prototype.resolveParent.call(this));
            },
            enumerable: true,
            configurable: true
        });
        return BuilderPath;
    }(Path_1.Path));
    ChromeExtension.BuilderPath = BuilderPath;
})(ChromeExtension || (ChromeExtension = {}));
exports.BuilderPath = ChromeExtension.BuilderPath;
module.exports = ChromeExtension;
//# sourceMappingURL=BuilderPath.js.map