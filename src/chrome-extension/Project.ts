import { BuiltPackage } from './BuiltPackage';
import { BuilderSpace } from './BuilderSpace';
import { ContentfullManifestBuilder } from './ContentfullManifestBuilder';
import { ContentfullManifest } from './ContentfullManifest';
import { ProjectManifest } from './ProjectManifest';
import { basename, join } from 'path';
import { ResolveRequired } from './ResolveRequired';
import { ProjectBuilderSpace } from './ProjectBuilderSpace';
import { ProjectPath } from './ProjectPath';
import { ProjectRootPath } from './ProjectRootPath';

module ChromeExtension {
    export class Project {
        public projectManifest: ProjectManifest;

        constructor(public location: ProjectRootPath) {
            this.projectManifest = ResolveRequired.resolve<ProjectManifest>(location.projectManifest.path);
        }

        public get name(): string {
            return this.location.name;
        }

        public build(builderSpace: BuilderSpace): BuiltPackage {
            let projetcBuilderSpace = builderSpace.createProjectBuilderSpace(this);
            projetcBuilderSpace.ensureReadyForFreshBuild();
            let manifest = this.buildManifest();
            this.saveManifest(manifest, projetcBuilderSpace);

            manifest
                .allFiles
                .forEach(f =>
                    f.toProjectPath(this).copyTo(
                        f.toBuilderPath(projetcBuilderSpace).parent
                    )
                );

            return new BuiltPackage(projetcBuilderSpace, this);
        }

        private buildManifest(): ContentfullManifest {
            return new ContentfullManifestBuilder(this, this.projectManifest).build();
        }

        private saveManifest(manifest: ContentfullManifest, projectBuilderSpace: ProjectBuilderSpace): void {
            projectBuilderSpace
                .projectBuildPath
                .manifest
                .write(manifest.stringifyManifest());
        }
    }
}

export = ChromeExtension;
exports.Project = ChromeExtension.Project;