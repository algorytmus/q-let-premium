"use strict";
var Project_1 = require("./Project");
var ProjectRootPath_1 = require("./ProjectRootPath");
var ProjectPath_1 = require("./ProjectPath");
var ChromeExtension;
(function (ChromeExtension) {
    var Workspace = /** @class */ (function () {
        function Workspace(root) {
            this.root = root;
        }
        Workspace.prototype.projects = function () {
            return [new Project_1.Project(new ProjectRootPath_1.ProjectRootPath(new ProjectPath_1.ProjectPath('./chrome-app')))];
        };
        return Workspace;
    }());
    ChromeExtension.Workspace = Workspace;
})(ChromeExtension || (ChromeExtension = {}));
exports.Workspace = ChromeExtension.Workspace;
module.exports = ChromeExtension;
//# sourceMappingURL=Workspace.js.map