"use strict";
var dependency_graph_1 = require("dependency-graph");
var ChromePath_1 = require("./ChromePath");
var ChromeExtension;
(function (ChromeExtension) {
    var ContentDependencyGraph = /** @class */ (function () {
        function ContentDependencyGraph() {
            this.graph = new dependency_graph_1.DepGraph();
        }
        ContentDependencyGraph.prototype.hasDependant = function (dependency) {
            return this.graph.hasNode(dependency.chromePath.chromeValidPath);
        };
        ContentDependencyGraph.prototype.addDependant = function (dependant) {
            this.graph.addNode(dependant.chromePath.chromeValidPath, dependant);
        };
        ContentDependencyGraph.prototype.addDependency = function (dependant, dependency) {
            this.graph.addDependency(dependant.chromePath.chromeValidPath, dependency.chromePath.chromeValidPath);
        };
        ContentDependencyGraph.prototype.overallOrder = function (contentFileExtension) {
            return this.graph
                .overallOrder()
                .map(function (d) { return new ChromePath_1.ChromePath(d); })
                .filter(function (d) { return d.isOfExtension(contentFileExtension); });
        };
        return ContentDependencyGraph;
    }());
    ChromeExtension.ContentDependencyGraph = ContentDependencyGraph;
})(ChromeExtension || (ChromeExtension = {}));
exports.ContentDependencyGraph = ChromeExtension.ContentDependencyGraph;
module.exports = ChromeExtension;
//# sourceMappingURL=ContentDependencyGraph.js.map