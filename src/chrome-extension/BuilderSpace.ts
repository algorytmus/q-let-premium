import { Project } from './Project';
import { ProjectBuilderSpace } from './ProjectBuilderSpace';
import { join } from 'path';
import { BuilderPath } from './BuilderPath';

module ChromeExtension {
    export class BuilderSpace {
        constructor(public location: BuilderPath) {}

        public createProjectBuilderSpace(project: Project): ProjectBuilderSpace {
            return new ProjectBuilderSpace(this, project);
        }
    }
}

export = ChromeExtension;
exports.BuilderSpace = ChromeExtension.BuilderSpace;