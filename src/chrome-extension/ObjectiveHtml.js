"use strict";
var ChromeExtension;
(function (ChromeExtension) {
    var ObjectiveHtml = /** @class */ (function () {
        function ObjectiveHtml(htmlHref) {
            this.htmlHref = htmlHref;
        }
        ObjectiveHtml.prototype.render = function () {
            return "";
        };
        return ObjectiveHtml;
    }());
    ChromeExtension.ObjectiveHtml = ObjectiveHtml;
})(ChromeExtension || (ChromeExtension = {}));
exports.ObjectiveHtml = ChromeExtension.ObjectiveHtml;
module.exports = ChromeExtension;
//# sourceMappingURL=ObjectiveHtml.js.map