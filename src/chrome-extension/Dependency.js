"use strict";
var ChromeExtension;
(function (ChromeExtension) {
    var Dependency = /** @class */ (function () {
        function Dependency(properties) {
            this.properties = properties;
        }
        return Dependency;
    }());
    ChromeExtension.Dependency = Dependency;
})(ChromeExtension || (ChromeExtension = {}));
exports.Dependency = ChromeExtension.Dependency;
module.exports = ChromeExtension;
//# sourceMappingURL=Dependency.js.map