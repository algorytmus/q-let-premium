
import { Project } from './Project';
import { ProjectRootPath } from './ProjectRootPath';
import { ProjectPath } from './ProjectPath';

module ChromeExtension {
    export class Workspace {
        constructor(private root: string) {}

        public projects(): Project[] {
            return [new Project(new ProjectRootPath(new ProjectPath('./chrome-app')))];
        }
    }
}

export = ChromeExtension;
exports.Workspace = ChromeExtension.Workspace;