"use strict";
var path_1 = require("path");
var fs_1 = require("fs");
var ChromeExtension;
(function (ChromeExtension) {
    var Path = /** @class */ (function () {
        function Path(path) {
            this.path = path;
        }
        Path.prototype.resolveSubpath = function (entryName) {
            return path_1.join(this.path, entryName);
        };
        Path.prototype.create = function () {
            if (!this.exists) {
                fs_1.mkdirSync(this.path);
            }
        };
        Object.defineProperty(Path.prototype, "exists", {
            get: function () {
                return fs_1.existsSync(this.path);
            },
            enumerable: true,
            configurable: true
        });
        Path.prototype.resolveRelative = function (path) {
            return new Path(path_1.resolve(this.path, path.path));
        };
        Path.prototype.resolveParent = function () {
            return path_1.parse(this.path).dir;
        };
        Path.prototype.remove = function () {
            if (this.exists) {
                this.removeDirRecursively(this.path);
            }
        };
        Path.prototype.removeDirRecursively = function (dir) {
            var _this = this;
            fs_1.readdirSync(dir)
                .map(function (entry) { return path_1.join(dir, entry); })
                .forEach(function (subEntry) { return fs_1.statSync(subEntry).isDirectory()
                ? _this.removeDirRecursively(subEntry) : fs_1.unlinkSync(subEntry); });
            fs_1.rmdirSync(dir);
        };
        Path.prototype.write = function (content) {
            fs_1.writeFileSync(this.path, content);
        };
        Path.prototype.read = function () {
            return fs_1.readFileSync(this.path).toString();
        };
        Path.prototype.copyTo = function (destination) {
            require('cpx').copySync(this.path, destination.path);
        };
        return Path;
    }());
    ChromeExtension.Path = Path;
})(ChromeExtension || (ChromeExtension = {}));
exports.Path = ChromeExtension.Path;
module.exports = ChromeExtension;
//# sourceMappingURL=path.js.map