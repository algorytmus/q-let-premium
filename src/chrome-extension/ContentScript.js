"use strict";
var ChromeExtension;
(function (ChromeExtension) {
    var ContentScript = /** @class */ (function () {
        function ContentScript(matchesUrlExpressions, rootFile) {
            this.matchesUrlExpressions = matchesUrlExpressions;
            this.rootFile = rootFile;
        }
        return ContentScript;
    }());
    ChromeExtension.ContentScript = ContentScript;
})(ChromeExtension || (ChromeExtension = {}));
exports.ContentScript = ChromeExtension.ContentScript;
module.exports = ChromeExtension;
//# sourceMappingURL=ContentScript.js.map