"use strict";
var ChromeExtension;
(function (ChromeExtension) {
    var ProjectBuilderRootPath = /** @class */ (function () {
        function ProjectBuilderRootPath(root) {
            this.root = root;
        }
        Object.defineProperty(ProjectBuilderRootPath.prototype, "manifest", {
            get: function () {
                return this.root.subpath(ProjectBuilderRootPath.COMPILED_MANIFEST_NAME);
            },
            enumerable: true,
            configurable: true
        });
        ProjectBuilderRootPath.COMPILED_MANIFEST_NAME = 'manifest.json';
        return ProjectBuilderRootPath;
    }());
    ChromeExtension.ProjectBuilderRootPath = ProjectBuilderRootPath;
})(ChromeExtension || (ChromeExtension = {}));
exports.ProjectBuilderRootPath = ChromeExtension.ProjectBuilderRootPath;
module.exports = ChromeExtension;
//# sourceMappingURL=ProjectBuilderRootPath.js.map