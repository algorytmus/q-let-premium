import { ContentDependencyGraphBuilder } from './ContentDependencyGraphBuilder';
import { ContentFile } from './ContentFile';
import { ChromePath } from './ChromePath';
import { ContentDependencyGraph } from './ContentDependencyGraph';

module ChromeExtension {
    export class ContentDependencyGraphBuilder {
        constructor(private contentScript: ContentFile) {
            if (!contentScript.projectPath.exists) {
                throw new Error(`File does not exist in this scope: ${contentScript.projectPath.path}`);
            }
        }

        public build(): ContentDependencyGraph {
            let dependencyGraph = new ContentDependencyGraph;
            ContentDependencyGraphBuilder.addDependencies(dependencyGraph, this.contentScript);
            return dependencyGraph;
        }

        private static addDependencies(dependencyGraph: ContentDependencyGraph, dependant: ContentFile): void {
            if (dependencyGraph.hasDependant(dependant)) {
                return;
            }
            dependencyGraph.addDependant(dependant);
            dependant.dependencies
                .forEach(
                    dependency => ContentDependencyGraphBuilder.addDependency(dependencyGraph, dependant, dependency)
                );
        }

        private static addDependency(dependencyGraph: ContentDependencyGraph, dependant: ContentFile, dependency: ContentFile): void {
            ContentDependencyGraphBuilder.addDependencies(dependencyGraph, dependency);
            dependencyGraph.addDependency(dependant, dependency);
        }
    }
}

export = ChromeExtension;
exports.ContentDependencyGraphBuilder = ChromeExtension.ContentDependencyGraphBuilder;