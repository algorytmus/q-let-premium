﻿let gulp = require('gulp');

import { TsRenderer } from './render-ts/TsRenderer';
gulp.task( 'render', () => new TsRenderer('./chrome-extension').render() );

import { BuilderSpace } from './chrome-extension/BuilderSpace';
import { Project } from './chrome-extension/Project';
import { Workspace } from './chrome-extension/Workspace';
import { BuiltPackage } from './chrome-extension/BuiltPackage';
import { BuilderPath } from './chrome-extension/BuilderPath';

let workspace = new Workspace('./');
let builderSpace = new BuilderSpace(new BuilderPath('./../build/'));
let projects = workspace.projects();
gulp.task( 'build', () => projects[0].build(builderSpace) );