import { ProjectManifest } from '../../chrome-extension/ProjectManifest';
import { ContentScript } from '../../chrome-extension/ContentScript';
import { ChromePath } from '../../chrome-extension/ChromePath';
import { Dependency } from '../../chrome-extension/Dependency';
import { WebAccessibleScript } from '../../chrome-extension/WebAccessibleScript';

exports.projectManifest = new ProjectManifest(
    {
        name: 'Test extension',
        description: 'Test extension description',
        permissions: [
            '*://google.com/'
        ],

        contentScripts : [
            new ContentScript(
                ['https://google.com/'],
                new ChromePath('content_scripts/content_script.js'))
        ],
        
        webAccessibleScripts : [
            new WebAccessibleScript(
                new ChromePath('web_accessible_scripts/web_accessible_script.js'))
        ],

        dependencies : [
            new Dependency(
                {
                    name: 'lib1',
                    jsPath: 'scripts/lib1/lib1.js',
                    tsPath: 'scripts/lib1/lib1.ts'
                }
            ),
            new Dependency(
                {
                    name: 'lib2',
                    jsPath: 'scripts/lib2/lib2.js',
                    tsPath: 'scripts/lib2/lib2.d.ts'
                }
            )
        ]
    }
);