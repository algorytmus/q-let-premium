"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ProjectManifest_1 = require("../../chrome-extension/ProjectManifest");
var ContentScript_1 = require("../../chrome-extension/ContentScript");
var ChromePath_1 = require("../../chrome-extension/ChromePath");
var Dependency_1 = require("../../chrome-extension/Dependency");
var WebAccessibleScript_1 = require("../../chrome-extension/WebAccessibleScript");
exports.projectManifest = new ProjectManifest_1.ProjectManifest({
    name: 'Test extension',
    description: 'Test extension description',
    permissions: [
        '*://google.com/'
    ],
    contentScripts: [
        new ContentScript_1.ContentScript(['https://google.com/'], new ChromePath_1.ChromePath('content_scripts/content_script.js'))
    ],
    webAccessibleScripts: [
        new WebAccessibleScript_1.WebAccessibleScript(new ChromePath_1.ChromePath('web_accessible_scripts/web_accessible_script.js'))
    ],
    dependencies: [
        new Dependency_1.Dependency({
            name: 'lib1',
            jsPath: 'scripts/lib1/lib1.js',
            tsPath: 'scripts/lib1/lib1.ts'
        }),
        new Dependency_1.Dependency({
            name: 'lib2',
            jsPath: 'scripts/lib2/lib2.js',
            tsPath: 'scripts/lib2/lib2.d.ts'
        })
    ]
});
//# sourceMappingURL=project.manifest.js.map