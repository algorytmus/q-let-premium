"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="./../../node_modules/mocha-typescript/globals.d.ts" />
var mocha_typescript_1 = require("mocha-typescript");
/// <reference path="./../../node_modules/@types/chai/index.d.ts" />
var chai_1 = require("chai");
var BuilderSpace_1 = require("./../../chrome-extension/BuilderSpace");
var Project_1 = require("./../../chrome-extension/Project");
var BuilderPath_1 = require("./../../chrome-extension/BuilderPath");
var ProjectPath_1 = require("./../../chrome-extension/ProjectPath");
var ProjectRootPath_1 = require("../../chrome-extension/ProjectRootPath");
var ContentFile_1 = require("../../chrome-extension/ContentFile");
var fs = require('fs');
var mkdirp = require('mkdirp');
var expectedManifest = "{\n\t\"manifest_version\": 2,\n\t\"version\": \"0.0.1\",\n\t\"name\": \"Test extension\",\n\t\"description\": \"Test extension description\",\n\t\"permissions\": [\n\t\t\"*://google.com/\"\n\t],\n\t\"content_scripts\": [\n\t\t{\n\t\t\t\"css\": [],\n\t\t\t\"js\": [\n\t\t\t\t\"scripts/lib1/lib1.js\",\n\t\t\t\t\"scripts/lib2/lib2.js\",\n\t\t\t\t\"internal_scripts/internal_script.js\",\n\t\t\t\t\"content_scripts/content_script.js\"\n\t\t\t],\n\t\t\t\"matches\": [\n\t\t\t\t\"https://google.com/\"\n\t\t\t]\n\t\t}\n\t],\n\t\"web_accessible_resources\": [\n\t\t\"scripts/lib1/lib1.js\",\n\t\t\"scripts/lib2/lib2.js\",\n\t\t\"internal_scripts/internal_script.js\",\n\t\t\"web_accessible_scripts/web_accessible_script.js\"\n\t],\n\t\"content_security_policy\": \"script-src 'self' 'unsafe-eval'; object-src 'self'\"\n}";
var dependantFileData = "\n//dependsOn=scripts/lib1/lib1.js\n/// <reference path=\"./../internal_scripts/internal_script.ts\" />\n";
var regexp = require('node-regexp');
var BuildTestSuite = /** @class */ (function () {
    function BuildTestSuite() {
    }
    BuildTestSuite_1 = BuildTestSuite;
    BuildTestSuite.prototype.manifestGenerated = function () {
        BuildTestSuite_1.deleteFolder('./chrome-extension-tests/build');
        mkdirp('./chrome-extension-tests/build', function (err) { });
        var builderSpace = new BuilderSpace_1.BuilderSpace(new BuilderPath_1.BuilderPath('./chrome-extension-tests/build/'));
        var project = new Project_1.Project(new ProjectRootPath_1.ProjectRootPath(new ProjectPath_1.ProjectPath('./chrome-extension-tests/test-extension/')));
        project.build(builderSpace);
        chai_1.assert.equal(expectedManifest.replace(' ', ''), fs.readFileSync('./chrome-extension-tests/build/test-extension/manifest.json', 'utf8').replace(' ', ''));
    };
    BuildTestSuite.prototype.dependencyAnalyzer = function () {
        var dependencyAnalyzer = new ContentFile_1.DependencyAnalyzer(dependantFileData);
        chai_1.assert.deepEqual(dependencyAnalyzer.dependencies, ['scripts/lib1/lib1.js', './../internal_scripts/internal_script.ts']);
    };
    BuildTestSuite.prototype.regex = function () {
        var str = '/// <reference path="./../internal_scripts/internal_script.ts" />', expr = /\/\/\/ <reference path="(.*?)" \/>/;
        var result = expr.exec(str);
        chai_1.assert.equal(result[1], './../internal_scripts/internal_script.ts');
    };
    BuildTestSuite.deleteFolder = function (path) {
        if (fs.existsSync(path)) {
            fs.readdirSync(path).forEach(function (file, index) {
                var curPath = path + '/' + file;
                if (fs.lstatSync(curPath).isDirectory()) {
                    BuildTestSuite_1.deleteFolder(curPath);
                }
                else {
                    fs.unlinkSync(curPath);
                }
            });
            fs.rmdirSync(path);
        }
    };
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], BuildTestSuite.prototype, "manifestGenerated", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], BuildTestSuite.prototype, "dependencyAnalyzer", null);
    __decorate([
        mocha_typescript_1.test,
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], BuildTestSuite.prototype, "regex", null);
    BuildTestSuite = BuildTestSuite_1 = __decorate([
        mocha_typescript_1.suite
    ], BuildTestSuite);
    return BuildTestSuite;
    var BuildTestSuite_1;
}());
//# sourceMappingURL=build.tests.js.map