/// <reference path="./../../node_modules/mocha-typescript/globals.d.ts" />
import { suite, test, slow, timeout } from 'mocha-typescript';

/// <reference path="./../../node_modules/@types/chai/index.d.ts" />
import { assert } from 'chai';

import { BuilderSpace } from './../../chrome-extension/BuilderSpace';
import { Project } from './../../chrome-extension/Project';
import { Workspace } from './../../chrome-extension/Workspace';
import { BuiltPackage } from './../../chrome-extension/BuiltPackage';
import { BuilderPath } from './../../chrome-extension/BuilderPath';
import { ProjectPath } from './../../chrome-extension/ProjectPath';
import { ProjectRootPath } from '../../chrome-extension/ProjectRootPath';
import { DependencyAnalyzer } from '../../chrome-extension/ContentFile';

let fs = require('fs');
let mkdirp = require('mkdirp');

const expectedManifest: string = `{
	"manifest_version": 2,
	"version": "0.0.1",
	"name": "Test extension",
	"description": "Test extension description",
	"permissions": [
		"*://google.com/"
	],
	"content_scripts": [
		{
			"css": [],
			"js": [
				"scripts/lib1/lib1.js",
				"scripts/lib2/lib2.js",
				"internal_scripts/internal_script.js",
				"content_scripts/content_script.js"
			],
			"matches": [
				"https://google.com/"
			]
		}
	],
	"web_accessible_resources": [
		"scripts/lib1/lib1.js",
		"scripts/lib2/lib2.js",
		"internal_scripts/internal_script.js",
		"web_accessible_scripts/web_accessible_script.js"
	],
	"content_security_policy": "script-src 'self' 'unsafe-eval'; object-src 'self'"
}`;

const dependantFileData: string = `
//dependsOn=scripts/lib1/lib1.js
/// <reference path="./../internal_scripts/internal_script.ts" />
`;

let regexp = require('node-regexp');

@suite
class BuildTestSuite {
    @test manifestGenerated() {
        BuildTestSuite.deleteFolder('./chrome-extension-tests/build');
        mkdirp('./chrome-extension-tests/build', function(err) {});
        let builderSpace = new BuilderSpace(new BuilderPath('./chrome-extension-tests/build/'));
        let project = new Project(new ProjectRootPath(new ProjectPath('./chrome-extension-tests/test-extension/')));
        project.build(builderSpace);
        assert.equal(expectedManifest.replace(' ', ''), fs.readFileSync('./chrome-extension-tests/build/test-extension/manifest.json', 'utf8').replace(' ', ''));
    }

    @test dependencyAnalyzer() {
        let dependencyAnalyzer = new DependencyAnalyzer(dependantFileData);
        assert.deepEqual(dependencyAnalyzer.dependencies, ['scripts/lib1/lib1.js', './../internal_scripts/internal_script.ts']);
    }

    @test regex() {
            let str = '/// <reference path="./../internal_scripts/internal_script.ts" />',
            expr = /\/\/\/ <reference path="(.*?)" \/>/;
            let result = expr.exec(str);
        assert.equal(result[1], './../internal_scripts/internal_script.ts');
    }

    private static deleteFolder(path: String): void {
        if (fs.existsSync(path)) {
            fs.readdirSync(path).forEach(function(file, index) {
            let curPath = path + '/' + file;
            if (fs.lstatSync(curPath).isDirectory()) { // recurse
                BuildTestSuite.deleteFolder(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
            });
            fs.rmdirSync(path);
        }
    }
}