/// <reference path="./../../node_modules/@types/knockout/index.d.ts" />
/// <reference path="./../scripts/reactive-knockout/2-reactive-knockout.d.ts" />

//dependsOn=scripts/knockout/knockout.js
//dependsOn=scripts/reactive-knockout/1-reactive-knockout.js

interface IKnockout {
    observable<T>(): KnockoutObservable<T>;
    toKnockoutComputed<T>(observable: Rx.Observable<T>): KnockoutComputed<T>;
}