/// <reference path="./../../node_modules/@types/knockout/index.d.ts" />
/// <reference path="./../scripts/reactive-knockout/2-reactive-knockout.d.ts" />
/// <reference path="./IKnockout.ts" />

//dependsOn=scripts/knockout/knockout.js
//dependsOn=scripts/reactive-knockout/1-reactive-knockout.js

class Knockout implements IKnockout {
    observable<T>(): KnockoutObservable<T>{
        return ko.observable<T>();
    }
    toKnockoutComputed<T>(observable: Rx.Observable<T>): KnockoutComputed<T>{
        return observable.toKnockoutComputed();
    }
}