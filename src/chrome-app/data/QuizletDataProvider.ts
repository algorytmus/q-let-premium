﻿import { Observer, Subject, Observable } from './../scripts/rxjs/rx.all';

import * as Rx from './../scripts/rxjs/rx.all';

/// <reference path="./../scripts/jquery/jquery.d.ts" />
/// <reference path="./../scripts/rxjs/ts/rx.all.d.ts" />
/// <reference path="./../scripts/rxjs-jquery/ts/rx.jquery.d.ts" />
/// <reference path="./../scripts/rx-dom/rx-dom.d.ts" />
//dependsOn=data/Quizlet.js
/// <reference path="./Quizlet.ts" />
/// <reference path="../Messaging/IMessageBus.ts" />

/// <reference path="./../messaging/IMessageBus.ts" />
import { IMessageBus } from './../messaging/IMessageBus';

/// <reference path="./../messaging/TypeIdentity.ts" />
import { TypeIdentity } from './../messaging/TypeIdentity';

/// <reference path="./WritePageQuizletDataMessage.ts" />
import { WritePageQuizletDataMessage } from './WritePageQuizletDataMessage';

export class QuizletDataProvider implements IQuizletDataProvider {
    private observableQuizlet: Observable<Quizlet>;

    constructor(private readonly messageBus: IMessageBus) {
        this.observableQuizlet = this.messageBus
            .observe<WritePageQuizletDataMessage>(TypeIdentity.createIdentity(WritePageQuizletDataMessage.prototype))
            .map(d => d.data);
    }

    public get Quizlet(): Observable<Quizlet> {
        return this.observableQuizlet;
    }
}