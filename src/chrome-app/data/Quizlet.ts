﻿/// <reference path="./Exercise.ts" />

export enum PromptOrder {
    WordThenDefinition = "1-2",
    DefinitionThenWord = "2-1"
}

export interface Quizlet {
    learnGameData:
        {
            allTerms: Exercise[],
            audioOn: boolean,
            autofocus: boolean,
            canEditSome: boolean,
            correctCount: number,
            definitionAccents: string[],
            definitionSideLabel: string,
            hardLangs: string[],
            incorrect: Exercise[],
            isEmbedding: boolean,
            promptOrder: PromptOrder,
            remaining: Exercise[],
            round: number,
            roundTotal: number,
            selectedOnly: boolean,
            selectedOnlyStartOverPath: string,
            session: number,
            sessionTermIds: string[],
            sessionTypeConstant: number,
            startOverPath: string,
            studyableId: number,
            studyableType: number,
            thirdSideAccents: string[],
            thirdSideLabel: string,
            wordAccents: string[],
            wordSideLabel: string
        };
}