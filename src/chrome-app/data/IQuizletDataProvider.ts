﻿/// <reference path="./Quizlet.ts" />

interface IQuizletDataProvider {
    Quizlet: Rx.Observable<Quizlet>;
}