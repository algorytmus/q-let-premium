﻿
interface Exercise {
    id: number;
    photo: string;
    word: string;
    definition: string;
    quiz_id: number;
    term_lang: string;
    def_lang: string;
}