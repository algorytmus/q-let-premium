import { Observer, Subject, Observable } from './../scripts/rxjs/rx.all';

/// <reference path="./../scripts/jquery/jquery.d.ts" />
/// <reference path="./../scripts/rxjs/ts/rx.all.d.ts" />

/// <reference path="./TypeIdentity.ts" />
import { TypeIdentityUntyped, TypeIdentity, TypeIdentityUntypedSerialized } from './TypeIdentity';

/// <reference path="./IRawMessageBus.ts" />
import { IRawMessageBus } from './IRawMessageBus';

/// <reference path="./IMessageWindow.ts" />
import { IMessageWindow } from './IMessageWindow';

type RawMessageUnchecked = {identity: TypeIdentityUntypedSerialized | undefined, data: any | undefined};
type RawMessageChecked = {identity: TypeIdentityUntyped, data: any};

export class RawMessageBus implements IRawMessageBus {
    constructor(private window: IMessageWindow) {}

    public sendData<T>(data: T, identity: TypeIdentity<T>): void {
        this.sendMessage({identity: identity.serialize(), data: data});
    }

    private sendMessage(rawMessage: RawMessageUnchecked): void {
        this.window.send(rawMessage);
    }

    public observeData<T>(identity: TypeIdentity<T>): Observable<T> {
        return this.window.eventStream
            .where(rm => rm != undefined)
            .where(rm => rm.identity != undefined)
            .map(RawMessageBus.check)
            .where(rm => rm != undefined)
            .where(rm => rm.identity != undefined)
            .filter(rm => rm.identity.isSameAs(identity))
            .map(rm => <T>rm.data);
    }

    private static check(rawMessage: RawMessageUnchecked): RawMessageChecked | undefined {
        if (rawMessage.identity && rawMessage.data) {
            return {identity: TypeIdentityUntyped.deserialize(rawMessage.identity), data: rawMessage.data};
        }
        return undefined;
    }
}