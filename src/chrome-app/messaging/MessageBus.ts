import { Observer, Subject, Observable } from './../scripts/rxjs/rx.all';

/// <reference path="./../scripts/jquery/jquery.d.ts" />
/// <reference path="./../scripts/rxjs/ts/rx.all.d.ts" />

/// <reference path="./ClientId.ts" />
import { ClientId } from './ClientId';

/// <reference path="./IMessageBus.ts" />
import { IMessageBus } from './IMessageBus';

/// <reference path="./RawSubscribtion.ts" />
import { RawSubscribtion } from './RawSubscribtion';

/// <reference path="./RawResponse.ts" />
import { RawResponse } from './RawResponse';

/// <reference path="./TypeIdentity.ts" />
import { TypeIdentity } from './TypeIdentity';

/// <reference path="./IRawMessageBus.ts" />
import { IRawMessageBus } from './IRawMessageBus';

export class MessageBus implements IMessageBus {
    private clientId: ClientId = new ClientId();

    constructor(private rawMessageBus: IRawMessageBus) {}

    public observeRequests<T>(identity: TypeIdentity<T>): Observable<Observer<T>> {
        let clientId = this.clientId;
        return this.rawMessageBus
            .observeData<RawSubscribtion<T>>(MessageBus.GetRawSubscribtionIdentity(identity))
            .groupBy(s => s.clientId.id)
            .map<Observer<T>>(_ => {
                let subject = new Subject<T>();
                subject.do( m => {
                    this.rawMessageBus
                        .sendData<RawResponse<T>>(new RawResponse(m, clientId), MessageBus.GetRawResponseIdentity(identity)); }
                    ).subscribe();
                    return subject;
                }
            );
    }

    private static GetRawSubscribtionIdentity<T>(identity: TypeIdentity<T>): TypeIdentity<RawSubscribtion<T>> {
        return TypeIdentity.createIdentityComplex<RawSubscribtion<T>, T>(RawSubscribtion.prototype, identity);
    }

    private static GetRawResponseIdentity<T>(identity: TypeIdentity<T>): TypeIdentity<RawResponse<T>> {
        return TypeIdentity.createIdentityComplex<RawResponse<T>, T>(RawResponse.prototype, identity);
    }

    public observe<T>(identity: TypeIdentity<T>): Observable<T> {
        let set = false;
        let rawMessageBus = this.rawMessageBus;
        let clientId = this.clientId;
        let dataArrivalEvent: Observable<T> = rawMessageBus.observeData<RawResponse<T>>(MessageBus.GetRawResponseIdentity(identity))
            .do(_ => set = true)
            .map(rawResponse => rawResponse.response);

        let subscriptionData = new RawSubscribtion<T>(clientId);

        let dataRequestPushes = Observable.interval(50)
            .takeWhile(() => !set)
            .do(_ => rawMessageBus.sendData<RawSubscribtion<T>>(subscriptionData, MessageBus.GetRawSubscribtionIdentity(identity)))
            .startWith(_ => rawMessageBus.sendData<RawSubscribtion<T>>(subscriptionData, MessageBus.GetRawSubscribtionIdentity(identity)));

        return dataArrivalEvent.merge(dataRequestPushes.where(_ => false));
    }
}