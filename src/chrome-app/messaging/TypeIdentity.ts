/// <reference path="./Export.ts" />

interface IFunction {
    name: string;
}

export class TypeIdentityUntyped {
    protected constructor(public identity: string) {}

    public isSameAs(identity: TypeIdentityUntyped): boolean {
        return this.identity === identity.identity;
    }

    public serialize(): TypeIdentityUntypedSerialized {
        return new TypeIdentityUntypedSerialized(this.identity);
    }

    public static deserialize(data: TypeIdentityUntypedSerialized): TypeIdentityUntyped {
        return new TypeIdentityUntyped(data.identity);
    }
}

export class TypeIdentityUntypedSerialized {
    constructor(public readonly identity: string) {}
}

export class TypeIdentity<T> extends TypeIdentityUntyped {
    private constructor(identity: string) {
        super(identity);
    }

    public static createIdentity<T>(prototype: T): TypeIdentity<T> {
        /*if (!TypeIdentity.canBeCloned(prototype)) {
            throw TypeError('Cannot create TypeIdentity because the type is not serializable.');
        }*/
        return TypeIdentity.createIdentityFromConstructor<T>(<any>prototype.constructor);
    }

    private static createIdentityFromConstructor<T>(constructor: IFunction): TypeIdentity<T> {

        return new TypeIdentity<T>(constructor.name);
    }

    public static createIdentityComplex<Touter, Tinner>(outerPrototype: Touter, innerPrototype: TypeIdentity<Tinner>): TypeIdentity<Touter> {
        return new TypeIdentity<Touter>(`${TypeIdentity.createIdentity(outerPrototype).identity}<${innerPrototype.identity}>`);
    }

    private static canBeCloned<T>(prototype: any): boolean {
        if (Object(prototype) !== prototype) // Primitive prototypeue
          return true;
        switch ({}.toString.call(prototype).slice(8, -1)) { // Class
          case 'Boolean':     case 'Number':      case 'String':      case 'Date':
          case 'RegExp':      case 'Blob':        case 'FileList':
          case 'ImageData':   case 'ImageBitmap': case 'ArrayBuffer':
            return true;
          case 'Array':       case 'Object':
            return Object.keys(prototype).every(prop => TypeIdentity.canBeCloned(prototype[prop]));
          case 'Map':
            return [...prototype.keys()].every(TypeIdentity.canBeCloned)
                && [...prototype.prototypeues()].every(TypeIdentity.canBeCloned);
          case 'Set':
            return [...prototype.keys()].every(TypeIdentity.canBeCloned);
          default:
            return false;
        }
    }
}