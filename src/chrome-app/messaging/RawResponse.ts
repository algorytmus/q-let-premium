/// <reference path="./ClientId.ts" />
import { ClientId } from './ClientId';

export class RawResponse<T> {
    constructor(
        public response: T,
        public clientId: ClientId) {
    }
}