/// <reference path="./TypeIdentity.ts" />
import { TypeIdentity, TypeIdentityUntypedSerialized } from './TypeIdentity';

type RawMessageUnchecked = {identity: TypeIdentityUntypedSerialized | undefined, data: any | undefined};

export interface IMessageWindow {
    send(data: RawMessageUnchecked): void;
    eventStream: Rx.Observable<RawMessageUnchecked>;
}