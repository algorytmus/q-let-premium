import { Observer, Subject, Observable } from './../scripts/rxjs/rx.all';

/// <reference path="./../scripts/jquery/jquery.d.ts" />
/// <reference path="./../scripts/rxjs/ts/rx.all.d.ts" />

/// <reference path="./TypeIdentity.ts" />
import { TypeIdentity, TypeIdentityUntypedSerialized } from './TypeIdentity';

/// <reference path="./IMessageWindow.ts" />
import { IMessageWindow } from './IMessageWindow';

type RawMessageUnchecked = {identity: TypeIdentityUntypedSerialized | undefined, data: any | undefined};

export class MessageWindow implements IMessageWindow {

    constructor(private window: IWindow) {}

    send<T>(data: RawMessageUnchecked): void {
        this.window.postMessage(data, '*');
    }

    public get eventStream(): Observable<RawMessageUnchecked> {
        return Observable
            .fromEvent<MessageEvent>(this.window, 'message')
            .map(MessageWindow.toRawMessageUnchecked);
    }

    private static toRawMessageUnchecked(rawMessage: MessageEvent): RawMessageUnchecked {
        return {identity: rawMessage.data.identity, data: rawMessage.data.data};
    }
}