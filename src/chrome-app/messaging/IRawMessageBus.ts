/// <reference path="./TypeIdentity.ts" />
import { TypeIdentity } from './TypeIdentity';

export interface IRawMessageBus {
    sendData<T>(data: T, identity: TypeIdentity<T>): void;
    observeData<T>(identity: TypeIdentity<T>): Rx.Observable<T>;
}