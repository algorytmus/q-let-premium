/// <reference path="./Export.ts" />

/// <reference path="./../scripts/rxjs/ts/rx.all.d.ts" />

/// <reference path="./TypeIdentity.ts" />
import { TypeIdentity } from './TypeIdentity';

export interface IMessageBus {
    observeRequests<T>(identity: TypeIdentity<T>): Rx.Observable<Rx.Observer<T>>;
    observe<T>(identity: TypeIdentity<T>): Rx.Observable<T>;
}