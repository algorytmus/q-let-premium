/// <reference path="./ClientId.ts" />
import { ClientId } from './ClientId';

export class RawSubscribtion<T> {
    constructor(public clientId: ClientId) {
    }
}