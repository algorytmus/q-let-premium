import { ContentDependencyGraphBuilder } from '../../../chrome-extension/ContentDependencyGraphBuilder';
import { ContentFile } from '../../../chrome-extension/ContentFile';
import { Project } from '../../../chrome-extension/Project';
import { ProjectRootPath } from '../../../chrome-extension/ProjectRootPath';
import { ProjectPath } from '../../../chrome-extension/ProjectPath';
import { ChromePath } from '../../../chrome-extension/ChromePath';


function render()
{
    return 'const writePageQuizletDataProvider_init_dependencies = exports.writePageQuizletDataProvider_init_dependencies = [' + new ContentDependencyGraphBuilder(
        new ContentFile(
            new ChromePath('/views/write/WritePageQuizletDataProvider.init.js'),
            new Project(new ProjectRootPath(new ProjectPath('./chrome-app/')) )
          )
      )
      .build()
      .overallOrder('')
      .map(chp =>  "'"+chp.chromeValidPath+"'") + ']';
      
}

exports.render = render;