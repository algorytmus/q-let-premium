
//dependsOn=views/write/WritePageQuizletDataProvider.js
//dependsOn=messaging/Export.js

/// <reference path="./../../messaging/RawMessageBus.ts" />
import { RawMessageBus } from './../../messaging/RawMessageBus';

/// <reference path="./../../messaging/MessageBus.ts" />
import { MessageBus } from './../../messaging/MessageBus';

/// <reference path="./../../messaging/IMessageBus.ts" />
import { IMessageBus } from './../../messaging/IMessageBus';

/// <reference path="./../../messaging/TypeIdentity.ts" />
import { TypeIdentity } from './../../messaging/TypeIdentity';

/// <reference path="./../../data/WritePageQuizletDataMessage.ts" />
import { WritePageQuizletDataMessage } from './../../data/WritePageQuizletDataMessage';

/// <reference path="./WritePageQuizletDataProvider.ts" />
import { WritePageQuizletDataProvider } from './WritePageQuizletDataProvider';

/// <reference path="./../../messaging/MessageWindow.ts" />
import { MessageWindow } from './../../messaging/MessageWindow';

/// <reference path="./../../injections/Dom.ts" />
import { Dom } from '../../injections/Dom';

const writePageQuizletDataProvider: WritePageQuizletDataProvider = new WritePageQuizletDataProvider(new MessageBus(new RawMessageBus(new MessageWindow(window))), window, new Dom(100));
writePageQuizletDataProvider.attach()