﻿
/// <reference path="./../../injections/InnerHTMLInjector.ts" />
/// <reference path="./../../injections/Url.ts" />

/// <reference path="./../../scripts/jquery/jquery.d.ts" />
/// <reference path="./../../scripts/rx-dom/rx-dom.d.ts" />

/// <reference path="./WritePageQuizletDataProvider.as-script.ts" />

//dependsOn=messaging/Export.js
//dependsOn=injections/Url.js
//dependsOn=injections/ScriptInjector.js
//dependsOn=injections/InnerHTMLInjector.js
//dependsOn=injections/BindingInjector.js
//dependsOn=injections/AttributeInjector.js
//dependsOn=injections/ElementAppender.js
//dependsOn=views/bindings/SelfBinding.js
//dependsOn=scripts/jquery/jquery.js
//dependsOn=scripts/rxjs/rx.all.js
//dependsOn=scripts/rx-dom/rx.dom.js
//dependsOn=scripts/rxjs-jquery/rx.jquery.js
//dependsOn=views/write/write_styles.css
//dependsOn=view_models/write/WriteViewModel.js
//dependsOn=views/bindings/ViewModelResolver.js
//dependsOn=views/write/WritePageQuizletDataProvider.init.js
//dependsOn=views/write/WritePageQuizletDataProvider.as-script.js


const dom = new Dom(10);

new ScriptInjector(
    dom,
    writePageQuizletDataProvider_init_dependencies.map(f => new Url(chrome.runtime.getURL(f))),
    'body'
).attach();

new AttributeInjector('body', 'data-bind', 'resolveViewModel: WriteViewModel', dom)
    .attach();

new ElementAppender(
    'div.UITextarea-content',
    '#user-hint', 
    () => $('#user-answer').length > 0 || $('#js-learnModeCopyInput').length > 0,
    '<span id="user-hint"></span>', dom)
    .attach();

new AttributeInjector(
    'body',
    'self-binding',
    '',
    dom
).attach();

new BindingInjector(
    '#user-answer',
    kbc => kbc.extend(
        { 
            value: kbc.$data.userAnswer, 
            valueUpdate: 'input',
            onInitialized: kbc.$data,
            externalEvent: { 
                thisValue: kbc.$data.userAnswer,
                element: 'button.UISpecialCharacterButton',
                event: 'click'
            }
        }),
    dom
).attach();

new BindingInjector(
    'span.TermText',
    kbc => kbc.extend(
        { 
            viewImmutableText: kbc.$data.term
        }),
    dom
).attach();

new BindingInjector(
    '#js-learnModeCopyInput',
    kbc => kbc.extend(
        { 
            value: kbc.$data.userAnswer,
            valueUpdate: 'input',
            externalEvent: { 
                thisValue: kbc.$data.userAnswer,
                element: 'button.UISpecialCharacterButton',
                event: 'click'
            }
        }),
    dom
).attach();

new BindingInjector(
    '#user-hint',
    kbc => kbc.extend({ text: kbc.$data.hint }),
    dom
).attach();

dom.observeInitialized()
    .filter(v => $('#user-hint').height() != undefined )
    .filter(v => $('#user-answer').height() != undefined )
    .do(v => $('#user-hint').width($('#user-answer').width()) )
    .do(v => $('#user-answer').attr('style','min-height:'+ ($('#user-hint').height() + 20).toString() + 'px;') )
    .subscribe();

dom.observeInitialized()
    .filter(v => $('#user-hint').height() != undefined )
    .filter(v => $('#js-learnModeCopyInput').height() != undefined )
    .do(v => $('#user-hint').width($('#js-learnModeCopyInput').width()) )
    .do(v => $('#js-learnModeCopyInput').attr('style','min-height:'+ ($('#user-hint').height() + 20).toString() + 'px;') )
    .subscribe();