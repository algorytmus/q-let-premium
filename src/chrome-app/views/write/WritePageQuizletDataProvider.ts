import { Observer, Subject, Observable } from './../../scripts/rxjs/rx.all';

/// <reference path="./../../scripts/jquery/jquery.d.ts" />
/// <reference path="./../../scripts/rxjs/ts/rx.all.d.ts" />
/// <reference path="./../../scripts/rxjs-jquery/ts/rx.jquery.d.ts" />

/// <reference path="./../../injections/InnerHTMLInjector.ts" />
/// <reference path="./../../injections/Url.ts" />
/// <reference path="./../../index.d.ts" />
/// <reference path="./../../../node_modules/@types/chrome/index.d.ts" />
/// <reference path="./../../scripts/jquery/jquery.d.ts" />
/// <reference path="./../../scripts/rx-dom/rx-dom.d.ts" />

//dependsOn=injections/Url.js
//dependsOn=injections/ScriptInjector.js
//dependsOn=injections/InnerHTMLInjector.js
//dependsOn=injections/BindingInjector.js
//dependsOn=injections/AttributeInjector.js
//dependsOn=injections/Dom.js
//dependsOn=views/bindings/SelfBinding.js
//dependsOn=scripts/rxjs/rx.all.js

/// <reference path="./../../messaging/IMessageBus.ts" />
import { IMessageBus } from './../../messaging/IMessageBus';

/// <reference path="./../../messaging/TypeIdentity.ts" />
import { TypeIdentity } from './../../messaging/TypeIdentity';

/// <reference path="./../../data/WritePageQuizletDataMessage.ts" />
import { WritePageQuizletDataMessage } from './../../data/WritePageQuizletDataMessage';

export class WritePageQuizletDataProvider {
    constructor(
        private messageBus: IMessageBus,
        private window: IWebAccessibleWindow,
        private dom: IDom) {}

    public attach(): any {
        let instance = this;
        return this.messageBus
            .observeRequests(TypeIdentity.createIdentity(WritePageQuizletDataMessage.prototype))
            .select(observer => instance.dom.observeInitialized()
                .where(_ => instance.window.Quizlet != undefined)
                .where(_ => instance.window.Quizlet.learnGameData != undefined)
                .do(_ => observer.onNext(new WritePageQuizletDataMessage( {learnGameData: instance.window.Quizlet.learnGameData })))
            )
            .switch()
            .subscribe();
    }
}