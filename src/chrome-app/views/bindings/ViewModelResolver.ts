﻿/// <reference path="./../../messaging/MessageWindow.ts" />
import { MessageWindow } from './../../messaging/MessageWindow';

/// <reference path="./../../../node_modules/@types/knockout/index.d.ts" />

/// <reference path="./../../../node_modules/@types/knockout/index.d.ts" />
/// <reference path="./../../scripts/reactive-knockout/2-reactive-knockout.d.ts" />

//dependsOn=scripts/knockout/knockout.js
//dependsOn=scripts/reactive-knockout/1-reactive-knockout.js

//dependsOn=view_models/write/WriteViewModel.js
//dependsOn=data/QuizletDataProvider.js
//dependsOn=scripts/knockout/knockout.js

/// <reference path="./../../messaging/RawMessageBus.ts" />
import { RawMessageBus } from './../../messaging/RawMessageBus';

/// <reference path="./../../messaging/MessageBus.ts" />
import { MessageBus } from './../../messaging/MessageBus';

/// <reference path="./../../data/QuizletDataProvider.ts" />
import { QuizletDataProvider } from './../../data/QuizletDataProvider';

/// <reference path="./../../injections/Dom.ts" />
import { Dom } from './../../injections/Dom';

/// <reference path="./../../view_models/write/WriteViewModel.ts"/>
import { WriteViewModel, HintProvider, SpecificHintProviderCollection, StarsInsteadOfLettersHintProvider, FirstLetterThenStarsInsteadOfLettersHintProvider, FirstLettersOfWordsThenEveryOtherLetterHintProvider, FirstLettersOfWordsThenStarsInsteadOfLettersHintProvider } from './../../view_models/write/WriteViewModel';

class ViewModelResolver implements KnockoutBindingHandler {
    public init(element: any,
            valueAccessor: () => any,
            allBindingsAccessor?: KnockoutAllBindingsAccessor,
            viewModel?: any,
            bindingContext?: KnockoutBindingContext): void {
        let type = valueAccessor();
        let mw = new MessageWindow(window);
        let dom = new Dom(200);
        let hintProvider: HintProvider = new HintProvider(new SpecificHintProviderCollection(
            {
                StarsInsteadOfLetters: new StarsInsteadOfLettersHintProvider(),
                FirstLetterThenStarsInsteadOfLetters: new FirstLetterThenStarsInsteadOfLettersHintProvider(),
                FirstLettersOfWordsThenEveryOtherLetter: new FirstLettersOfWordsThenEveryOtherLetterHintProvider(),
                FirstLettersOfWordsThenStarsInsteadOfLetters: new FirstLettersOfWordsThenStarsInsteadOfLettersHintProvider()
            }
        ));
        let vm = new WriteViewModel(new QuizletDataProvider(new MessageBus(new RawMessageBus(mw)) ), hintProvider);
        if (bindingContext) {
            bindingContext.$data = vm;
        }
    }
}

ko.bindingHandlers.resolveViewModel = new ViewModelResolver();

var vm : WriteViewModel = undefined;

ko.bindingHandlers.onInitialized = {
    init: function (element, valueAccessor, allBindings, viewModel) {
        var callback: WriteViewModel = valueAccessor();
        console.log('calling callback');
        console.log(callback);
        if(callback != undefined)
        {
            console.log('callback called 1');
            vm = callback;
            callback.nextExcercise({});//.call(viewModel);
            console.log('callback called 2');
        }
    }
};

ko.bindingHandlers.externalEvent = {
    init: function (element, valueAccessor, allBindings, viewModel) {
        var property: {thisValue: any, element: string, event: string} = valueAccessor();
        $(property.element).click(function () {
            setTimeout( () => property.thisValue($(element)[0].value), 10 );
            return true;
        });
    }
};

ko.bindingHandlers.viewImmutableText = {
    init: function (element, valueAccessor, allBindings, viewModel) {
        var property: ko.observable<string> = valueAccessor();
        property($("span.TermText")[0].outerText);
        $("span.TermText").on('DOMSubtreeModified', function () {
            property($("span.TermText")[0].outerText);
          });
    }
};