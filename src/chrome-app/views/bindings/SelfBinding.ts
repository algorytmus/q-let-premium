import { Observer, Subject, Observable, IDisposable } from './../scripts/rxjs/rx.all';


/// <reference path="./../../scripts/jquery/jquery.d.ts" />
/// <reference path="./../../scripts/rxjs/ts/rx.all.d.ts" />
/// <reference path="./../../scripts/rxjs-jquery/ts/rx.jquery.d.ts" />
/// <reference path="./../../scripts/rx-dom/rx-dom.d.ts" />
/// <reference path="./../../../node_modules/@types/knockout/index.d.ts" />


//dependsOn=scripts/jquery/jquery.js
//dependsOn=scripts/rxjs/rx.all.js
//dependsOn=scripts/rxjs-jquery/rx.jquery.js
//dependsOn=scripts/rx-dom/rx.dom.js
//dependsOn=injections/Url.js
//dependsOn=scripts/knockout/knockout.js
//dependsOn=messaging/Export.js

class SelfBinding {
    private static SelfBindingAttribut = 'self-binding';

    public attach(): IDisposable {
        let observable = this.observeInitialized()
            .merge(this.observeDOMSubtreeModified())
            .debounce(200);

        return observable
            .subscribe(v => SelfBinding.apply());
    }

    private observeInitialized(): Observable<string> {
        return Observable.start(() => '');
    }

    private observeDOMSubtreeModified(): Observable<string> {
        return $(document)
            .onAsObservable('DOMSubtreeModified')
            .debounce(200)
            .map(f => '');
    }

    private static selectSelfBindingNodes(): JQuery {
        return $( '[' + SelfBinding.SelfBindingAttribut + ']' );
    }

    private static apply(): void {
        SelfBinding.selectSelfBindingNodes().each(function( index: number, elem: Element ) {
                if (!ko.dataFor(elem)) {
                    try{
                        ko.applyBindings();
                    }catch(e)
                    {
                        console.log(e);
                    }
                }
            }
        );
    }
}

new SelfBinding().attach();