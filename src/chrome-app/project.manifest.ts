import { ProjectManifest } from './../chrome-extension/ProjectManifest';
import { ContentScript } from '../chrome-extension/ContentScript';
import { ChromePath } from '../chrome-extension/ChromePath';
import { Dependency } from '../chrome-extension/Dependency';
import { WebAccessibleScript } from '../chrome-extension/WebAccessibleScript';

exports.projectManifest = new ProjectManifest(
    {
        name: 'Qlet Extras',
        description: 'Extra tools for Quizlet',
        permissions: [
            '*://quizlet.com/'
        ],

        contentScripts : [
            new ContentScript(
                ['https://quizlet.com/*/write'],
                new ChromePath('views/write/write_view.html.js'))
        ],

        webAccessibleScripts : [
            new WebAccessibleScript(
                new ChromePath('views/write/WritePageQuizletDataProvider.init.js'))
        ],

        dependencies : [
            new Dependency(
                {
                    name: 'jquery',
                    jsPath: 'scripts/jquery/jquery.js',
                    tsPath: 'scripts/jquery/jquery.d.ts'
                }
            ),
            new Dependency(
                {
                    name: 'knockout',
                    jsPath: 'scripts/knockout/knockout.js',
                    tsPath: './../node_modules/@types/knockout/index.d.ts'
                }
            ),
            new Dependency(
                {
                    name: 'reactive-knockout',
                    jsPath: 'scripts/reactive-knockout/1-reactive-knockout.js',
                    tsPath: 'scripts/reactive-knockout/2-reactive-knockout.d.ts'
                }
            ),
            new Dependency(
                {
                    name: 'rx-dom',
                    jsPath: 'scripts/rx-dom/rx.dom.js',
                    tsPath: 'scripts/rx-dom/rx-dom.d.ts'
                }
            ),
            new Dependency(
                {
                    name: 'rxjs',
                    jsPath: 'scripts/rxjs/rx.all.js',
                    tsPath: 'scripts/rxjs/ts/rx.all.d.ts'
                }
            ),
            new Dependency(
                {
                    name: 'rx-jquery',
                    jsPath: 'scripts/rxjs-jquery/rx.jquery.js',
                    tsPath: 'scripts/rxjs-jquery/ts/rx.jquery.d.ts'
                }
            ),
            new Dependency(
                {
                    name: 'typefac',
                    jsPath: 'scripts/typefac/Typefac.js',
                    tsPath: 'scripts/typefac/Typefac.d.ts'
                }
            )
        ]
    }
);