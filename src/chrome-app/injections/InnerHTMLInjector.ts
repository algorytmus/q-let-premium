﻿import { Dom } from './Dom';
/// <reference path="./../scripts/jquery/jquery.d.ts" />
/// <reference path="./../scripts/rxjs/ts/rx.all.d.ts" />
/// <reference path="./../scripts/rxjs-jquery/ts/rx.jquery.d.ts" />
/// <reference path="./../scripts/rx-dom/rx-dom.d.ts" />
/// <reference path="./Url.ts" />

//dependsOn=scripts/jquery/jquery.js
//dependsOn=scripts/rxjs/rx.all.js
//dependsOn=scripts/rxjs-jquery/rx.jquery.js
//dependsOn=scripts/rx-dom/rx.dom.js
//dependsOn=injections/Url.js
//dependsOn=injections/Dom.js

class InnerHTMLInjector{

    constructor(private readonly parentName: string,
         private readonly htmlUrl: Url,
         private readonly dom: IDom)
    {}

    public attach(): Rx.IDisposable {
        const injector = this;
        return this.dom.observeInitializedAndChanges()
            .subscribe(() => InnerHTMLInjector.load(injector));
    }

    private static load(injector: InnerHTMLInjector): void
    {
        $.get(injector.htmlUrl.url, (d) => InnerHTMLInjector.inject(injector, d));
    }

    private static inject(injector: InnerHTMLInjector, data: any): void
    {
        if (data == undefined)
        {
            throw new URIError('Data to inject not found.');
        }

        if(!InnerHTMLInjector.hasInjectionAppliedFlag(injector.parentName))
        {
            $(injector.parentName).html(data);
            InnerHTMLInjector.setInjectionAppliedFlag(injector.parentName);
        }
    }

    private static hasInjectionAppliedFlag(parentName: String): boolean
    {
        return $(parentName).attr('injection-applied') !== undefined;
    }

    private static setInjectionAppliedFlag(parentName: String): void
    {
        $(parentName).attr('injection-applied', '');
    }
}