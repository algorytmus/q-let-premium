/// <reference path="./../scripts/jquery/jquery.d.ts" />
/// <reference path="./../scripts/rxjs/ts/rx.all.d.ts" />
/// <reference path="./../scripts/rxjs-jquery/ts/rx.jquery.d.ts" />
/// <reference path="./../scripts/rx-dom/rx-dom.d.ts" />
/// <reference path="./Url.ts" />
/// <reference path="./../messaging/Export.ts" />

//dependsOn=messaging/Export.js
//dependsOn=scripts/jquery/jquery.js
//dependsOn=scripts/rxjs/rx.all.js
//dependsOn=scripts/rxjs-jquery/rx.jquery.js
//dependsOn=scripts/rx-dom/rx.dom.js
//dependsOn=injections/Url.js

export class Dom implements IDom {

    constructor(private readonly refreshRate: number) {}

    public observeInitialized(): Rx.Observable<any> {
        return Dom.observeLoad()
            .startWith('')
            .throttle(this.refreshRate);
    }

    public observeChanged(): Rx.Observable<any> {
        return Dom.observeDOMSubtreeModified()
            .throttle(this.refreshRate);
    }

    public observeInitializedAndChanges()
    {
        return this.observeInitialized()
            .merge(this.observeChanged());
    }

    private static observeLoad(): Rx.Observable<string> {
        return $(document)
            .onAsObservable('load');
    }

    private static observeDOMSubtreeModified(): Rx.Observable<string> {
        return $(document)
            .onAsObservable('DOMSubtreeModified');
    }

    $(selector: any): JQuery {
        return jQuery(selector);
    }
}