﻿/// <reference path="./../scripts/jquery/jquery.d.ts" />
/// <reference path="./../scripts/rxjs/ts/rx.all.d.ts" />
/// <reference path="./../scripts/rxjs-jquery/ts/rx.jquery.d.ts" />
/// <reference path="./../scripts/rx-dom/rx-dom.d.ts" />
/// <reference path="./Url.ts" />

//dependsOn=scripts/jquery/jquery.js
//dependsOn=scripts/rxjs/rx.all.js
//dependsOn=scripts/rxjs-jquery/rx.jquery.js
//dependsOn=scripts/rx-dom/rx.dom.js
//dependsOn=injections/Url.js

class BindingInjector {

    constructor(
        private readonly elementSelector: string,
        private readonly on$dataAvailable: (kbc: KnockoutBindingContext ) => KnockoutBindingContext,
        private readonly dom: IDom) {}

    public attach(): Rx.IDisposable {
        const injector = this;
        return this.dom
            .observeInitializedAndChanges()
            .subscribe(v => BindingInjector.update(injector));
    }

    private static update(injector: BindingInjector): void {
        $(injector.elementSelector)
            .each( function(index: number, elem: Element) {
                BindingInjector.inject(injector, elem);
            });
    }

    private static inject(injector: BindingInjector, node: Node): void {
        if (ko.dataFor(node)) {
            ko.applyBindingsToNode(node, injector.on$dataAvailable(ko.contextFor(node)));
        }
    }
}