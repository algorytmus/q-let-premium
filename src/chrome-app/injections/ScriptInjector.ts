
class ScriptInjector {
    public constructor(
        private readonly dom: IDom,
        private readonly files: Url[],
        private readonly elementSelector: string){}

    public attach()
    {
        var onloadAction = "";
        while(this.files.length > 0)
        {
            onloadAction = ScriptInjector.formatScriptInjection(this.files.pop(), onloadAction);
        }
        this.dom.observeInitialized()
        .do(_ => ScriptInjector.inject(onloadAction))
        .subscribe();
    }

    private static inject(script: string): void {
        $('<script>'+script+'</' + 'script>').appendTo(document.body);
    }

    private static formatScriptInjection(file: Url, onloadAction: string): string
    {
        return `(function(d, script) {
            script = d.createElement('script');
            script.type = 'text/javascript';
            script.async = true;
            script.onload = function(){
                ${onloadAction}
            };
            script.src = '${file.url}';
            d.getElementsByTagName('head')[0].appendChild(script);
        }(document));`
    }
}