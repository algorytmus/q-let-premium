/// <reference path="./../scripts/jquery/jquery.d.ts" />
/// <reference path="./../scripts/rxjs/ts/rx.all.d.ts" />
/// <reference path="./../scripts/rxjs-jquery/ts/rx.jquery.d.ts" />
/// <reference path="./../scripts/rx-dom/rx-dom.d.ts" />
/// <reference path="./Url.ts" />

//dependsOn=scripts/jquery/jquery.js
//dependsOn=scripts/rxjs/rx.all.js
//dependsOn=scripts/rxjs-jquery/rx.jquery.js
//dependsOn=scripts/rx-dom/rx.dom.js
//dependsOn=injections/Url.js

class AttributeInjector {

    constructor(private readonly elementSelector: string,
        private readonly attributeName: string,
        private readonly attributeValue: string,
        private readonly dom: IDom ) {}

    public attach(): Rx.IDisposable {
        const injector = this;
        return this.dom
            .observeInitializedAndChanges()
            .subscribe(v => AttributeInjector.inject(injector));
    }

    private static inject(_this: AttributeInjector): void {
        $(_this.elementSelector).attr(_this.attributeName, _this.attributeValue);
    }
}