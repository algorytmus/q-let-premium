import { Dom } from './Dom';
/// <reference path="./../scripts/jquery/jquery.d.ts" />
/// <reference path="./../scripts/rxjs/ts/rx.all.d.ts" />
/// <reference path="./../scripts/rxjs-jquery/ts/rx.jquery.d.ts" />
/// <reference path="./../scripts/rx-dom/rx-dom.d.ts" />
/// <reference path="./Url.ts" />

//dependsOn=scripts/jquery/jquery.js
//dependsOn=scripts/rxjs/rx.all.js
//dependsOn=scripts/rxjs-jquery/rx.jquery.js
//dependsOn=scripts/rx-dom/rx.dom.js
//dependsOn=injections/Url.js

class ElementAppender {
    constructor(private readonly parentElementSelector: string,
        private readonly elementSelector: string,
        private readonly predicate: () => boolean,
        private readonly valueToAppend: string,
        private readonly dom: IDom ) {}

    public attach(): Rx.IDisposable {
        const injector = this;
        return this.dom
            .observeInitializedAndChanges()
            .delay(100)
            .subscribe(v => ElementAppender.inject(injector));
    }

    private static inject(_this: ElementAppender): void {
        console.log("APPENDING TRY");
        if($(_this.elementSelector).length == 0 && _this.predicate())
        {
            console.log("APPENDING OK");
            $(_this.parentElementSelector).prepend(_this.valueToAppend);
        }
    }
}